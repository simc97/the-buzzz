// To do. Use express to host a server
//const express = require("express");
//const https = require("https");
//const app = express(); //initialize a new express app

let userNames = ["Zelma Garza", "Asutosh Dhakal", "Simrat Chandi", "SeongBeomKo", "Rudra Garg", "Shivi Revuru"];
var users = [];

function User(name) {
    this.name = name;
    this.commitCount = 0;
    this.issueCount = 0;
    this.alternateName = "";
}

function setUpUsers() {
    //declare the expected usernames
    for (var i = 0; i < 6; i++) {
        var currUser = new User(userNames[i]);
        users.push(currUser);
    }
    //special case of other alternate names
    users[3].alternateName = "David Ko";
    users[1].alternateName = "Asu Dhakal";
    users[5].alternateName = "Shivani";
    return users;
}

function updateGitLabStats() {
    setUpUsers();
    updateCommits();
    updateIssues();
}

function updateCommits() {
    const urlCommits = "https://gitlab.com/api/v4/projects/16944761/repository/commits?all=True&per_page=10000";
    $.get(urlCommits, function (data) {
        var totalCommits = data.length;
        for (var i = 0; i < totalCommits; i++) {
            var author = data[i].author_name;
            var userIndex = -1;
            for (var u = 0; u < users.length; u++) {
                if (users[u].name.includes(author) || users[u].alternateName.includes(author)) {
                    userIndex = u;
                    break;
                }
            }
            users[userIndex].commitCount += 1;
        }
        var precount = "Number of commits: ";
        $("#zelmaC").text(precount + users[0].commitCount + "/" + totalCommits);
        $("#asuC").text(precount + users[1].commitCount + "/" + totalCommits);
        $("#simratC").text(precount + users[2].commitCount + "/" + totalCommits);
        $("#davidC").text(precount + users[3].commitCount + "/" + totalCommits);
        $("#rudraC").text(precount + users[4].commitCount + "/" + totalCommits);
        $("#shivaniC").text(precount + users[5].commitCount + "/" + totalCommits);
    });
}

function updateIssues() {
    const urlIssues = "https://gitlab.com/api/v4/projects/16944761/issues?per_page=100&state=all&scope=all";
    $.get(urlIssues, function (data) {
        var totalIssues = data.length;
        console.log("Total issues "+totalIssues);
        for (var i = 0; i < totalIssues; i++) {
            var numAssignees = data[i].assignees.length;
            for (var a = 0; a < numAssignees; a++) {
                var currAssignee = data[i].assignees[a].name;
                console.log("Current assignee "+currAssignee);
                var userIndex = -1;
                //update that user's issue count
                for (var u = 0; u < users.length; u++) {
                    if (users[u].name.includes(currAssignee) || users[u].alternateName.includes(currAssignee)) {
                        userIndex = u;
                        break;
                    }
                }
                users[userIndex].issueCount += 1;
            }
        }
        var precount = "Number of issues: ";
        $("#zelmaI").text(precount + users[0].issueCount + "/" + totalIssues);
        $("#asuI").text(precount + users[1].issueCount + "/" + totalIssues);
        $("#simratI").text(precount + users[2].issueCount + "/" + totalIssues);
        $("#davidI").text(precount + users[3].issueCount + "/" + totalIssues);
        $("#rudraI").text(precount + users[4].issueCount + "/" + totalIssues);
        $("#shivaniI").text(precount + users[5].issueCount + "/" + totalIssues);
    });
}

// const urlCommits = "https://gitlab.com/api/v4/projects/16944761/repository/commits?all=true";
//     https.get(urlCommits,function(response){ 
//         response.on("data",function(data){
//             const commitData = JSON.parse(data);
//             //console.log(commitData);

//             //const author = commitData[0].author_name;
//             console.log(commitData);
//         });
//     });
// app.get("/",function(req,res){
//     const urlCommits = "https://gitlab.com/api/v4/projects/16944761/repository/commits";
//     https.get(urlCommits,function(response){

//         response.on("data",function(data){
//             const commitData = JSON.parse(data);
//             //console.log(commitData);
//             const author = commitData[0].author_name;
//         });
//     });
//     res.sendFile("/Users/mac/Desktop/the-buzzz/the-buzzz-app/public/home.html");
// });

// app.listen(3000,function(){
//     console.log("Server is running on port 3000");
// });