# start from base
FROM ubuntu:latest

MAINTAINER your_name "ksb3602@gmail.com"

RUN apt-get update -y
RUN apt-get -y install python3
RUN apt-get install -y python3-pip python3-dev build-essential

COPY . usr/src/app2
# COPY . usr/src/app2/.
COPY requirements.txt usr/src/app2/requirements.txt

WORKDIR usr/src/app2

RUN pip3 install --upgrade pip
RUN pip3 install -r requirements.txt

# RUN black ./application_name.py
# EXPOSE 5000

ENTRYPOINT ["python3"]

CMD ["start_helloflask.py"]
# CMD ["./application_name.py"]
