import json
import psycopg2
import apis
import tweepy
import private 
import datetime, time #time
from wordsegment import load, segment
from country_list import countries_for_language
from newsapi import NewsApiClient 
import requests
import math
import datetime
import time



auth = tweepy.OAuthHandler(private.TWITTER_APP_KEY, private.TWITTER_APP_SECRET)
auth.set_access_token(private.TWITTER_KEY, private.TWITTER_SECRET)
api = tweepy.API(auth)
 


with open('countries.json') as cjson:
	jlst = json.load(cjson)



conn = psycopg2.connect(host="buzzz.cyg5ivkoczx2.us-east-2.rds-preview.amazonaws.com", database="buzzz", user="dbAdmin",
	password="thebuzzz")


country_info = []
countries = []
isos = []
val = 0
for item in jlst:
	info = (jlst[val]['name'], jlst[val]['alpha3Code'][0].lower(), jlst[val]['alpha2Code'][0].lower())
	country_info.append(info)
	countries.append(jlst[val]['name'])
	isos.append((jlst[val]['altSpellings'][0].lower(), jlst[val]['alpha2Code'][0].lower()))
	val += 1

#===============================================================================#
def update():
	cur = conn.cursor()
	cur.execute("""
	DROP TABLE IF EXISTS "tweets" """)
	cur.execute("""
	DROP TABLE IF EXISTS "news" """)
	cur.execute("""
	DROP TABLE IF EXISTS "countries"
	 """)


def countries_sql(jlist):
	cur = conn.cursor()
	cur.execute("""
		CREATE TABLE IF NOT EXISTS "countries" (
			countryid INT,
			name VARCHAR,
			capital VARCHAR,
			population INTEGER,
			subregion VARCHAR,
			latlng JSON,
			currencies JSON,
			languages JSON,
			timezones JSON,
			borders JSON,
			relevance FLOAT,
			callingcodes JSON,
			tweettopic VARCHAR,
			newstitle VARCHAR
		)
		""")


	cur = conn.cursor()
	val = 0
	for item in jlist:
		cur = conn.cursor()
		print(val)
		tempval = val + 1
		cur.execute("""SELECT * FROM "tweets" WHERE countryid = %s""", [tempval])
		tweet_id_topic = cur.fetchall()[0][1] #guessing that this is in the second column of tweets

		cur.execute("""SELECT * FROM "news" WHERE countryid = %s""", [tempval])
		news_id_topic = cur.fetchall()[0][2]

		toexec = (tempval, jlist[val]["name"], jlist[val]["capital"], jlist[val]["population"],  jlist[val]["subregion"],
			json.dumps(jlist[val]["latlng"]), json.dumps(jlist[val]["currencies"]), json.dumps(jlist[val]["languages"]), 
			json.dumps(jlist[val]['timezones']), json.dumps(jlist[val]["borders"]),
			jlist[val]["relevance"], json.dumps(jlist[val]["callingCodes"]), tweet_id_topic, news_id_topic)
		

		cur.execute("""INSERT INTO public."countries" (
			"countryid",
			"name",
			"capital",
			"population",
			"subregion",
			"latlng",
			"currencies",
			"languages",
			"timezones",
			"borders",
			"relevance",
			"callingcodes",
			"tweettopic",
			"newstitle"
		)
	   VALUES (%s, %s, %s, %s,  %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""", toexec)
		conn.commit()
		val += 1

#unfinished code
def tweetsandnews_sql(countries, start_val):
	cur = conn.cursor()	
	cur.execute("""
		CREATE TABLE IF NOT EXISTS "tweets" (
			countryid INTEGER,
			topic VARCHAR,
			rank INTEGER,
			promotedcontent VARCHAR,
			tweetvolume INTEGER NULL,
			asof VARCHAR,
			createdat VARCHAR,
			hashtag VARCHAR,
			tweetsearchlink VARCHAR
			)
		""")
	cur.execute("""
		CREATE TABLE IF NOT EXISTS "news" (
			countryid INTEGER,
			tweettopic VARCHAR,
			title VARCHAR,
			description VARCHAR,
			link VARCHAR,
			author VARCHAR,
			source VARCHAR,
			publishedat VARCHAR,
			publishtime VARCHAR,
			imageurl VARCHAR
			)
		""")

	val = start_val
	for country in countries:
		tweet, article = apis.get_tweet_and_article_for_country(country)
		flag = "no"
		if(tweet["name"].startswith("#")):
			flag = "yes"
		time = datetime.datetime.now()
		time_input = time.strftime("%m/%d/%Y")

		toexec = (val, tweet['name'], tweet['rank'], tweet['promoted_content'],
			tweet['tweet_volume'], time_input, time_input, flag, tweet['url'])
		print(toexec)
		cur.execute("""
			INSERT INTO public."tweets"(
				countryid,
				topic,
				rank,
				promotedcontent,
				tweetvolume,
				asof,
				createdat,
				hashtag,
				tweetsearchlink) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)
			""", toexec)
		conn.commit()
		news_sql(tweet['name'], article, val)
		conn.commit()
		val += 1
def news_sql(topic, article, country_id):
	cur = conn.cursor()
	# article = apis.get_article_for_tweet(topic)
	if (article != None):
		toexec = (country_id, topic, 
			article['title'], article['description'],
			article['url'], article['author'], article.get('source').get('name'), 
			article['publishedAt'], article['time'], article['urlToImage'])
	else:
		toexec = (country_id, None, None, None, None, None, None, None, None, None)
	cur.execute("""
		INSERT INTO "news" (
			"countryid",
			"tweettopic",
			"title",
			"description",
			"link",
			"author",
			"source",
			"publishedat",
			"publishtime",
			"imageurl"
			) VALUES
			(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
		""", toexec)



update()
tweetsandnews_sql(countries[:50], 1)
# conn.commit()
time.sleep(900)
tweetsandnews_sql(countries[50:], 51)
# conn.commit()
countries_sql(jlst)

# conn.commit()
conn.close()


