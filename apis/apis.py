import tweepy
import private 
import datetime, time
from wordsegment import load, segment
from country_list import countries_for_language
import psycopg2 as p
from newsapi  import NewsApiClient 
import requests
import math 
woeids = {'Albania': 963291, 'Algeria': 23424740, 'Argentina': 23424747, 'Armenia': 2343932, 'Austria': 23424750,
'Australia': 23424748, 'Bahamas': 2450022, 'Bahrain': 23424753, 'Belarus': 23424765, 'Belgium': 23424757, 
'Brazil': 23424768, 'Bulgaria': 963291, 'Canada': 23424775, 'Chile': 349861, 'China': 1236594, 'Colombia': 23424787,
'Costa Rica': 23424924, 'Croatia': 551801, 'Cuba': 2450022, 'Cyprus': 2323778, 'Czech Republic': 526363, 'Denmark': 23424796,
'Dominica': 23424800, 'Dominican Republic': 23424800, 'Ecuador': 375733, 'Egypt': 23424802, 'Estonia': 23424874, 'Ethiopia': 23424863,
'Finland': 2123260, 'France': 23424819, 'Guatamala': 23424834, 'Georgia': 2357024, 'Germany': 23424829, 'Greece': 23424833,
'Honduras': 23424834, 'Hong Kong': 1236690, 'Hungary': 551801, 'Iceland': 21125, 'India': 2282863, 'Indonesia': 1046138, 
'Iran': 23424870, 'Iraq': 23424870, 'Ireland': 23424803, 'Israel': 1968222, 'Italy': 23424853, 'Jamaica': 23424800, 
'Japan': 1116753, 'Jordan': 23424860, 'Kuwait': 23424870, 'Latvia': 23424874, 'Lebanon': 23424873, 'Libya': 1522006, 'Lithuania': 23424874, 'Luxembourg': 667931, 'Madagascar': 1528335, 'Malaysia': 1154679, 
'Maldives': 2295420, 'Mauritius': 1528335, 'Mexico': 116545, 'Moldova': 929398, 'Mongolia': 2121040, 'Morocco': 766356, 
'Netherlands': 23424909, 'New Zealand': 23424916, 'Nigeria': 23424908, 'Norway': 862592, 'Oman': 23424898, 'Panama': 23424924, 
'Paraguay': 455822, 'Peru': 23424919, 'Philippines': 1198785, 'Poland': 23424923, 'Portugal': 23424925, 'Qatar': 23424930, 
'Romania': 924943, 'Russia': 23424936, 'Rwanda': 1528488, 'Saudi Arabia': 1937801, 'Serbia': 963291, 'Slovakia': 502075, 
'Slovenia': 23424750, 'South Africa': 23424942, 'South Korea': 23424868, 'Sudan': 1939873, 'Spain': 766273, 'Sri Lanka': 2295424, 
'Sweden': 906057, 'Singapore': 23424948, 'Switzerland': 23424957, 'Taiwan': 2345896, 'Thailand': 1225448, 'Tanzania': 1528335, 
'Tunisia': 719846, 'Turkey': 23424969, 'Turkmenistan': 23424922, 'Ukraine': 924938, 'United Arab Emirates': 1940330, 
'United Kingdom': 23424975, 'United States': 2391279, 'Uruguay': 468739, 'Venezuela': 23424982, 'Vietnam': 23424984, 
'Zimbabwe': 1586638}

english = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
numbers = "0123456789"

def get_tweet_and_article_for_country(country):
    
    auth = tweepy.OAuthHandler(private.TWITTER_APP_KEY, private.TWITTER_APP_SECRET)
    auth.set_access_token(private.TWITTER_KEY, private.TWITTER_SECRET)
    api = tweepy.API(auth)
    
    #get the Where On Earth ID and find the trends there
    woeid = woeids.get(country)
    trends = api.trends_place(woeid)
    
    finding = True
    j = 0
    rank = 1
    #while true, go through every tweet until found a valid english one
    while finding: 
        #get one tweet
        tweetInfo = trends[0].get("trends")[j]
        tweet = tweetInfo.get("name")
        index = 0
        #if it is a hashtag, look after the tag
        if tweet.startswith('#'):
            index = index + 1
        
        allNumbers = False
        #while there are no more numbers starting the tweet, increase index
        while(tweet[index] in numbers):
            index = index + 1
            #if the index is greater that the len of the tweet, tweet is full of numbers
            if(index >= len(tweet)):
                 allNumbers = True
                 break
        #if there are no more numbers
        if(allNumbers == False):
            #check if in english, and return if so
            if tweet[index] in english:
                #here we must make sure if the value is already in the database
                tweet = "#NoMoreDreamIsComing"
                
                
                con = p.connect("dbname='buzzz' user='dbAdmin' host='buzzz.cyg5ivkoczx2.us-east-2.rds-preview.amazonaws.com:5432' password='thebuzzz'")
                cur = con.cursor()
                cur.execute("select * from Tweets")
                rows = cur.fetchall()
                duplicate = False
                for index in range(len(rows)):
                    if rows[index][1] == tweet:
                        duplicate = True
                        print("Found")
                        break
                
                if (duplicate == False):
        
                    article = get_article_for_tweet(tweetInfo.get("name"))

                    if(article != None):
                        tweetInfo['rank'] = rank
                        return tweetInfo, article

        #move to next tweet
        j+=1
        rank+=1
        #if we have looked at all trends and still found nothing, return none
        if(j >= len(trends[0].get("trends"))):
            return None
            

#split the hashtag up into differnet words
def get_words(hashtag):
    load()
    words = segment(hashtag)
    search = " "
    return (search.join(words)) 



def get_article_for_tweet(topic):
    newsapi = NewsApiClient(api_key=private.NEWS_KEY)
    #if hashtag
    if(topic.startswith('#')):
        #split up words
        topic = topic[1:]
        search = get_words(topic)
    else:
        search = topic
    #search
    news = newsapi.get_everything(q=search , language = "en")
    #get the top article
    top_articles = news.get("articles")
    try:
        article = top_articles[0]
    except:
        return None
    date, time = getDateAndTime(article)
    article['time'] = time
    article['publishedAt'] = date


    return article


def getDateAndTime(article):
    #get the string
    published = article.get('publishedAt')
    date = ""
    time = ""
    index = 0
    #while not T, place every char of published into date
    while(published[index] != 'T'):
        date = date + published[index]
        index += 1
    
    #move pass T
    index += 1
    while(published[index] != 'Z'):
        time = time + published[index]
        index += 1
    
    return (date, time)

def search_for_tweet(topic):

    auth = tweepy.OAuthHandler(private.TWITTER_APP_KEY, private.TWITTER_APP_SECRET)
    auth.set_access_token(private.TWITTER_KEY, private.TWITTER_SECRET)
    api = tweepy.API(auth)
    
    tweetIDs = []
    for result in api.search(q = topic, result_type = "popular", count = 3):
        tweetIDs.append(result.id)

    return tweetIDs



tweet, article = get_tweet_and_article_for_country("United States")
print(tweet)
print('----')
print(article)