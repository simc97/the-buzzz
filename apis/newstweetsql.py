import json
import psycopg2
import apis #Rudra's file
import private
import tweepy
import datetime, time #time
from geopy.geocoders import Nominatim #
from wordsegment import load, segment
from country_list import countries_for_language
import time
from newsapi import NewsApiClient 
import requests
import math 

auth = tweepy.OAuthHandler(private.TWITTER_APP_KEY, private.TWITTER_APP_SECRET)
auth.set_access_token(private.TWITTER_KEY, private.TWITTER_SECRET)
api = tweepy.API(auth)

with open('countries.json') as cjson:
	jlst = json.load(cjson)

#jlst is a dictionary
country_info = []
countries = []
isos = []
val = 0
for item in jlst:
	info = (jlst[val]['name'], jlst[val]['alpha3Code'][0].lower(), jlst[val]['alpha2Code'][0].lower())
	country_info.append(info)
	countries.append(jlst[val]['name'])
	isos.append((jlst[val]['altSpellings'][0].lower(), jlst[val]['alpha2Code'][0].lower()))
	val += 1



conn = psycopg2.connect(host="buzzz.cyg5ivkoczx2.us-east-2.rds-preview.amazonaws.com", database="buzzz", user="dbAdmin",
	password="thebuzzz")





def tweet_countrydb(api, countries): 
	cur = conn.cursor()	
	#create database
	tags = []
	cur.execute("""CREATE TABLE IF NOT EXISTS "CountryTweets" (
		country VARCHAR NOT NULL,
		tweets JSON NULL
		) """)
	tweets = apis.get_hashtags(api, countries[:50])
	for item in tweets:
		name = item[0]
		tweet = json.dumps(item[1])
		toexec = (name, tweet)
		# print(name)
		cur.execute("""INSERT INTO public."CountryTweets"(
    		"country", "tweets")
   		VALUES (%s, %s)""", toexec)
	#timer for 15 mins 
	time.sleep(900)
	tweets2 = apis.get_hashtags(api, countries[50:]) 
	for item in tweets2:
		name = item[0]
		tweet = json.dumps(item[1])
		toexec = (name, tweet)
		cur.execute("""INSERT INTO public."CountryTweets"(
    		"country", "tweets")
   		VALUES (%s, %s)""", toexec)

	return tweets + tweets2


def news_articles(country_info):
	cur = conn.cursor()
	cur.execute("""CREATE TABLE IF NOT EXISTS "CountryNews" (
		country VARCHAR NOT NULL,
		country_iso VARCHAR NOT NULL,
		news JSON NULL
		) """)
	for item in country_info:
		name = item[0]
		iso = [item[1]]
		other_iso = [item[2]]
		try:
			news = apis.get_articles_countries(iso)
		except:
			pass
		try:
			news = apis.get_articles_countries(other_iso)
		except:
			news = None
		toexec = (name, iso, news)
		cur.execute("""INSERT INTO public."CountryNews"(
   		"country", "country_iso", "news")
   		VALUES (%s, %s, %s)""", toexec)

def get_tags():
	cur = conn.cursor()
	tags = []
	cur.execute("""SELECT * FROM "CountryTweets" """)
	tags = cur.fetchall()
	return tags


def tweet_and_news(tags):
	cur = conn.cursor()
	cur.execute("""CREATE TABLE IF NOT EXISTS "CountryTweetNews" (
		country VARCHAR NOT NULL,
		hashtag VARCHAR NULL,
		news JSON NULL
		)""")

	for i in range(0, len(tags)):
		try: 
			articles = apis.get_articles_hashtags(tags[i][1].get("name"))
			print("article has something", tags[i][1].get("name"))
		except:
			articles = None
		toexec = (tags[i][0], tags[i][1].get("name"), json.dumps(articles))
		cur.execute("""INSERT INTO "CountryTweetNews"(
			country, hashtag,news)
			VALUES (%s, %s, %s) """, toexec)
	conn.commit()

# tags = tweet_countrydb()
# news_articles(country_info)

# tags = tweet_countrydb(api, countries)
# conn.commit()
#send in just hashtags 
all_tags = get_tags()
tweet_and_news(all_tags)
# tweet_and_news()
conn.commit()
conn.close()







