import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { api } from '../../instance.js';
import './Visualizations.css'
import BubbleVis from "../../components/BubbleVis.jsx";
import MapChart from "../../components/MapChart.jsx";
import BarGraph from "../../components/StackedBar.jsx";
import ReactTooltip from "react-tooltip";
import CustomerCitiesVis from '../../components/CustomerCitiesVis.jsx';
import CustomerBarGraph from '../../components/CustomerBarGraph.jsx';
import CustomerScatter from '../../components/CustomerScatter.jsx';
import {countryCodes} from '../../Pages/Countries/countryDict.js'; 


export default class Visualizations extends React.Component {
    constructor() {
        super();
        this.state = {
            showBarGraph: false,
            showBubbleGraph: false,
            showMap: false,
            showCustomerVis: false,
            tooltip: "", 
            countries: []      
        }
    
        
    }
    setContent = (tooltip) => {
        this.setState({tooltip: tooltip})
      }
    
    checkToolTip(tooltip) {
          return tooltip
      }
    
      componentDidMount() {
		api
			.get("countries")
			.then((res) => {
				const countries = res.data;
				this.setState(
					{
						countries: countries.objects,
						loading: false,
					}
				);
			})
			.catch((error) => console.log("OOPS i DID IT AGAIN " + error.response));
	}
    
    getData = () => {
        let obj = {};
        this.state.countries.map((element) => {
			if (element.name) {
				obj[countryCodes[element.name].toUpperCase()] = element.tweettopic;
			}
	    });
        return obj;
      
    }


    render() {
        let codesTweets = this.getData();
        return (
            <div>
                <h1 className="my-4">
                    <strong>Visualizations</strong>
                </h1>
                <div className="btn-group d-flex" role="group" aria-label="...">
                    <button type="button" className="btn btn-primary" id='button-format' onClick={() => {
                        this.setState({
                            showBarGraph: true,
                            showBubbleGraph: false,
                            showMap: false,
                            showCustomerVis: false
                        });
                    }}>
                        Bar Graph
                    </button>
                    <button type="button" className="btn btn-primary" id='button-format' onClick={() => {
                        this.setState({
                            showBarGraph: false,
                            showBubbleGraph: false,
                            showMap: true,
                            showCustomerVis: false
                        });
                    }}>
                        World Map
                    </button>
                    <button type="button" className="btn btn-primary" id='button-format' onClick={() => {
                        this.setState({
                            showBarGraph: false,
                            showBubbleGraph: true,
                            showMap: false,
                            showCustomerVis: false
                        });
                    }}>
                        Bubble Graph
                    </button>
                    <button type="button" className="btn btn-primary" id='button-format'onClick={() => {
                        this.setState({
                            showBarGraph: false,
                            showBubbleGraph: false,
                            showMap: false,
                            showCustomerVis: true  
                        });
                    }}>
                        Customer's Visualizations
                    </button>
                </div>
                {this.state.showBarGraph === false && this.state.showBubbleGraph === false &&  this.state.showMap === false && this.state.showCustomerVis === false &&
                <div className= "subtext">Click Above Tabs to show Visualizations</div>}
                {this.state.showBarGraph &&
                    <div>
                        <BarGraph />
                    </div>
                }
                {this.state.showBubbleGraph &&
                    <div>
                        <BubbleVis />
                    </div>
                }
                {this.state.showMap &&
                    <div>
                        <div className="bar_graph">
                            <h2 className="my-4">
                                <strong>World Map</strong>
                            </h2>
                        </div>
                        <div className= "mapStyle">
                            <MapChart setTooltipContent={this.setContent} data = {codesTweets} />
                            <ReactTooltip multiline={true}>{this.checkToolTip(this.state.tooltip)}</ReactTooltip>
                        </div>
                    </div>
                }
                {this.state.showCustomerVis &&
                    <div>
                        <div>
                            <CustomerCitiesVis/>
                        </div>
                        <div>
                            <CustomerBarGraph/>
                        </div>
                        <div>
                            <CustomerScatter/>
                        </div>
                    </div>
                }
            </div>
        );
    }
}