import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {api} from '../../instance.js';
import {countryCodes} from './countryDict.js'; 
import './Country.css'



export default class CountryProfile extends React.Component {
	constructor(){
		super();
        this.state = {
            country: null,
            loading: true
        }
	}
    componentDidMount() {
        //get ALL countries, update state
        let urlSplit = String(window.location.href).split('/');
        let countryName = urlSplit[urlSplit.length - 1];
        let link = `countries?q={"filters":[{"name":"countryid", "op":"eq", "val":"${countryName}"}]}`;
		api.get(link)
        .then(res  => res.data)
		.then(data => {
			let jsonData = JSON.parse(JSON.stringify(data.objects));
			let countryData = jsonData[0];
			this.setState({country: countryData, loading: false});
		})
        .catch(error => console.log("OOPS i DID IT AGAIN " + error.response));
    }

    getCountryTweet= (country) => {
        let tweet = [];
        let tweetName = String(country.tweettopic);
        if(tweetName.substring(0,1) === "#") {
            tweetName = country.tweettopic.replace('#', '%23');
        }
        tweet.push(
            <div>
                <a href= {("/tweets/" + String(tweetName))}>{country.tweettopic} </a>
            </div>
        )

        return tweet;
    }

    render(){
        if (this.state.country === null) {
            return <b> Loading... </b>
        } 
        else {
			let countryCode = countryCodes[this.state.country.name];
            let link = "https://flagpedia.net/data/flags/w1160/"+countryCode+".webp";
            let googleMaps = "https://www.google.com/maps/embed/v1/place?key=AIzaSyC87AOeLjqelqKRypO7Ewx0nsKC2b09oxg&q=" + this.state.country.name
            return (
                    <div className="container">
                        <div className = "country_header">
                        <h1 className="my-4">{this.state.country.name}</h1>
                        </div>
                        <div className="row">
							<img src={link} alt = "Flag" className="flag_border flag_profile"></img>
                            <div className="col-md-4">
                                <ul>
                                    <li>capital: {this.state.country.capital}</li>
                                    <li>population: {(this.state.country.population).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}</li>
                                    <li>subregion: {this.state.country.subregion}</li>
                                    <li>lat/lng: {this.state.country.latlng.toString()}</li>
                                    <li>currencies: {this.state.country.currencies}</li>
                                    <li>languages: {this.state.country.languages}</li>
                                    <li>timezones: {this.state.country.timezones}</li>
                                    <li>borders: {this.state.country.borders.toString()}</li>
                                    <li>relevance: {this.state.country.relevance}</li>
                                    <li>callingCodes: {this.state.country.callingcodes}</li>
                                    <li>Top trending topics: {this.getCountryTweet(this.state.country)}</li>
                                    <li>News: <a alt="News Title" href= {("/news/" + String(this.state.country.newstitle))}>{this.state.country.newstitle}</a></li>
                                </ul>
                            </div>
                        </div>
                        <div className = "map_embed"> <iframe src = {googleMaps} width="500" height="500" title="embedded map"></iframe>  </div>
                    </div>

            );
		}
    }
}