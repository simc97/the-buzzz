import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { api} from '../../instance.js';
import {countryCodes} from './countryDict.js';
import Pagination from '../../components/Pagination.jsx';
import {Nav, Dropdown, Button} from 'react-bootstrap';
import './Country.css'
import {langDict} from "../Home/languagesDict.js";
import { subregions } from "./subregions.js";



export default class Countries extends React.Component {

	constructor() {
        super();
        //set up skeleton code to fetch all the countries from API
        this.state = {
            countries: null,
            filteredCountries: [],
            pageCountries: [],
            loading: true,
            currentPage: null,
            totalPages: null,
            filters: {
                subregion: "",
                population: ""
            },
            sort: "-",
            query: ""
        }
    }

    handleInputChange = (event) => {
		const query = event.target.value;
		this.setState((prevState) => {
			const filteredCountries = prevState.countries.filter((element) => {
				let res = false;
                if(element.name){
                    res |= element.name.toLowerCase().includes(query.toLowerCase());
                }
                if(element.subregion){
                    res |= element.subregion.toLowerCase().includes(query.toLowerCase());
                }
                if(element.capital) {
                    res |= element.capital.toLowerCase().includes(query.toLowerCase());
                }
                if(element.languages) {
                    let langRes = element.languages.reduce((cumRes, lang) => {
                        return langDict[lang].toLowerCase().includes(query.toLowerCase()) || cumRes;
                    });
                    res |= langRes;
                }
                if(element.tweettopic) {
                    res |= element.tweettopic.toLowerCase().includes(query.toLowerCase()); 
                }
                if(element.newstitle) {
                    res |= element.newstitle.toLowerCase().includes(query.toLowerCase()); 
                }
                return res;
			});

			var index = 0;
			var newCountries = [];
			while (index < filteredCountries.length && index < 12) {
				newCountries.push(filteredCountries[index]);
				index++;
			}

			this.setState({ pageCountries: newCountries });

			// up to 12 tweets from filtered tweets
			return {
				query,
				filteredCountries,
			};
		});
    };
    
    handleFilter = () => {
        let filters = this.state.filters;
        let filteredCountriesT = this.state.countries;

        for(let key in filters) {
            switch(key) {
                case "subregion":
					if (filters[key] && filters[key] !== "All") {
						filteredCountriesT = filteredCountriesT.filter((element) => {
							return element.subregion === filters[key];
						});
					}
                    break;
                case "population":
                    if(filters[key] && filters[key] !== "All") {
                        filteredCountriesT = filteredCountriesT.filter((element) => {
                            if(filters[key] === "0-50 million") {
                                return element.population >= 0 && element.population < 50000000
                            } 
                            if(filters[key] === "50 million - 250 million") {
                                return element.population >= 50000000 && element.population < 250000000
                            }
                            if(filters[key] === "250 million+") {
                                return element.population >= 250000000
                            }                            
                        })
                    }
                    break;
                default:
                    break;

            }

        }

        switch(this.state.sort) {
            case "Population":
                filteredCountriesT = filteredCountriesT.sort(function(a,b) {
                    return b.population - a.population;
                })
                break;
            case "Relevance":
                filteredCountriesT = filteredCountriesT.sort(function(a,b) {
                    return b.relevance - a.relevance;
                })
                break;
            case "Capital":
                filteredCountriesT = filteredCountriesT.sort(function(a, b){
                    if(a.capital < b.capital) { return -1; }
                    if(a.capital > b.capital) { return 1; }
                    return 0;
                })
                break;
            case "Tweet Topic":
                filteredCountriesT = filteredCountriesT.sort(function(a, b){
                    if(a.tweettopic < b.tweettopic) { return -1; }
                    if(a.tweettopic > b.tweettopic) { return 1; }
                    return 0;
                })
                break;
            case "News":
                filteredCountriesT = filteredCountriesT.sort(function(a, b){
                    if(a.newstitle < b.newstitle) { return -1; }
                    if(a.newstitle > b.newstitle) { return 1; }
                    return 0;
                })
                break;
            default:
                break;

        }
        var index = 0;
		var newCountries = [];
		while (index < filteredCountriesT.length && index < 12) {
            newCountries.push(filteredCountriesT[index]);
			index++;
		}
		this.setState({ pageCountries: newCountries, filteredCountries: filteredCountriesT });

    }

    componentDidMount() {
        //get ALL countries, update state
        api.get('countries')
        .then(res  =>  {
            const countries  =  res.data;
            this.setState({ countries: countries.objects, filteredCountries: countries.objects, loading: false });
        })
        .catch(error => console.log("OOPS i DID IT AGAIN " + error.response));
    }

    createCountriesCards = (countries) => {
       //3 country cards per row. 4 rows
       if(countries.length > 0) {
            let rows = []
            for (let r = 0; r < 4; r++) {
                let children = [];
                for(let c = 0; c < 3; c++) {
                    if(countries.length <= c + 3 * r) {
                        break;
                    }
                    let link = ""
                    let country = countries[c + (3 * r)];
                    if(country ===  undefined) {
                        return;
                    }
                    if(country.name) {
                            let countryCode = countryCodes[country.name];
                            link = "https://flagpedia.net/data/flags/w1160/"+countryCode+".webp";
                    }
                    children.push(
                        <div className = "col-lg-4 col-sm-6 mb-4" >
                                <div className = "card h-100">
                                <Nav.Link className="country-instance" href={("/countries/" + String(country.countryid))}>
                                    <img className = "card-img-top flag_border flag_card" src = {link} alt = "Flag"/>
                                    <div className = "card-body">
                                        <h4 className = "card-title" >{this.highlight(country["name"])}</h4>
                                        
                                        <p> Capital: {this.highlight(country.capital)} </p> 
                                        <p> Population: {(country.population).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}</p> 
                                        <p> Subregion: {this.highlight(country.subregion)} </p> 
                                        <p> Latitude/Longitude: {country.latlng.toString()}</p> 
                                    </div> 
                                    </Nav.Link>
                                </div> 
                        </div>
                    )
                    }
                    rows.push(<br></br>);
                    rows.push(<div className="row">{children}</div>);
                    if (countries.length < 3 * r){ break;}
                    }
                return rows;  
        } 
        return [];
    }

    highlight = (countryName) => {
		if (this.state.query.length > 0) {
            if(countryName ===  null) {
                return;
            }
			var copyCountryName = countryName.toLowerCase();
			let index = copyCountryName.indexOf(this.state.query.toLowerCase());
			if (index >= 0) {
				var newText = [
					countryName.substring(0, index),
					<highlight>
						{countryName.substring(index, index + this.state.query.length)}
					</highlight>,
					countryName.substring(index + this.state.query.length),
				];
            }
            else {
				return countryName;
			}
			return newText;
		} else {
			return countryName;
		}
	};

// called each time we navigate to a new page from the pagination control
onPageChanged = (data) => {
    const { currentPage} = data;

    var startIndex = 12 * (currentPage - 1);
    var index = startIndex;
    var newCountries = [];
    while (
        index < startIndex + 12 &&
        index < this.state.filteredCountries.length
    ) {
        newCountries.push(this.state.filteredCountries[index]);
        index++;
    }
    this.setState({ pageCountries: newCountries });
}



    render() {
        if (this.state.countries === null) {
            return <h2>Loading...</h2>
        } 
        else {
            return ( 
                <div className = "container">
                    <div className = "country_model_header">
                	<h1 className = "my-4" > <strong>Countries</strong></h1> 
                    </div>
                    <div className="input-group mb-3">
						<input
							type="text"
							className="form-control"
							placeholder="Enter country, news..."
							aria-label="Place"
							aria-describedby="basic-addon1"
							background-color="white"
							value={this.state.query}
							onChange={this.handleInputChange}
						/>
					</div>
                    <div className="dropdown">
						{/* Subregion dropdown */}
						<Dropdown>
							<Dropdown.Toggle variant="warning" id="dropdown-basic">
								{this.state.filters["subregion"]
									? this.state.filters["subregion"]
									: "Subregion"}
							</Dropdown.Toggle>
					
							<Dropdown.Menu className="dropdown">
								{subregions.map((value) => {
									return (
										<Dropdown.Item
											key={value}
											onClick={() => {
												//want it to be Hashtag: yes
												// Create new "bar" object, cloning existing bar into new bar
												//filters["hashtag"]
												const newFilters = this.state.filters;
												newFilters["subregion"] = value;
												this.setState({ filters: newFilters });
											}}
										>
											{value}
										</Dropdown.Item>
									);
								})}
							</Dropdown.Menu>
						</Dropdown>
					</div>
                    <div className = "dropdown">
						{/* Tweet Volume dropdown */}
						<Dropdown>
							<Dropdown.Toggle variant="warning" id="dropdown-basic">
								Population: {this.state.filters["population"]}
							</Dropdown.Toggle>

							<Dropdown.Menu className="dropdown">
								{["All", "0-50 million", "50 million - 250 million", "250 million+"].map((value) => {
									return (
										<Dropdown.Item
											key={value}
											onClick={() => {
												const newFilters = this.state.filters;
												newFilters["population"] = value;
												this.setState({ filters: newFilters });
											}}
										>
											{value}
										</Dropdown.Item>
									);
								})}
							</Dropdown.Menu>
						</Dropdown>
					</div>
                    <div className = "dropdown">
						{/* Country population dropdown */}
						<Dropdown>
							<Dropdown.Toggle variant="warning" id="dropdown-basic">
								Sort : {this.state.sort}
							</Dropdown.Toggle>

							<Dropdown.Menu className="dropdown">
								{["No Sort", "Population", "Relevance", "Capital", "Tweet Topic", "News"].map((value) => {
									return (
										<Dropdown.Item
											key={value}
											onClick={() => {
												this.setState({ sort: value });
											}}
										>
											{value}
										</Dropdown.Item>
									);
								})}
							</Dropdown.Menu>
						</Dropdown>
					</div>
                    <div className = "dropdown"> 
						<Button variant="outline-dark" onClick={this.handleFilter}>
							Go
						</Button>
					</div>
                	<div> {this.createCountriesCards(this.state.pageCountries)} </div> 
                	<div className = "pagination">
                    <Pagination
							pageLimit={12}
							pageNeighbours={2}
							totalRecords={this.state.filteredCountries.length}
							onPageChanged={this.onPageChanged}
							key={this.state.filteredCountries.length}
						/>
                	</div> 
                </div>
            );
        }
    }
}
