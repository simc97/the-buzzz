export const countryDict = {1: 'Albania', 
2: 'Algeria', 3: 'Argentina', 
4: 'Armenia', 6: 'Austria', 5: 'Australia', 
7: 'Bahrain', 8: 'Belarus', 9: 'Belgium', 
10: 'Brazil', 11: 'Bulgaria', 
12: 'Canada', 13: 'Chile', 
14: 'China', 15: 'Colombia', 
16: 'Costa Rica', 17: 'Croatia', 
18: 'Cuba', 19: 'Cyprus', 
20: 'Czech Republic', 21: 'Denmark', 
22: 'Dominica', 23: 'Dominican Republic', 
24: 'Ecuador', 25: 'Egypt', 26: 'Estonia', 
27: 'Ethiopia', 28: 'Finland', 29: 'France', 
30: 'Georgia', 31: 'Germany', 
32: 'Greece', 33: 'Honduras', 34: 'Hong Kong', 
35: 'Hungary', 36: 'Iceland', 37: 'India', 
38: 'Indonesia', 39: 'Iran', 40: 'Iraq', 
41: 'Israel', 42: 'Italy', 43: 'Jamaica', 44: 'Japan', 45: 'Jordan', 
46: 'Kuwait', 47: 'Latvia', 48: 'Lebanon', 49:
'Libya', 50:  'Lithuania', 51: 'Luxembourg', 52: 
'Madagascar', 53: 'Malaysia', 54: 'Maldives', 
55: 'Mauritius', 56: 'Mexico', 57: 'Moldova', 
58: 'Mongolia', 59: 'Morocco', 60: 'Netherlands', 
61: 'New Zealand', 62: 'Nigeria', 63: 'Norway', 
64: 'Oman', 65: 'Panama', 66: 'Paraguay', 67: 'Peru', 
68: 'Philippines', 69: 'Poland', 
70: 'Portugal', 71: 'Qatar', 
72: 'Romania', 73: 'Russia', 
74: 'Rwanda', 75: 'Saudi Arabia', 
76: 'Serbia', 77: 'Slovakia', 78: 'Singapore', 
79: 'Slovenia', 80:  'South Africa', 
81: 'South Korea', 82: 'Spain', 83: 'Sri Lanka', 
84: 'Sudan', 85: 'Sweden', 86: 'Switzerland', 87: 'Taiwan', 
88: 'Tanzania', 89: 'Thailand', 90: 'Tunisia', 91: 'Turkey', 
92: 'Turkmenistan', 93: 'Ukraine', 94: 'United Arab Emirates', 
95: 'United Kingdom', 96: 'United States', 
97: 'Uruguay', 98: 'Venezuela', 99: 'Vietnam', 100: 'Zimbabwe'};