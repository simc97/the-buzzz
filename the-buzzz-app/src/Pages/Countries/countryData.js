export const countries = 
    [{
            "borders": [
                "MNE",
                "GRC",
                "MKD",
                "KOS"
            ],
            "callingcodes": [
                "355"
            ],
            "capital": "Tirana",
            "countryid": 1,
            "currencies": [
                "ALL"
            ],
            "languages": [
                "sq"
            ],
            "latlng": [
                41.0,
                20.0
            ],
            "name": "Albania",
            "newstitle": "*Wonder Woman 1984* Is Being Delayed",
            "population": 2893005,
            "relevance": 0.0,
            "subregion": "Southern Europe",
            "timezones": [
                "UTC+01:00"
            ],
            "tweettopic": "#Contagion"
        },
        {
            "borders": [
                "TUN",
                "LBY",
                "NER",
                "ESH",
                "MRT",
                "MLI",
                "MAR"
            ],
            "callingcodes": [
                "213"
            ],
            "capital": "Algiers",
            "countryid": 2,
            "currencies": [
                "DZD"
            ],
            "languages": [
                "ar"
            ],
            "latlng": [
                28.0,
                3.0
            ],
            "name": "Algeria",
            "newstitle": "The founder of a coworking destination on a Thai island says there's an important step you should take before becoming a digital nomad",
            "population": 39500000,
            "relevance": 0.0,
            "subregion": "Northern Africa",
            "timezones": [
                "UTC+01:00"
            ],
            "tweettopic": "#kohlanta"
        },
        {
            "borders": [
                "BOL",
                "BRA",
                "CHL",
                "PRY",
                "URY"
            ],
            "callingcodes": [
                "54"
            ],
            "capital": "Buenos Aires",
            "countryid": 3,
            "currencies": [
                "ARS"
            ],
            "languages": [
                "es",
                "gn"
            ],
            "latlng": [
                -34.0,
                -64.0
            ],
            "name": "Argentina",
            "newstitle": "Fake copyright claim takes down Twitch’s biggest political streamers during Democratic debate",
            "population": 43131966,
            "relevance": 0.0,
            "subregion": "South America",
            "timezones": [
                "UTC-03:00"
            ],
            "tweettopic": "Bad Bunny"
        },
        {
            "borders": [
                "AZE",
                "GEO",
                "IRN",
                "TUR"
            ],
            "callingcodes": [
                "374"
            ],
            "capital": "Yerevan",
            "countryid": 4,
            "currencies": [
                "AMD"
            ],
            "languages": [
                "hy",
                "ru"
            ],
            "latlng": [
                40.0,
                45.0
            ],
            "name": "Armenia",
            "newstitle": "'Miracle on Ice' hockey team captain calls backlash over Trump rally appearance 'unfortunate'",
            "population": 3006800,
            "relevance": 0.0,
            "subregion": "Western Asia",
            "timezones": [
                "UTC+04:00"
            ],
            "tweettopic": "#HaticeKurt"
        },
        {
            "borders": [],
            "callingcodes": [
                "61"
            ],
            "capital": "Canberra",
            "countryid": 5,
            "currencies": [
                "AUD"
            ],
            "languages": [
                "en"
            ],
            "latlng": [
                -27.0,
                133.0
            ],
            "name": "Australia",
            "newstitle": "Queenslanders head to the polls during coronavirus pandemic",
            "population": 23868800,
            "relevance": 1.5,
            "subregion": "Australia and New Zealand",
            "timezones": [
                "UTC+05:00",
                "UTC+06:30",
                "UTC+07:00",
                "UTC+08:00",
                "UTC+09:30",
                "UTC+10:00",
                "UTC+10:30",
                "UTC+11:30"
            ],
            "tweettopic": "#QLDelection"
        },
        {
            "borders": [
                "CZE",
                "DEU",
                "HUN",
                "ITA",
                "LIE",
                "SVK",
                "SVN",
                "CHE"
            ],
            "callingcodes": [
                "43"
            ],
            "capital": "Vienna",
            "countryid": 6,
            "currencies": [
                "EUR"
            ],
            "languages": [
                "de"
            ],
            "latlng": [
                47.33333333,
                13.33333333
            ],
            "name": "Austria",
            "newstitle": "UK Prime Minister Boris Johnson tests positive for coronavirus",
            "population": 8602112,
            "relevance": 0.0,
            "subregion": "Western Europe",
            "timezones": [
                "UTC+01:00"
            ],
            "tweettopic": "boris johnson"
        },
        {
            "borders": [],
            "callingcodes": [
                "973"
            ],
            "capital": "Manama",
            "countryid": 7,
            "currencies": [
                "BHD"
            ],
            "languages": [
                "ar"
            ],
            "latlng": [
                26.0,
                50.55
            ],
            "name": "Bahrain",
            "newstitle": "Another Filipino tests positive for coronavirus in Hong Kong - ABS-CBN News",
            "population": 1359800,
            "relevance": 0.0,
            "subregion": "Western Asia",
            "timezones": [
                "UTC+03:00"
            ],
            "tweettopic": "#covid2019"
        },
        {
            "borders": [
                "LVA",
                "LTU",
                "POL",
                "RUS",
                "UKR"
            ],
            "callingcodes": [
                "375"
            ],
            "capital": "Minsk",
            "countryid": 8,
            "currencies": [
                "BYR"
            ],
            "languages": [
                "be",
                "ru"
            ],
            "latlng": [
                53.0,
                28.0
            ],
            "name": "Belarus",
            "newstitle": "Belarus leader says nation being forced to merge with Russia",
            "population": 9485300,
            "relevance": 0.0,
            "subregion": "Eastern Europe",
            "timezones": [
                "UTC+03:00"
            ],
            "tweettopic": "belarus"
        },
        {
            "borders": [
                "FRA",
                "DEU",
                "LUX",
                "NLD"
            ],
            "callingcodes": [
                "32"
            ],
            "capital": "Brussels",
            "countryid": 9,
            "currencies": [
                "EUR"
            ],
            "languages": [
                "nl",
                "fr",
                "de"
            ],
            "latlng": [
                50.83333333,
                4.0
            ],
            "name": "Belgium",
            "newstitle": "The founder of a coworking destination on a Thai island says there's an important step you should take before becoming a digital nomad",
            "population": 11248330,
            "relevance": 1.5,
            "subregion": "Western Europe",
            "timezones": [
                "UTC+01:00"
            ],
            "tweettopic": "#KohLanta"
        },
        {
            "borders": [
                "ARG",
                "BOL",
                "COL",
                "GUF",
                "GUY",
                "PRY",
                "PER",
                "SUR",
                "URY",
                "VEN"
            ],
            "callingcodes": [
                "55"
            ],
            "capital": "Brasília",
            "countryid": 10,
            "currencies": [
                "BRL"
            ],
            "languages": [
                "pt"
            ],
            "latlng": [
                -10.0,
                -55.0
            ],
            "name": "Brazil",
            "newstitle": "Coronavirus: Tokoroa forestry family hit hard as industry struggles",
            "population": 204772000,
            "relevance": 2.0,
            "subregion": "South America",
            "timezones": [
                "UTC-05:00",
                "UTC-04:00",
                "UTC-03:00",
                "UTC-02:00"
            ],
            "tweettopic": "#FicaPrior"
        }
    ]