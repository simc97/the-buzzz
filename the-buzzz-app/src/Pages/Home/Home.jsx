import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

import './Home.css';
import bee from '../../images/bee.png';
import news from '../../images/newspaper.png';
import tweet from '../../images/tweet.png';
import map from '../../images/map.png';

export default class Home extends React.Component {
    constructor(){
        super();
        this.state = {
            query: ""
        }      
    }

    renderSearch = (event) => {
        let query = event.target.value;
        this.setState({query: query});
    }

    renderURL = () => {
        let query = this.state.query
        if (query.includes("#")) {
            query = query.replace("#", "%23");
        }
        return ("/search/" + query)
    } 

    render() {
      return (
        <section>
            <section id="landingHeader">
                <div>
                <img id="bee" src={bee} alt="Bee"/>
                    <div className="slogan">
                        <h1> <strong>What's happening around you?</strong></h1>
                        <div className="input-group mb-3">
                            <input type="text" className="form-control" placeholder="Enter country, news, tweet..." aria-label="Place"
                                aria-describedby="basic-addon1" background-color="white" value={this.state.query} onChange={this.renderSearch}/>
                        </div>
                        <a className="btn" href ={this.renderURL()}><button type="button" className="btn btn-outline-dark">Find Out!</button></a>
                        
                    </div> 
                </div>
            </section>
            <section id="features">
                <div className="row">
                    <div className="feature-box col-lg-4">
                        <a href="/tweets">
                            <img id="tweet" src={tweet} alt="Tweet Icon"/>
                        </a>
                        <h4 className="landingCard">See what people are talking about.</h4>
                    </div>
                    <div className="feature-box col-lg-4">
                        <a href="/news">
                            <img id="news" src={news} alt="Newspaper"/>
                        </a>
                        <h4 className="landingCard">Read the news.</h4>
                    </div>
                    <div className="feature-box col-lg-4">
                        <a href="/countries">
                            <img id="country" src={map} alt="Map"/>
                        </a>
                        <h4 className="landingCard">See what's going on based on country.</h4>
                    </div>
                </div>
            </section>
        </section>
      );
    }
  }
  