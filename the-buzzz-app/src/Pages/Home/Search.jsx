import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios';
import {api} from '../../instance.js';
import { countryCodes } from "../Countries/countryDict.js";
import { countryDict } from "../Countries/countryDictID.js";
import {Nav} from 'react-bootstrap';
import {langDict} from "./languagesDict.js";

export default class Search extends React.Component {
	constructor() {
		super();
		this.state = {
            loading: true,
            tweets: [],
            filteredTweets: [],
            countries: [],
            filteredCountries: [], 
            news: [],
            filteredNews: [],
            query: "" 
		};
    }
    
    componentDidMount() {
        let urlSplit = String(window.location.href).split('/');
        let link= urlSplit[urlSplit.length - 1];
        axios.all([
                api.get('tweets'),
                api.get('countries'),
                api.get('news'),
        ])
        .then((responseArr) => [responseArr[0].data.objects, responseArr[1].data.objects, responseArr[2].data.objects])
        .then(
            axios.spread((tweets, countries, news) => {
            this.setState({tweets, countries, news});
            this.handleSearchQuery();
        })
        )
        .catch(console.warn);
        this.setState({loading: false, query: link});
    }

    handleSearchQuery = () => {
        //*** tweets: topic, country, subregion
        let query = this.state.query;
        if (query.includes("%23")) {
            query = query.replace("%23", "#");
        }
        if(query.includes("%20")) {
            query = query.replace("%20", " ");
        }

        //Tweet topic, country, subregion
		const filteredTweetsT = this.state.tweets.filter((element) => {
                return element.topic.toLowerCase().includes(query.toLowerCase()) || countryDict[element.countryid].toLowerCase().includes(query.toLowerCase()) ||
                element.subregion.toLowerCase().includes(query.toLowerCase());
        });

        //*** News: Title, subregion, source, author, country
        const filteredNewsT = this.state.news.filter((element) => {
            let res = false;
            if(element.title){
                res |= element.title.toLowerCase().includes(query.toLowerCase());
            }
            if(element.subregion){
                res |= element.subregion.toLowerCase().includes(query.toLowerCase());
            }
            if(element.author) {
                res |= element.author.toLowerCase().includes(query.toLowerCase());
            }
            if(element.source) {
                res |= element.source.toLowerCase().includes(query.toLowerCase());
            }
            if(element.tweettopic) {
                res |= element.tweettopic.toLowerCase().includes(query.toLowerCase()); 
            }
            if(element.countryid) {
                res |= countryDict[element.countryid].toLowerCase().includes(query.toLowerCase());
            }
            return res;
        });
        
    
        const filteredCountriesT = this.state.countries.filter((element) => {
            let res = false;
            if(element.name){
                res |= element.name.toLowerCase().includes(query.toLowerCase());
            }
            if(element.subregion){
                res |= element.subregion.toLowerCase().includes(query.toLowerCase());
            }
            if(element.capital) {
                res |= element.capital.toLowerCase().includes(query.toLowerCase());
            }
            if(element.languages) {
                let langRes = element.languages.reduce((cumRes, lang) => {
                    return langDict[lang].toLowerCase().includes(query.toLowerCase()) || cumRes;
                });
                res |= langRes;
            }
            if(element.tweettopic) {
                res |= element.tweettopic.toLowerCase().includes(query.toLowerCase()); 
            }
            if(element.newstitle) {
                res |= element.newstitle.toLowerCase().includes(query.toLowerCase()); 
            }
            return res;
        });
    

        this.setState({filteredTweets: filteredTweetsT, filteredNews: filteredNewsT, filteredCountries: filteredCountriesT, query: query});

    }

    highlight = (outputString) => {
		if (this.state.query.length > 0) {
            if(outputString ===  null) {
                return;
            }
			var copyOutputString = outputString.toLowerCase();
			let index = copyOutputString.indexOf(this.state.query.toLowerCase());
			if (index >= 0) {
				var newText = [
					outputString.substring(0, index),
					<highlight>
						{outputString.substring(index, index + this.state.query.length)}
					</highlight>,
					outputString.substring(index + this.state.query.length),
				];
			}
			else {
				return outputString;
			}
			return newText;
		} else {
			return outputString;
		}
	};

    createCountriesCards = (countries) => {
        //3 country cards per row. 4 rows
        let rows = []
        for (let r = 0; r <= countries.length / 3; r++) {
            let children = [];
            for(let c = 0; c < 3; c++) {
                if(countries.length <= c + 3 * r) {
                    break;
                }
                let link = ""
                let country = countries[c + (3 * r)];
                if(country.name) {
                     let countryCode = countryCodes[country.name];
                     link = "https://flagpedia.net/data/flags/w1160/"+countryCode+".webp";
                }
                children.push(
                    <div className = "col-lg-4 col-sm-6 mb-4" >
                            <div className = "card h-100">
                            <Nav.Link className="city-instance" href={("/countries/" + String(country.countryid))}>
                                <img className = "card-img-top flag_border flag_card" src = {link} alt = "Flag"/>
                                <div className = "card-body">
                                    <h4 className = "card-title" >{this.highlight(country["name"])}</h4>
                                    <p> Capital: {this.highlight(country.capital)} </p> 
                                    <p> Population: {(country.population).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}</p> 
                                    <p> Subregion: {this.highlight(country.subregion)} </p> 
                                    <p> Latitude/Longitude: {country.latlng.toString()}</p> 
                                </div> 
                             </Nav.Link>
                            </div> 
                    </div>
                )
              }
             rows.push(<br></br>);
             rows.push(<div className="row">{children}</div>);
             if (countries.length < 3 * r){ break;}
             }
         return rows;   
     }

     createTweetsCards = (tweets) => {
		//3 tweets cards per row. 4 rows
		let rows = [];
		for (let r = 0; r < Math.ceil(tweets.length * 1.0 / 3); r++) {
			let children = [];
			for (let c = 0; c < 3; c++) {
				if (tweets.length <= c + 3 * r) {
					break;
				}
				var tweet = tweets[c + 3 * r];
				let countryName = countryDict[tweet.countryid];
				let tweetName = String(tweet.topic);
				if (tweet.topic.substring(0, 1) === "#") {
					tweetName = tweet.topic.replace("#", "%23");
				}

				children.push(
					<div className="col-lg-4 col-sm-6 mb-4">
						<Nav.Link className="city-instance" href={"/tweets/" + tweetName}>
							<div className = "card">
							<div className="card h-100">
								<div className="card-body">
									<h4 className="card-title">
										<h4 href=""> {this.highlight(tweet.topic)}</h4>
									</h4>
									<p>
										Promoted Content: {!tweet.promotedcontent ? "No" : "Yes"}
									</p>
									<p>
										Tweet Volume:
										{!tweet.tweetvolume
											? "N/A"
											: tweet.tweetvolume
													.toString()
													.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")}
									</p>
									<p> Country: {this.highlight(countryName)}</p>
									<p> As Of: {tweet.asof}</p>
									<p> Created At: {tweet.createdat}</p>
								</div>
							</div>
							</div>
						</Nav.Link>
					</div>
				);
			}
			rows.push(<br></br>);
			rows.push(<div className="row">{children}</div>);
			if (tweets.length < 3 * r) break;
		}
		return rows;
    };
    
    createNewsCards = (news) => {
        //3 news cards per row. 4 rows
        let rows = []
        for (let r = 0; r < Math.ceil(news.length * 1.0 / 3); r++) {
            let children = [];
            for(let c = 0; c < 3; c++) {
                if(news.length <= c + 3 * r) {
                    break;
                }
                var newsTopic = news[c + (3 * r)];
                let countryName = countryDict[newsTopic.countryid];
                children.push(
                    <div className="col-lg-4 col-sm-6 mb-4">
                        <Nav.Link className="city-instance" href={("/news/" + String(newsTopic.title))}>
                        <div className="card h-100">
                            <a href={("/news/" + String(newsTopic.title))}><img className="card-img-top" src={newsTopic.imageurl} alt=""/></a>
                            <div className="card-body">
                                <h4 className="card-title">   
                                <h4 href=""> {this.highlight(newsTopic.title)}</h4>
                                </h4>
                                <p>Description: {newsTopic.description}</p>
                                <p>Author: {this.highlight(newsTopic.author)}</p>
                                <p>Source: {this.highlight(newsTopic.source)}</p>
                                <p>Country: {this.highlight(countryName)}</p>
                                <p>Tweet Topic: {this.highlight(newsTopic.tweettopic)}</p> 
                            </div>
                        </div>
                    </Nav.Link> 
                 </div>
                )
            }
             rows.push(<br></br>);
             rows.push(<div className="row">{children}</div>);
             if (news.length < 3 * r) break
        }
        return rows
     }

	
    render() {
        if (this.state.loading) {
			return <h2> Loading... </h2>;
        } 
        else {
            return (
                <div>
                    <h2>Tweets</h2>
                    <div> {this.createTweetsCards(this.state.filteredTweets)} </div>
                    <hr></hr>
                    <h2>News</h2>
                    <div> {this.createNewsCards(this.state.filteredNews)} </div>
                    <hr></hr>
                    <h2>Countries</h2>
                    <div> {this.createCountriesCards(this.state.filteredCountries)} </div>
                    <hr></hr>
                </div>
            );
        }
    }
}
 