import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { api } from '../../instance.js';
import  { TwitterTweetEmbed } from 'react-twitter-embed';
import {countryDict} from '../Countries/countryDictID.js'; 
import './Tweets.css';



export default class TweetProfile extends React.Component {
	constructor(){
		super();
        this.state = {
            tweet: null,
            loading: true
        }
	}
    componentDidMount() {
        //get ALL countries, update state
        let urlSplit = String(window.location.href).split('/');
        let tweetName = urlSplit[urlSplit.length - 1];
        
        let link = `tweets?q={"filters":[{"name":"topic", "op":"eq", "val":"${tweetName}"}]}`;
		api.get(link)
        .then(res  => res.data)
		.then(data => {
			let jsonData = JSON.parse(JSON.stringify(data.objects));
			let tweetData = jsonData;
			this.setState({tweet: tweetData, loading: false});
		})
        .catch(error => console.log("OOPS i DID IT AGAIN " + error.response));
        
    }


    getTweetCountries= (tweet) => {
        let countries = [];
        for (let i= 0; i < tweet.length; i++) {
            let countryName = countryDict[tweet[i].countryid];
            countries.push(
                <div>
                <a href= {("/countries/" + String(tweet[i].countryid))}>{countryName} </a>
                </div>
            )
        }
        return countries;
    }

    render(){
        if (this.state.tweet === null) {
            return <b> Loading... </b>
        } 
        else {
			let tweetId1 = String(this.state.tweet[0].tweet1);
            return (
                <div>
                <div className = "tweet_header">
                <h1 className="my-4">{this.state.tweet[0].topic}</h1>
                </div>
        
                <div className="row">
              
                  <div className="col-md-8">
					<TwitterTweetEmbed tweetId={this.state.tweet[0].tweet1}/>
					<TwitterTweetEmbed tweetId={this.state.tweet[0].tweet2}/>
					<TwitterTweetEmbed tweetId={this.state.tweet[0].tweet3}/>
                  </div>
                  <div className="col-md-4">
                    <ul>
                        <li> Rank: {this.state.tweet[0].rank}</li>
                        <li> Promoted Content: {!(this.state.tweet[0].promotedcontent)? "No" : "Yes"}</li>
                        <li> Tweet Volume: {!(this.state.tweet[0].tweetvolume)? "N/A" : this.state.tweet[0].tweetvolume.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}</li>
                        <li> Countries: {this.getTweetCountries(this.state.tweet)}</li>
                        <li> As Of: {this.state.tweet[0].asof}</li>
                        <li> Created At: {this.state.tweet[0].createdat}</li>
                        <li> Hashtag: {this.state.tweet[0].hashtag}</li>
                        <li> <a href= {this.state.tweet[0].tweetsearchlink}> Search</a></li>
                    </ul>
                  </div>
                </div> 
                </div>  

            );
		}
    }
}