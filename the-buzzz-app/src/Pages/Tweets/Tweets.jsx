import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { api } from "../../instance.js";
import Pagination from "../../components/Pagination.jsx";
import { Nav, Dropdown, Button } from "react-bootstrap";
import { countryDict } from "../Countries/countryDictID.js";
import "./Tweets.css";
import { subregions } from "./subregions.js";

export default class Tweets extends React.Component {
	constructor() {
		super();
		//set up skeleton code to fetch all the tweets from API
		this.state = {
			tweets: [], // unchanging 100 instances
			filteredTweets: [], // filtered depending on search bar input
			pageTweets: [], // tweets on the page currently
			loading: true,
			currentPage: null,
			totalPages: null,
			country: null,
			query: "",
			filters: {
				hashtag: "",
				subregion: "",
				tweetvolume: "",
			},
			sort: "-"
		};
	}

	handleInputChange = (event) => {
		const query = event.target.value;
		const filteredTweetsT = this.state.tweets.filter((element) => {
				return element.topic.toLowerCase().includes(query.toLowerCase()) || countryDict[element.countryid].toLowerCase().includes(query.toLowerCase());
		});

			var index = 0;
			var newTweets = [];
			while (index < filteredTweetsT.length && index < 12) {
				newTweets.push(filteredTweetsT[index]);
				index++;
			}

		this.setState({ pageTweets: newTweets, filteredTweets: filteredTweetsT, query: query});

	};



	handleFilter = () => {
		let filters = this.state.filters;
		let filteredTweetsT = this.state.tweets;
		for (let key in filters) {
			//if they selected something for filter
			switch (key) {
				case "hashtag":
					if (
						filters[key] &&
						(filters[key] === "yes" || filters[key] === "no")
					) {
						filteredTweetsT = filteredTweetsT.filter((element) => {
							return element.hashtag === filters[key];
						});
					}
					break;

				case "tweetvolume":
					//ranges 1 - 999,999 million, 1 million plus
					if (
						filters[key] &&
						(filters[key] === "0-999,999" || filters[key] === "1,000,000+")
					) {
						let rangeStart = filters[key] === "0-999,999" ? 0 : 1000000;
						let rangeEnd =
							filters[key] === "0-999,999" ? 999999 : Number.MAX_SAFE_INTEGER;
						filteredTweetsT = filteredTweetsT.filter((element) => {
							if (element.tweetvolume) {
								return (
									element.tweetvolume >= rangeStart &&
									element.tweetvolume <= rangeEnd
								);
							}
						});
					}
					break;

				case "subregion":
					if (filters[key] && filters[key] !== "All") {
						filteredTweetsT = filteredTweetsT.filter((element) => {
							return element.subregion === filters[key];
						});
					}
					break;

				default:
					break;
			}
		}

		switch(this.state.sort) {
			case "Tweet Topic":
				filteredTweetsT = filteredTweetsT.sort(function(a, b){
					if(a.topic < b.topic) { return -1; }
					if(a.topic > b.topic) { return 1; }
					return 0;
			})
				break;
			case "Low to High Volume":
				//increasing order
				filteredTweetsT = filteredTweetsT.sort(function(a, b) {
					return a.tweetvolume - b.tweetvolume;
				})
				break;
			case "High to Low Volume":
				filteredTweetsT = filteredTweetsT.sort(function(a, b) {
					return b.tweetvolume - a.tweetvolume;
				})
				break;
			case "Country":
				filteredTweetsT = filteredTweetsT.sort(function(a, b){
					return a.countryid - b.countryid;
			})
				break;
			default:
				filteredTweetsT = filteredTweetsT.sort(function(a, b){
					return a.countryid - b.countryid;
				})
				break;
		}


		var index = 0;
		var newTweets = [];
		while (index < filteredTweetsT.length && index < 12) {
			newTweets.push(filteredTweetsT[index]);
			index++;
		}
		this.setState({ pageTweets: newTweets, filteredTweets: filteredTweetsT });
	};

	componentDidMount() {
		api
			.get("tweets")
			.then((res) => {
				const tweets = res.data;
				this.setState(
					{
						tweets: tweets.objects,
						filteredTweets: tweets.objects,
						loading: false,
					},
					() => console.log(this.state.tweets)
				);
			})
			.catch((error) => console.log("OOPS i DID IT AGAIN " + error.response));
	}

	createTweetsCards = (tweets) => {
		//3 tweets cards per row. 4 rows
		let rows = [];
		for (let r = 0; r < 4; r++) {
			let children = [];
			for (let c = 0; c < 3; c++) {
				if (tweets.length <= c + 3 * r) {
					break;
				}
				var tweet = tweets[c + 3 * r];
				if(tweet ===  undefined) {
					return;
				}
				let countryName = countryDict[tweet.countryid];
				//let countryid = tweet.countryid;
				let tweetName = String(tweet.topic);
				if (tweet.topic.substring(0, 1) === "#") {
					tweetName = tweet.topic.replace("#", "%23");
				}

				children.push(
					<div className="col-lg-4 col-sm-6 mb-4">
						<Nav.Link className="city-instance" href={"/tweets/" + tweetName}>
							<div className = "card">
							<div className="card h-100">
								<div className="card-body">
									<h4 className="card-title">
										<h4 href=""> {this.highlight(tweet.topic)}</h4>
									</h4>
									<p>
										Promoted Content: {!tweet.promotedcontent ? "No" : "Yes"}
									</p>
									<p>
										Tweet Volume:
										{!tweet.tweetvolume
											? "N/A"
											: tweet.tweetvolume
													.toString()
													.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")}
									</p>
									<p> Country: {this.highlight(countryName)}</p>
									<p> As Of: {tweet.asof}</p>
									<p> Created At: {tweet.createdat}</p>
								</div>
							</div>
							</div>
						</Nav.Link>
					</div>
				);
			}
			rows.push(<br></br>);
			rows.push(<div className="row">{children}</div>);
			if (tweets.length < 3 * r) break;
		}
		return rows;
	};

	highlight = (tweetname) => {
		if (this.state.query.length > 0) {
			var copyTweetName = tweetname.toLowerCase();
			let index = copyTweetName.indexOf(this.state.query.toLowerCase());
			if (index >= 0) {
				var newText = [
					tweetname.substring(0, index),
					<highlight>
						{tweetname.substring(index, index + this.state.query.length)}
					</highlight>,
					tweetname.substring(index + this.state.query.length),
				];
			}
			else {
				return tweetname;
			}
			return newText;
		} else {
			return tweetname;
		}
	};

	getCountry = (id) => {
		//find country name based on id
		let link = `countries?q={"filters":[{"name":"countryid", "op":"eq", "val":"${id}"}]}`;
		api.get(link).then((res) => {
			const countryData = res.data;
			this.setState({ country: countryData });
		});
	};

	// called each time we navigate to a new page from the pagination control
	onPageChanged = (data) => {
		const { currentPage, totalPages} = data;

		this.setState({ currentPage, totalPages });

		var startIndex = 12 * (currentPage - 1);
		var index = startIndex;
		var newTweets = [];
		while (
			index < startIndex + 12 &&
			index < this.state.filteredTweets.length
		) {
			newTweets.push(this.state.filteredTweets[index]);
			index++;
		}
		this.setState({ pageTweets: newTweets });
	};

	render() {
		if (this.state.loading) {
			return <h2> Loading... </h2>;
		} else {
			return (
				<div className="container">
					<div className="tweets_model_header">
						<h1 className="my-4">
							<strong>Trending Tweet Topics</strong>
						</h1>
					</div>
					<div className="input-group mb-3">
						<input
							type="text"
							className="form-control"
							placeholder="Enter country, tweet..."
							aria-label="Place"
							aria-describedby="basic-addon1"
							background-color="white"
							value={this.state.query}
							onChange={this.handleInputChange}
						/>
					</div>
					<div className="dropdown">
						{/* Hashtag DropDown */}
						<Dropdown>
							<Dropdown.Toggle variant="warning" id="dropdown-basic">
								Hashtag? {this.state.filters["hashtag"]}
							</Dropdown.Toggle>
							<Dropdown.Menu className="dropdown">
								{["All", "yes", "no"].map((value) => {
									return (
										<Dropdown.Item
											key={value}
											onClick={() => {
												//want it to be Hashtag: yes
												const newFilters = this.state.filters;
												newFilters["hashtag"] = value;
												this.setState({ filters: newFilters });
											}}
										>
											{value}
										</Dropdown.Item>
									);
								})}
							</Dropdown.Menu>
						</Dropdown>
					</div>
					<div className="dropdown">
						{/* Subregion dropdown */}
						<Dropdown>
							<Dropdown.Toggle variant="warning" id="dropdown-basic">
								{this.state.filters["subregion"]
									? this.state.filters["subregion"]
									: "Subregion"}
							</Dropdown.Toggle>
					
							<Dropdown.Menu className="dropdown">
								{subregions.map((value) => {
									return (
										<Dropdown.Item
											key={value}
											onClick={() => {
												//want it to be Hashtag: yes
												// Create new "bar" object, cloning existing bar into new bar
												const newFilters = this.state.filters;
												newFilters["subregion"] = value;
												this.setState({ filters: newFilters });
											}}
										>
											{value}
										</Dropdown.Item>
									);
								})}
							</Dropdown.Menu>
						</Dropdown>
					</div>

					<div className = "dropdown">
						{/* Tweet Volume dropdown */}
						<Dropdown>
							<Dropdown.Toggle variant="warning" id="dropdown-basic">
								Tweet Volume: {this.state.filters["tweetvolume"]}
							</Dropdown.Toggle>

							<Dropdown.Menu className="dropdown">
								{["All", "0-999,999", "1,000,000+"].map((value) => {
									return (
										<Dropdown.Item
											key={value}
											onClick={() => {
												const newFilters = this.state.filters;
												newFilters["tweetvolume"] = value;
												this.setState({ filters: newFilters });
											}}
										>
											{value}
										</Dropdown.Item>
									);
								})}
							</Dropdown.Menu>
						</Dropdown>
					</div>

					<div className = "dropdown">
						{/* Tweet Volume dropdown */}
						<Dropdown>
							<Dropdown.Toggle variant="warning" id="dropdown-basic">
								Sort : {this.state.sort}
							</Dropdown.Toggle>

							<Dropdown.Menu className="dropdown">
								{["No Sort", "Tweet Topic", "Low to High Volume", "High to Low Volume", "Country"].map((value) => {
									return (
										<Dropdown.Item
											key={value}
											onClick={() => {
												this.setState({ sort: value });
											}}
										>
											{value}
										</Dropdown.Item>
									);
								})}
							</Dropdown.Menu>
						</Dropdown>
					</div>

					<div className = "dropdown"> 
						<Button variant="outline-dark" onClick={this.handleFilter}>
							Go
						</Button>
					</div>
					<div> {this.createTweetsCards(this.state.pageTweets)} </div>
					<div className="pagination">
						<Pagination
							pageLimit={12}
							pageNeighbours={2}
							totalRecords={this.state.filteredTweets.length}
							onPageChanged={this.onPageChanged}
							key={this.state.filteredTweets.length}
						/>
					</div>
				</div>
			);
		}
	}
}
