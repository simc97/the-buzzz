import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { api } from '../../instance.js';
import {countryDict} from '../Countries/countryDictID.js'; 
import './News.css';
import '../../App.css'


export default class NewsProfile extends React.Component {
	constructor(){
		super();
        this.state = {
            news: null,
            loading: true
        }
	}
    componentDidMount() {
        //get ALL news, update state
        let urlSplit = String(window.location.href).split('/');
        let newsName = urlSplit[urlSplit.length - 1];
        let link = `news?q={"filters":[{"name":"title", "op":"eq", "val":"${newsName}"}]}`;
		api.get(link)
        .then(res  => res.data)
		.then(data => {
			let jsonData = JSON.parse(JSON.stringify(data.objects));
			let newsData = jsonData;
			this.setState({news: newsData, loading: false}, () => console.log("News data "+this.state.news));
		})
        .catch(error => console.log("OOPS i DID IT AGAIN " + error.response));
    }

    getNewsCountries= (news) => {
        let countries = [];
        for (let i= 0; i < news.length; i++) {
            let countryName = countryDict[news[i].countryid];
            countries.push(
                <div>
                    <a href= {("/countries/" + String(news[i].countryid))}>{countryName} </a>
                </div>
            )
        }
        return countries
    }

    getNewsTweet= (news) => {
        let tweet = [];
        let tweetName = String(news[0].tweettopic);
        if(tweetName.substring(0,1) === "#") {
            tweetName = news[0].tweettopic.replace('#', '%23');
        }
        tweet.push(
            <div>
                <a href= {("/tweets/" + String(tweetName))}>{news[0].tweettopic} </a>
            </div>
        )

        return tweet
    }

    render(){
        if (this.state.news === null) {
            return <b> Loading... </b>
        } 
        else {
            return (
                <div>
                <div className = "news_header">
                <h1 className="my-4">{this.state.news[0].title}</h1>
                </div>
                <div className="row">
              
                  <div className="col-md-8">
                    <img className="img-fluid" src={this.state.news[0].imageurl}alt=""/>
                  </div>
              
                  <div className="col-md-4">
                    <ul>
                        <li>{this.state.news[0].description}<a href= {this.state.news[0].link}> Read more </a></li>
                        <li>Author: {this.state.news[0].author}</li>
                        <li>Source: {this.state.news[0].source}</li>
                        <li>Published At: {this.state.news[0].publishedat}</li>
                        <li>Time: {this.state.news[0].publishtime}</li> 
                        <li>Countries: {this.getNewsCountries(this.state.news)}</li>
                        <li>Trending Tweet Topic: {this.getNewsTweet(this.state.news)}</li>
                    </ul>
                    
                  </div>
                  <iframe src = {this.state.news[0].link} width="1000" height="500" title="News Article"></iframe>  
                </div>
                </div>
            

            );
		}
    }
}