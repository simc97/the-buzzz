import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { api } from '../../instance.js';
import Pagination from '../../components/Pagination.jsx';
import { Nav, Dropdown, Button } from 'react-bootstrap';
import './News.css'
import { countryDict } from "../Countries/countryDictID.js";
import { subregions } from "./subregions.js";



export default class News extends React.Component {
    constructor() {
        super();
        //set up skeleton code to fetch all the news from API
        this.state = {
            news: [],
            filteredNews: [],
            pageNews: [],
            loading: true,
            currentPage: null,
            totalPages: null,
            filters: {
                subregion: "",
                source: "All",
                date: ""
            },
            sort: "-",
            query: "",
        }
    }
    componentDidMount() {
        //get ALL countries, update state
        api.get('news')
            .then(res => {
                const news = res.data;
                this.setState({ news: news.objects, filteredNews: news.objects, loading: false }, () => console.log(this.state.news));
            })
            .catch(error => console.log("OOPS i DID IT AGAIN " + error.response));

    }

    handleInputChange = (event) => {
        const query = event.target.value;
        this.setState((prevState) => {
            const filteredNews = prevState.news.filter((element) => {
                let res = false;
                if (element.title) {
                    res |= element.title.toLowerCase().includes(query.toLowerCase());
                }
                if (element.subregion) {
                    res |= element.subregion.toLowerCase().includes(query.toLowerCase());
                }
                if (element.author) {
                    res |= element.author.toLowerCase().includes(query.toLowerCase());
                }
                if (element.source) {
                    res |= element.source.toLowerCase().includes(query.toLowerCase());
                }
                if (element.tweettopic) {
                    res |= element.tweettopic.toLowerCase().includes(query.toLowerCase());
                }
                if (element.countryid) {
                    res |= countryDict[element.countryid].toLowerCase().includes(query.toLowerCase());
                }
                return res;
            });

            var index = 0;
            var newNews = [];
            while (index < filteredNews.length && index < 12) {
                newNews.push(filteredNews[index]);
                index++;
            }

            this.setState({ pageNews: newNews });

            return {
                query,
                filteredNews,
            };
        });
    };

    handleFilter = () => {
        let filters = this.state.filters;
        let filteredNewsT = this.state.news;

        for (let key in filters) {
            switch (key) {
                case "subregion":
                    if (filters[key] && filters[key] !== "All") {
                        filteredNewsT = filteredNewsT.filter((element) => {
                            return element.subregion === filters[key];
                        });
                    }
                    break;
                case "source":
                    if (filters[key] && filters[key] !== "All") {
                        filteredNewsT = filteredNewsT.filter((element) => {
                            return element.source.toLowerCase().includes(filters[key].toLowerCase());
                        })
                    }
                    break;
                default:
                    break;
            }
        }

        switch (this.state.sort) {
            case "News Title":
                filteredNewsT = filteredNewsT.sort(function (a, b) {
                    if (a.title < b.title) { return -1; }
                    if (a.title > b.title) { return 1; }
                    return 0;
                })
                break;
            case "Author":
                filteredNewsT = filteredNewsT.sort(function (a, b) {
                    if (a.author < b.author) { return -1; }
                    if (a.author > b.author) { return 1; }
                    return 0;
                })
                break;
            case "Country":
                filteredNewsT = filteredNewsT.sort(function (a, b) {
                    return a.countryid - b.countryid;
                })
                break;
            case "Date":
                filteredNewsT = filteredNewsT.sort(function (a, b) {
                    return new Date(b.publishedat) - new Date(a.publishedat);
                })
                break;
            default:
                break;
        }

        var index = 0;
        var newNews = [];
        while (index < filteredNewsT.length && index < 12) {
            newNews.push(filteredNewsT[index]);
            index++;
        }

        this.setState({ pageNews: newNews, filteredNews: filteredNewsT });

    }


    createNewsCards = (news) => {
        //3 news cards per row. 4 rows
        let rows = []
        for (let r = 0; r < 4; r++) {
            let children = [];
            for (let c = 0; c < 3; c++) {
                if (news.length <= c + 3 * r) {
                    break;
                }
                var newsTopic = news[c + (3 * r)];
                if (newsTopic === undefined) {
                    return;
                }
                children.push(
                    <div className="col-lg-4 col-sm-6 mb-4">
                        <Nav.Link className="city-instance" href={("/news/" + String(newsTopic.title))}>
                            <div className="card h-100">
                                <a href={("/news/" + String(newsTopic.title))}><img className="card-img-top" src={newsTopic.imageurl} alt="" /></a>
                                <div className="card-body">
                                    <h4 className="card-title">
                                        <h4 href=""> {this.highlight(newsTopic.title)}</h4>
                                    </h4>
                                    <p>Description: {newsTopic.description}</p>
                                    <p>Author: {this.highlight(newsTopic.author)}</p>
                                    <p>Source: {this.highlight(newsTopic.source)}</p>
                                    <p>Published At: {newsTopic.publishedat}</p>
                                    <p>Time: {newsTopic.publishtime}</p>
                                </div>
                            </div>
                        </Nav.Link>
                    </div>
                )
            }
            rows.push(<br></br>);
            rows.push(<div className="row">{children}</div>);
            if (news.length < 3 * r) break
        }
        return rows
    }

    highlight = (newsTitle) => {
        if (this.state.query.length > 0) {
            if (newsTitle === null) {
                return;
            }
            var copyNewsTitle = newsTitle.toLowerCase();
            let index = copyNewsTitle.indexOf(this.state.query.toLowerCase());
            if (index >= 0) {
                var newText = [
                    newsTitle.substring(0, index),
                    <highlight>
                        {newsTitle.substring(index, index + this.state.query.length)}
                    </highlight>,
                    newsTitle.substring(index + this.state.query.length),
                ];
            }
            else {
                return newsTitle;
            }
            return newText;
        } else {
            return newsTitle;
        }
    };


    // called each time we navigate to a new page from the pagination control
    onPageChanged = (data) => {

        const { currentPage, totalPages } = data;

        this.setState({ currentPage, totalPages });

        var startIndex = 12 * (currentPage - 1);
        var index = startIndex;
        var newNews = [];
        while (
            index < startIndex + 12 &&
            index < this.state.filteredNews.length
        ) {
            newNews.push(this.state.filteredNews[index]);
            index++;
        }
        this.setState({ pageNews: newNews });
    }
    render() {
        if (this.state.loading) {
            return <h2> Loading... </h2>
        }
        else {
            return (
                <div className="container">
                    <div className="news_model_header">
                        <h1 className="my-4"><strong>What's going on in the news...</strong></h1>
                    </div>
                    <div className="input-group mb-3">
                        <input
                            type="text"
                            className="form-control"
                            placeholder="Enter country, news..."
                            aria-label="Place"
                            aria-describedby="basic-addon1"
                            background-color="white"
                            value={this.state.query}
                            onChange={this.handleInputChange}
                        />
                    </div>
                    <div className="dropdown">
                        {/* Subregion dropdown */}
                        <Dropdown>
                            <Dropdown.Toggle variant="warning" id="dropdown-basic">
                                {this.state.filters["subregion"]
                                    ? this.state.filters["subregion"]
                                    : "Subregion"}
                            </Dropdown.Toggle>

                            <Dropdown.Menu className="dropdown">
                                {subregions.map((value) => {
                                    return (
                                        <Dropdown.Item
                                            key={value}
                                            onClick={() => {
                                                //want it to be Hashtag: yes
                                                // Create new "bar" object, cloning existing bar into new bar
                                                const newFilters = this.state.filters;
                                                newFilters["subregion"] = value;
                                                this.setState({ filters: newFilters });
                                            }}
                                        >
                                            {value}
                                        </Dropdown.Item>
                                    );
                                })}
                            </Dropdown.Menu>
                        </Dropdown>
                    </div>
                    <div className="dropdown">
                        {/* Tweet Volume dropdown */}
                        <Dropdown>
                            <Dropdown.Toggle variant="warning" id="dropdown-basic">
                                Source: {this.state.filters["source"]}
                            </Dropdown.Toggle>

                            <Dropdown.Menu className="dropdown">
                                {["All", "CNN", "CNET", "BBC", "Reuters", "NPR", "TechCrunch", "Wired", "The Verge", "LA Times"].map((value) => {
                                    return (
                                        <Dropdown.Item
                                            key={value}
                                            onClick={() => {
                                                const newFilters = this.state.filters;
                                                newFilters["source"] = value;
                                                this.setState({ filters: newFilters });
                                            }}
                                        >
                                            {value}
                                        </Dropdown.Item>
                                    );
                                })}
                            </Dropdown.Menu>
                        </Dropdown>
                    </div>
                    <div className="dropdown">
                        {/* Tweet Volume dropdown */}
                        <Dropdown>
                            <Dropdown.Toggle variant="warning" id="dropdown-basic">
                                Sort : {this.state.sort}
                            </Dropdown.Toggle>

                            <Dropdown.Menu className="dropdown">
                                {["No Sort", "News Title", "Author", "Country", "Date"].map((value) => {
                                    return (
                                        <Dropdown.Item
                                            key={value}
                                            onClick={() => {
                                                this.setState({ sort: value });
                                            }}
                                        >
                                            {value}
                                        </Dropdown.Item>
                                    );
                                })}
                            </Dropdown.Menu>
                        </Dropdown>
                    </div>
                    <div className="dropdown">
                        <Button variant="outline-dark" onClick={this.handleFilter}>
                            Go
						</Button>
                    </div>
                    <div> {this.createNewsCards(this.state.pageNews)} </div>
                    <div className="pagination">
                        <Pagination
                            pageLimit={12}
                            pageNeighbours={2}
                            totalRecords={this.state.filteredNews.length}
                            onPageChanged={this.onPageChanged}
                            key={this.state.filteredNews.length}
                        />
                    </div>
                </div>
            );
        }
    }
}
