import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios';

import './About.css';



import Simrat from '../../images/simrat.jpg'
import { users } from './users.js';
import { gitlab } from '../../instance.js';

export default class About extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            contributors: [],
            issues: [],
            totalCommits: 0,
            totalIssues: 0,
            totalTests: 0
        };
    }

    async componentDidMount(getCommitCount) {
        //do multiple requests at once.
        //spread responses into different indices in an array
        axios.all([
            gitlab.get('repository/contributors'),
            gitlab.get('issues?per_page=100&page=1'),
        ])
            .then((responseArr) => [responseArr[0].data, responseArr[1].data])
            .then(
                axios.spread((contributors, issues) => {
                    this.setState({ contributors, issues });
                })
            )
            .catch(console.warn);
    }

    getCommitCount = () => {
        //1. get the commit names
        for (const user of users) {
            const relevantContributors = this.state.contributors.filter(contributor => contributor["name"].includes(user["commitName"]));
            user["commitCount"] = relevantContributors.reduce((sum, contributor) => sum + contributor["commits"], 0);
        }
    }

    getIssueCount = () => {
        // match member.userName to issue.author.username
        for (const user of users) {
            for(const issue of this.state.issues){
                if(issue.hasOwnProperty("assignees")){
                    var assigneesObjects = issue["assignees"];
                    for(var assignee of assigneesObjects){
                        if(assignee["username"] === user["username"]){
                            user.issueCount+=1;
                            break;
                        }
                    }
                }else if(issue.hasOwnProperty("assignee") && issue["assignee"]["username"] === user["username"]){
                    user.issueCount+=1;
                }
            }
        }
    }

    getTotalTests = () => {
        return users.reduce((sum, user) => sum + user.unitTests, 0);
    }
    getTotalCommits = () => {
        return users.reduce((sum, user) => sum + user.commitCount, 0);
    }


    render() {
        this.getCommitCount();
        this.getIssueCount();
        var totalCommits = this.getTotalCommits();
        var totalTests = this.getTotalTests();

        return (
            <div>
                <div className="about">
                    <h1><strong>About Us</strong></h1>
                    <p>Do you ever wonder what is going on in the world but don't know where to start with all the different platforms these days? The Buzzz makes getting informed easy by displaying what people are talking about and what's in the news based on country.  The sources we used to make this happen are below. </p>
                    <h2>results</h2>
                    <p>By aggregating topic trending topics on Twitter, we are able to inform and find associated news articles that provide useful current topics and details occuring in each country. </p>
                    <h1>APIs</h1>
                    <div>
                        <a href="https://developer.twitter.com/en/docs/trends/trends-for-location/overview" styles="text-decoration:none; font-size:large;">Twitter API</a>
                    </div>
                    <div> <a href="https://newsapi.org/s/google-news-api" styles="text-decoration:none; font-size:large;">Google News API</a> </div>
                    <div> <a href="https://rapidapi.com/apilayernet/api/rest-countries-v1" styles="text-decoration:none; font-size:large;">Rapid Country API</a></div>
                </div>

                {/* Page Content*/}
                <div className="section">
                    <div className="container">
                        <h1>Developers</h1>
                        <div className="row">
                            {/* Team Member 1 */}
                            <div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-0 shadow">
                                    <img src= {users[0]["photoURL"]}
                                        className="card-img-top" alt="..." />
                                    <div className="card-body text-center">
                                        <h5 className="card-title mb-0">Asu Dhakal</h5>
                                        <div className="card-text text-black-50">{users[0]["role"]}</div>
                                        <hr />
                                        <div className="card-text text-black-50">{users[0]["bio"]}</div>
                                        <hr />
                                        <div className="card-text text-black-50" id="asuC">Number of commits: {users[0]["commitCount"]}/{totalCommits}</div>
                                        <div className="card-text text-black-50" id="asuI">Number of issues: {users[0]["issueCount"]}/{this.state.issues.length}</div>
                                        <div className="card-text text-black-50" id="asuI">Number of tests: {users[0]["unitTests"]}/{totalTests}</div>
                                    </div>
                                </div>
                            </div>
                            {/* Team Member 2 */}
                            <div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-0 shadow">
                                    <img
                                        src= {users[2]["photoURL"]}
                                        className="card-img-top" alt="..." />
                                    <div className="card-body text-center">
                                        <h5 className="card-title mb-0">David Ko</h5>
                                        <div className="card-text text-black-50">{users[2]["role"]}</div>
                                        <hr />
                                        <div className="card-text text-black-50">{users[2]["bio"]}</div>
                                        <hr />
                                        <div className="card-text text-black-50" id="davidC">Number of commits: {users[2]["commitCount"]}/{totalCommits}</div>
                                        <div className="card-text text-black-50" id="davidI">Number of issues: {users[2]["issueCount"]}/{this.state.issues.length}</div>
                                        <div className="card-text text-black-50" id="davidI">Number of tests: {users[2]["unitTests"]}/{totalTests}</div>
                                    </div>
                                </div>
                            </div>
                            {/* Team Member 3 */}
                            <div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-0 shadow">
                                    <img
                                        src= {users[3]["photoURL"]}
                                        className="card-img-top" alt="..." />
                                    <div className="card-body text-center">
                                        <h5 className="card-title mb-0">Rudra Garg</h5>
                                        <div className="card-text text-black-50">{users[3]["role"]}</div>
                                        <hr />
                                        <div className="card-text text-black-50">{users[3]["bio"]}</div>
                                        <hr />
                                        <div className="card-text text-black-50" id="rudraC">Number of commits: {users[3]["commitCount"]}/{totalCommits}</div>
                                        <div className="card-text text-black-50" id="rudraI">Number of issues: {users[3]["issueCount"]}/{this.state.issues.length}</div>
                                        <div className="card-text text-black-50" id="rudraI">Number of tests: {users[3]["unitTests"]}/{totalTests}</div>
                                    </div>
                                </div>
                            </div>
                            {/* Team Member 4 */}
                            <div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-0 shadow">
                                    <img
                                        src= {users[4]["photoURL"]}
                                        className="card-img-top" alt="..." />
                                    <div className="card-body text-center">
                                        <h5 className="card-title mb-0">Shivani Revuru</h5>
                                        <div className="card-text text-black-50">{users[4]["role"]}</div>
                                        <hr />
                                        <div className="card-text text-black-50">{users[4]["bio"]}</div>
                                        <hr />
                                        <div className="card-text text-black-50" id="shivaniC">Number of commits: {users[4]["commitCount"]}/{totalCommits}</div>
                                        <div className="card-text text-black-50" id="shivaniI">Number of issues: {users[4]["issueCount"]}/{this.state.issues.length}</div>
                                        <div className="card-text text-black-50" id="shivaniI">Number of tests: {users[4]["unitTests"]}/{totalTests}</div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div className="container">
                        <div className="row">

                            {/* Team Member 5 */}
                            <div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-0 shadow">
                                    <img src={Simrat} className="card-img-top" alt="..." />
                                    <div className="card-body text-center">
                                        <h5 className="card-title mb-0">Simrat Chandi</h5>
                                        <div className="card-text text-black-50">{users[1]["role"]}</div>
                                        <hr />
                                        <div className="card-text text-black-50">{users[1]["bio"]}</div>
                                        <hr />
                                        <div className="card-text text-black-50" id="simratC">Number of commits: {users[1]["commitCount"]}/{totalCommits}</div>
                                        <div className="card-text text-black-50" id="simratI">Number of issues: {users[1]["issueCount"]}/{this.state.issues.length}</div>
                                        <div className="card-text text-black-50" id="simratI">Number of tests: {users[1]["unitTests"]}/{totalTests}</div>
                                    </div>
                                </div>
                            </div>
                            {/* Team Member 6 */}
                            <div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-0 shadow">
                                    <img src="https://miro.medium.com/max/3150/2*zTT4mmYgerP77ET2tKUPpQ.png" className="card-img-top" alt="..." />
                                    <div className="card-body text-center">
                                        <h5 className="card-title mb-0">Zelma Garza</h5>
                                        <div className="card-text text-black-50">{users[5]["role"]}</div>
                                        <hr />
                                        <div className="card-text text-black-50">{users[5]["bio"]}</div>
                                        <hr />
                                        <div className="card-text text-black-50" id="zelmaC">Number of commits: {users[5]["commitCount"]}/{totalCommits}</div>
                                        <div className="card-text text-black-50" id="zelmaI">Number of issues: {users[5]["issueCount"]}/{this.state.issues.length}</div>
                                        <div className="card-text text-black-50" id="zelmaI">Number of tests: {users[5]["unitTests"]}/{totalTests}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="section">
                    <h1>tools used</h1>
                    <div className="container">
							<div className="row">
                            <div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-0 shadow">
                                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAP8AAADFCAMAAACsN9QzAAABIFBMVEX///8VcrbkTybr6+vxZiozqdwAAAATExMAaLLjQADO3u3419ATb7Q0q90AarPrXCgnkszCwsLoVicyMjLr8fIMDAweg8EnJyfxYR/kSRxTtOD39/c7grzwWw3mh3LJ1uH1nHzycD7q3drkRhLc3Nx/f3+cnJzd8PnpwrzkWjnw+fz+8e0se7rb4+fJyclaWlrkVC4bGxtLS0tycnKRkZGGhobX19f0wbb75+I4ODi3t7dtbW21zeRZk8Ysm9LpdlrzfVDotKqFx+jG5PSh0+zysqbnlILxajHwo5O0yNv3rpXnZ0mXudlsnst8qND2zMOrq6vhLgAAX67qgGj0j2mivttpveTpcFP5wa2y2vD3tJ/vnY3qz8n0hlyQzOrzuq0UOnvSAAAN80lEQVR4nO2c+V/TSheHB3qtLaQpXKtQwqYF2QqKCyCyuIEo2/UiLhev/v//xZu1nTkzmTlnQnj9vO98fxFiyeRJpsmT05My5uLi4uLi4uLi4uLi4uLi4uLi4uLi4uLi4vJ/kYe3x8bGbj+Mf34R/jhWnWOVaBmX9ZeMPV+PfnqR/tXT+LcxxoJq/DfXtDHBHT5jzxirrN8Rl4VbWhm7I73OOg+rUZI13I5/DvmrII8Ym69yL1xLl0f86Q/XkqB6l0v1ecgqLAmXPVQse15gSJ5/3cwf/m/vhWXw3+aS8N8WkvCDZTfJ/yR83bNsefn8c78bf3Wb+++S+LPVz8fviIyymm1psF4Vl80XGJLMX2WPSuavjq4lid5rG8kOuDs2V4kTsPgEEOOvp8sqBYZM+JOzbDXlZ/FKnySHOx2gz/+iWjZ/wC9bS+fEI27ZXLID4jdjwTyExzo5w4VJDvNa+tu89Lob4mej6Q54yS3bSJcV3wG/Pz/bTmGfcstGFbPCKjT+bO6/uEn+cBvl87xqp9iExj+aniM2bpSfPU9hedHLdkqRkz+j8q8l4NuVm+VnT1PYv7hlzxQ7hZ6Ef35uY2Njbt3IPxr/+yQ8AYv8T5O8/CtvGFxy+Xs7YJtb9lyxU8hJ+MXrfxI1f3STUJH4sxScjAn/3SdpItd7Ge/Y+flUeaLtCZ6my1I1EnYKNWT+tWh/l8p/+266tue9gaqZ8d0dY309y5atFxiSzM+i15bLz3k9dP27LxTLxgoMSeeP8r/Gn1xZqwL/E4H/6c3ww7XNVUH4+d97HxQYcm57NEzCHP00up2dfdfi37J7iw3htyD+q2hvJD9k2SiwJb0t4NcWjIKsKZaNmlfs4uLi4uLiwtjKcLlZMW5Ba6jcLGtH3/xULzOfho38O3/XyszfQ9rRh+uDZaZuPv5DtYEyU2tpR98qmd+IzxbK5ff183+lUyp/28y/7JfKP6AfPSgVf3DJzB+USu91DcM/LhO/vW/mZ12vTP5dw+jH7TL5LxD8k0R+DxUs/z6N/09kUv5DBP8ujb97D5Vuyn9mGH2axt/8A5Vm8urOEYL/lMTvjTRQGUle7p8YRj+iXQCI/JsI/hPSBSDkv4VI416yV/0dw+gHpfCn879+juC//K/yb5bKb9bfUIBL4U/f/zW9/pIFGMmfvrq+heCnCTCaPzn+Bv0lCzCRX/EJnhSaACP5b2Uv1+svWYBp/Aj9DW+Ay5j/Gf+A6QgEpVz/0lcj9De8AaDgI/kbt5Lpb9Rfxkj4NP72Vww/65Zw/PdS/knj6Esl8KeXf5T+KgRYK7YjqOmf8Zv0Vxbgtjbj2kD+aRQ/FODupC57E7o0AL9JfyUBbu9P6zKrzSuRH6W/kgD7J0FFE/0J7V36/h9J9cekv5IAd14vtzTRX04+jov8v1D8QIC9Mx1+RX9BfwP4L42jAwHuHLXs9/7nlD/VP5T+SwLsnRbg/5LyY/VXEuD2dAH+bP5T9FcSYG9SO//1/BMZf7Iuo/5KAty+KMD/Xrz8o/Q3FGDA3y3An57/evq/YBwdCHD7awH+ccBvrn5HWYUCrNsAHP+tgfT8b9LfUIDBDcCSdnQt/8qUyN/G6L9CgFet+Zez63+2KjN/AG+A7Pm3AD9Kf8ONBtd//xr401Wa9ZcxcAPUeWDNPyzyt49x/Awc/tqMNf+CyI/QX6kCXrfnP58S9Q9T/Y4CKuD+B2v+t2T9ZeyrKID1H9b83wA/Tn8lAfYvrbfgO1l/JQHuLPIXAKB/+tH/AfqH019JgL2zGU2GFkD4Uxxdfxm7AvyvOf4fiyDnug/7Z630l7EzcAL0/PzU4P3f3ltuTXT91Qpw6+BTR8yULtnln6a/tAq4fP/PKw5df2UBPuT4F2F1DHX/T9NfWgVY5ufnP11/w7O2RoB/QDlA8WdnUpz+SgJ8DfyZ/q4iRoc3APsc/4NC/Dj9pVXAZX5+TUB/fVP1OwqsgB9Xrom/jeVfKHL8G9yKoP4i9D+MRoBbRfgfI/ElASbxT6j4sxWihgeMdZ5/0J4frb+SAJP4v3DrSfWXUP2O8ifg5wS4BdsjMPxU/SVVwCX+n9x6bPRXK8CtiwL8WP0ltYBI/O+49UD9PUWNrhHg1qE9P1p/SS0QUv3/O7ceG/1l7DBfgFtXHXv+AzQ/FGCAzMeH/Lz+/gT8GP2VK+AHHP/rx0tC7muSVf+o+msQYP/DqpBlMfx6bPSXsV/5Agzv/7Q+8a9Y/UY1fyTRtoD4YjlEtwk9/uQvUforC7CuAq67/70vHn+0/hpuAEA5RMcP9RfHrxNgCj+sfmP1zyDA/gmaP7s0pAUl31z9jiIJsB3/Cqh+o/XXIMDg8zANfwD018eNTqkA6/hB9Retv4YeaPB5mIYf6i+SXyfABH7L6nccLf+u8HmQht9Of0kVYA0/rP7imj+SaPDh52Eafjv9lXqgdRVgDb9t9TeKVoA9LL+d/jIGJL++aMUPq79XBH5tD3RN2AINP9BfVPU7ikaACfyw+QGvvwYBFj8P0/BD/cXpv1aACfyzoPkBW/2OohdgQQA1/Fn1m6a/CgF+kJutFSH8Wuz119QD3eWzNyH0uSur3+l+w+mfJMCD7U5uQMWf3wGw+QOvv5QKOLz/1VV/sfyEHmjx/necX4td80cSfAUY1j/4tfSaP9LeZ5z+knqgRf733EoCoL8dvP5SeqB1/OmiRvrSGnZ02AKC5n/FrwTo3yABny3b8gvV3z1Rf9H8UgUcyT/+mVsH1F+C/oeB/KpnuVT8iuovqfkjCXwI8DEXHf8stw7Y/EHRX8ZgCwx/yROqg4D/DbcOqL+Y5o8kUIAf9HtAl0U5FPk/cuvYBPqL633OAgRYqHmcafh11W+s/moFuDWt4f/GreObqH9tiv7qW0BONPx89TfTf6r+UirgAv8UrzhQf/HV7yhAgD2+5iPIEeBXVn/TfYjVX8auNBVgsT1C5Oc/4Ldt/kj5NT3QH5D8tvort4Bc8RVwDT+vOJ8L6K++B3qmls+va/7A6h+lBUTkF/QX8GObP5LAHuhdHP/edeivvgdabAERz3/8OrLqd/pCkv5KAux1Of5VzfHn12Grv3IP9HFuC4jof9wqCumvQoA5/gqSP13U019c93EUSYD5AkAu/ziv/1B/OyR8SYCFms+u5/uekp/Xn6z62yDrr9QDXOf5D5c6nbbEPz41/i9/+Yf6S9J/Jglwja/5BJWZk0nPj0W4zz/xTpzg9vqrrwC3Wg9eTz+uJ/ugmbBPvfoIzm+2vc9ZgABLPdDB6oezbjgNkvv/iZ9vpdYee/1VCLA4eKtV+XGwPxjug2bIfn/2XH5zw+o3TX/1Atw/E17uDtxrvPmubGuy11/pIUBVBTzaB0dfm5+/qU/ssPqN+eYLPlCAc3rAg0puS5+9/hp6oPl9kHtOLaa/iocA1fyaCqy9/so90LkV4NzRYfWXUv2OAm8Ach8CzN0Cm97nLOiHAHNHL6a/8kOQuQ8B5m6BXfNHEo0AI0cvpr96AcZtgU3vcxZdDzRu9GL6S3gIMHcL6I/+9QMr4MfR6Z4yOmx+IDR/JIEVcG/g9FL5HJhyC5a/f8nsp6e/qN7fbPOlT0D2j36o9oFy9OGPrzL76fET8VlLqgB7fm3yTL4Mylvw9ucEp8SZ/mKbH+IE0reAtDv1x9OvH7TAPpBG3/r2OVTh3pP/lvrP5ApwvAu82sDu5YxmCxb6B76A/uZ8DVy4D44PF4VpIIy+cj57v//Ui6C/lOaPJAr8bBp0zz6sBvIWLL99o/guDBv9zf8auHa73t4/6L8V+qMP//MKshfQX20LhBfe+0yezFQCbgsW3k3I7Dw/tvkjie5r4Hpvhd7o4aT/g5/0Mj9Vf41fAxfeAkdnxCDagmV50sv8FP2VKuCKfdA5vlqMP3w///heceBFfrL+onqgw30weTb0M+fAA/2j6K/UApIzDToXR6pJr+Cn6q+pB7q/Cwzf/2Klv+ivgWsrJz0XW/1FPwRo+v4fK/2VWkByY+p/t9VfdAuEmT95HUl/5RaQovxU/b0+fhv9xbeA6Pmb2aNEZP1Ft4Do+BuNW3vp4afpL74FIp+/2eSeoyLrL7oFJIe/EbGPdAf6X/xKGx37NXg5/E3wDBnqi+/EBAX4Q/iRewNCowRNf9FfA6fgh+yDVvqLfQhQfv4zmvRdD1w9ifqL/ho4yK+AH7TSX+z3QIv8jUYy6aU/JepveAOAewM0jexROpTe5yyTqAtAnz8528nocXxK9TvK5n693jFLYMbfbOa9ItZE+uU/vADu+jXfOAdi/qjxMzzwiuMev8Sveae0y3+c4aMl4y5o6g98u945PrKBTzJ00vUNuyD+/mfpbMfD+5MnFvBJVjanB7X7oKlhD28TDxWfDNHS2jkd0E0Db0Rxtuuzd892aBd+OVsHXzt14rfCh+zt/QO69KkzdDLp1XIYB2BDYH/SD+xe0qQvNyvnh+a3Ag9/fDWM/7Qdk+Wdsy7ibNCHD2+Mix54MVu/LtrmfRCyD05vFp306ixc7nrmfRAd+NMdzBdd0DN8dKzbBe16/fomvTrhW0GzC7wQftf+bIdJeEZUToPkbHe9k16d5Z1T5TSIC6M7N7EF4Rmxzp8RQ/b6xa9yD7wYOA1C9tq1ne1QOT9ML4wh/FKBS7x1gp2z9MIYHfhyJ706W7/2w8tcWWc7TMIzYny2u94zPSU3OenVuclJ7+Li4uLi4uLi4uLi4uLi4uLi4uLi4uLi4uLi4uLi4uLi4vJ75T9ha2Vg1ZlAygAAAABJRU5ErkJggg==" className="card-img-top" alt="..." />
                                    <div className="card-body text-center">
                                        <h5 className="card-title mb-0">HTML and CSS</h5>
                                        <div className="card-text text-black-50">to create our static frontend pages</div>
                                    </div>
                                </div>
                            </div>
							<div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-0 shadow">
                                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQcAAADACAMAAAA+71YtAAAAk1BMVEUiIiJh2vscAABk4f9j3/9k4v9i3f8bAAAhHh0iICAgGBUgGhgeDAAfFhIdCAAfEgxdz+5f1fVZw+BSsctHk6hUt9JNo7oeDwcdCQA6b349d4daxuNQqsMmLzIkJyhFjaEvTldBg5UtR084Z3QzWGNFjqJKnLImLjE1YGwqPkQoNjo2Y3A/fY86bXswUlsuSlIVAABRKQBNAAAMt0lEQVR4nO2ca2OiOreAJQkh4RYuIgqoVam3Wjv//9cdsgKKiDP7/bBnCyfPh860YmsWK+seJhONRqPRaDQajUaj0Wg0Go1Go9FoNBqNRqPRaDQajUaj0Wg0Go1Go9Fo/guY58894fuC/elK7qTI9YTl2vxvfLC/Cp8fLnEeBHmx2frid+tjc+c4TapLo9nO8/7aB/w7MLE3MKWGQSnG+Xrlv5IEQ9tZaJLqUkqJGSzQX/2Y/zbOOcDGDYrpfpL2SYKhnwTT1pVmgUa0N9gyhMVRSutFErJO7afr0mVh1hc0l+LE/w8+8L+EGxG5dhoVRVQpPSwUB2XnVjO0qcVEcJAUcQCaYU5HIwixMeXCZ0vk+4gfZwZWu2PvOu2rzpGp9kLwuU3lpR8gPrz9o4MZBlzA/d9YsB7u+JMpBZ3A+VncrkI7Q22EYOGmauWOn1Q/IvFIbKV3qe4/KazbD7jLZ6DzlO7qNXK0V8pgXNDdVzI7lDtjOQ6FQFG1ZvywGI6uyoGYazASjpfAtzhepe23CilCvBaTEcBX1SagXeW2vRkogDmrBGEvcwLqkaHHW89T+eNkFBvD/pL3NOtGhhwtCFWOUZzArZLglHbfjGIZe43CY4h1da/N8/MeT08h+IPox1ACeXAf9Zs3Uog/YzAQ7qxabfh0pyvsFThGqizFvi9ydI5SDrvnkGt4oMr50bxXtZlImnDb3PQaAXaurAi+jCHdku6CRv2mjqNCRZcke3EBq+RARuEwUEBfm3z7oBKPyOpPp7hdyYlM+3bV0ECBdJtW72tsEqiMAhf9F3BP+ov9KOSQv9QHLiLS2Idp/xU2Ho0+vLYPKAbHCUGUmfWZUj4Zj3146S/Q1ARNsFReWfZ4hRH5i7l0CWHPHU0XkI4XiK2UleiJtZxSxg/fY4gf0n21SJM9+QNnCyln4vOJcwKnkbtPF3mZlEP5HGgOD0i7zadiCmNSCWjA5AuirFWj+2YIykmPogwPB/KsRVe1lY2kJ3Wr/QuYiovbucovpLBGkW+ys7ylnx0DMYdanfndeESrkBE23XZ2ADjdF8Ho0EBGN5DiTPyANnz+QghZFpJfI9gmrv1gSVwpw2L+tz/yv4J0nEYIcuCOl/rImmxL5SGSKA+CMAyCIEoiqEEU17Ow0FzYYBKcq9xTmzGED5XDmFY3FZ9lb9P5yKZxZBBcN2tuDY3bfwgmJEyK9eKU+q7nyvKDeR2Du6hyqW8oSF3XcWCamNBWv+oFlBBs0mh2OclyFPltO3QwsNQFE9gjAfrAszSw7G7R5Nfw+95cWOdNQjurV5kVDYM8ihJFVFmKEJP2BQ1h8e2gQUeUjjXZRPjeuK3uMMZGBBYRX13h+j5q8OeuQFPQnCSvrmtpD8E03vnuQIMp7vnHmLSEUElgdjmurF+BFENfns39XNqD/S903q2LwLj3yKkZ7k9oiPZSiCy/N6+rL/nWQa7ncEvedBrM+/a88yOXbl5tZgsfrXa0tUsIicvBScJBWdD4RkxUbLBSIQGkV2bZv+HRXgqpztLFQn4T5yZpflPygYa0Ozi65koKFOMk41Z2b+VAVEVmL0Jl7soAC28g3FZNnAM6rxvNomaxGk5Q5Xgzs5ZCvpkgj7Mlbdpz3rfMK8LnNLzGPkK7T+oO57IhWCUXTFjbfVgLlmZDmZAR26AecEiOSMVAoAT4wOT9lkYyu9/UKtT27JayQx4K+qJqDyqoZnPnEijhmrE3iL3hf4Flo2a1mRsjADUIuaR0jY12Asl8r8wuix90ixnZGezHiU0g85LCA7hwF0q+JD8MwFzOF6qDHxxbxp0zuTFyi61gkT/NK+l5ZmBJsL41N+fgTxKLnXCn112lG0rE4fntBSGOSgz7xwgQyXqKebX2csvfyk5ocwsvcHit9wp3YfjjiCA9W7RLtNw9RBBpBas33xpKq6nx1fEHjjR/ZAbdK9qoej0D08Sax1oQXmZCh0uKw0i7s2QQcpJ3L82AQaTh9sm5ya5WlSnIHT+tyyoia4lBSqIpQ8oilEFlm5zuu4W6CVIVvPeuSdhf8kPS7XO/AQwkzDmEXN1iZSvackish99i9JV35WYC8Tnv7D1BHcxFTxuOHepV1zFSXZ554FZwQXWG2t8OtKRnfesiFViHVz28usMfNsUEP+iI4b4LoHsjZfbd18hiS/Lyr7wHYA2fR6HUax/4QR3YqasOlWO9ORKlEEF/fxfi7dch6X8PTDPJEKgPtbaw6Vk5pdmVw316SCkEeaH70NyhL/7MOyA2L0bCJP6etmyhmnzqyqGJObgDpZoXSSn8GfLGcrAXr7uRfKVs4Uf9ar1PHgiafeGCDX01ReNKiRrL990XTJZR6Kx3Jgz2TGttfPIkhluzhymZvbrnEIsEb2wnJ6n8hKR3HtptJoC6zvHObc5hXrtUsu/rZKUZSPspwnojUsiR+oa+VGzUVgjn+GQovTrCOtwk1OMUHJWQfryveWiUHfccJqq9hVzBsbZ+VkchzEvtHnwIqYO2htxxYGiEvPnMtcoZcNEtsrMlKMoOt6KEZgym2RVNGsrOYEm2vTGZOMOb6LuPRCDo3+No+RgCgcvHFyuHuLu+yQ+n18yiiSxU3Dz9BTH6Y4LBrS+VdyzeOKoGuJ+oIwSXhwIEguEX4YFNuMdSYk/kaUZKcHgrO9YBlueB8rQTTp5OCnVYYf3eu0LSzEybeasgBfEhqRwq2AR8O3rG/eUmlodbF+nNEEAvB1e2AklrEt5WzIXYGGQoYpAlx/qIEU5KVHsAiHzMKsBiW/NxdzMhW3tz72ZY4QgOldEBvAkflTCZ613q8i8dyOFWjhbqHBrFeebO5TpSeWsDqQVI+oLfHD1jE/ApX7ZsbplKiSbcQ8tpXbjH+fNplXdFHOKmgUFnpeumsC1gqzPwrObu1WRoW05gVELh+WyR1Kc+CVkPqbfHrGPd0DKIGexLWZ6tI59U6r0Rev3ZgQ1Gss4l1SDNbhHTW4swPg9sVsr2F/c2Lywjt1Lo1yg7uO8fK/eDmx11PHSC7Bvffk08rPamwvOPjTYrkun3qTKJaFv3tHveg1Qv3Ep95H5ks/z+ZrnDhtn4r3QCnafB4xQIjYr1rlATgq4Qnm07Ctv2hECQi5NLNo1zLEdB7u+MLt58eLrQwFJU7tuPPIARMGVCk+l6k31/leX1WpbH3eKy+ZyqQJs8jlJRGq1P1guDMhgc19+qCNN4REpEYap/CHm6BFKJaIn+/CyVAQBxAI2T8J8NDd6lZAaFtBBvXXH5H1BnUa+Wv/z+jHOpBc93vq0D8gIjKjZHZlnT3xQ8h4aamZ/L4/tVEO2fjtl0FtfFqRbqJ2E8Wy/KZeVXhMMn9g53e73DxQoeZuaZ46WupSYEs+vxayc5lj8zKFF/WK6wnUYB2AGP5bieqsGTbjkRqfF6x6s8ZoXjl3BYbd2p8cqm90jO+6uZ+W6BjS2V82yKleeHb2+0ZvWHzoumhviCwpWKr3kKrf7wabwDDndRPvDYARCf0uYfnmy+mugwL5CNq4Ocz7G2Gqwaz3MPjJ56IoIKHinFg0geUQ/R+Br0lHmNX/nIKpt4foF7Kow+WfVzlXrMIdSvXnTQBwac5436en31sc0Qav006Tvyzw/m6773sPjNOXcBzhKKrzSY9BkB7pijOef+m+c/zJtRMWr0j0RyT5bpRhFI/fY5GGhTV2p6Rssk/z+e/1A5kw9VjMjt/lLTmPbFSzspZ7FrdaDBodc38tVo7CSkm31+sxJRpvoc4DVOfYtVfnMUCSeExrhn7JNbU9X4itWIadmjM2N67sGLpTgihriBHn8peZib57EJ6O2R1RjyC7btfdTNfBtA3GBc02ZU2CycrpGAQYFgFPmm6lt3iozMqo9SBCe599ECylEkuD6qBFMtzncehfrnuLLShLOWQjB0itR0bcLUfnE/4Jl71NyL9mluS9bvzOMYzENt8w1j2eg88w/7une9v/Xp7JWSDAkv6a1K78PZnrHUq1WCXe0AZDuOnaLtvh4LCHetFTJrWgsn2ByQqC71rAXMAF3GED1I1Eg5pcV3WX5P87pob8ado5iobM75Gcnm61peYLCG5uOwDpIUztAY0L+qpYDD76fetS2mzYl/0lTyafMknVGA1o8zo9j4nPfEiNw/x/jhOAKl1zHEkjfQwrg1vokZfK6eH5kFMLQt7ldSMzqNSgyTiVh9BtDPNYNi5/U+5F/BrMMlMUxo/0aLnkfWDhyepuevS3Y8C/8PjzlhnuWcdllWrvyRKUMNsz3P+UcleA6XjiGr0Gg0Go1Go9FoNBqNRqPRaDQajUaj0Wg0Go1Go9FoNBqNRqPRaDQajUajGQv/B+T9uzrn7A7xAAAAAElFTkSuQmCC" className="card-img-top" alt="..." />
                                    <div className="card-body text-center">
                                        <h5 className="card-title mb-0">React</h5>
                                        <div className="card-text text-black-50">to build a dynamic website with reusable components</div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-0 shadow">
                                    <img src="https://yt3.ggpht.com/a/AGF-l7_xQXUi4SUYUw0ncIZXXmzEzUr926q2ZjoU-A=s900-c-k-c0xffffffff-no-rj-mo" className="card-img-top" alt="..." />
                                    <div className="card-body text-center">
                                        <h5 className="card-title mb-0">AWS S3 and Route 53</h5>
                                        <div className="card-text text-black-50">to host our application and manage our domain</div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-0 shadow">
                                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAA9lBMVEX////iQyn8bSb8oyb+cCfqWzXgMg30w738nAD8ayT8aCPiQSb8pSThQCj8oiL8nQDhPSDgLAD8bCrhOBj8qCr8czD8nxThNxf+9vX//vrhQivyt7DgLwX/9+79y5P8lzv8ojj8pzH98e/odWb76ebna1vuoJf64d3jUDr8nDvvZzz8fDb8gjf9wXz+583+8N/2zsnqgHPlYU7zvrftl4zrjoP2cTrkTTL+2rL8kDv8sFD8iDn8s1v+6NH9uWf6dTr92K/9xYXkWETwp5/mZlTqhXnpemzue13qVy/sZEDuWij9myP8q0H90qDnWjztZD79jCr1iVqqsX6KAAAN9UlEQVR4nO2daVfbyBKG45EBeZMNGIEdTDBLSEhYMoQlBELIMndmLiS5///PXC2W1JKquqtaLdnk+P06nD5d0qN6u6ramWfP5pprrrnmmmsuunZ2pr0DQSVsZm+31vu2ZHxZPZ3ut1r7B2bX3FvttWq95edmV9XU6XKv1eotnxpddLdX87U4E6Retvy99L6YXHNvsRZGOAsv8XQ53IzRl/h8EmFv3+CiujpfDTez+t7govshpN5z2zC4qqaivbRemVtzZ8KFh+mZuVU1dRBvZtlcOj1bjBadAUzPo3dYW31pbNEYUi/EqWfTMJMGmA5NrbkRczEDmJ4uJpsxhumZsGjvL0OL6ur9arIZY5juJpB6ZEwZ0y8tYTN/mllzpyMsWtuc7uH0YFPYS23dDKZLi+KivXdGFtXVy/VUhF+NLPquJy5aW52q6YuQeo/7m4k1N3qpRc0eB7k6WE7tpba5Z2DRpc30olPF9Ot6ejOLJjD9nobUg9/Aorp6lebJTDbNPLWpYrq3nNtMcfM6zS3a+25gr3rKQmqkYs1BWmtdTi2bfmnlNlMc01Zu0dritDA9yL1Cz7yKZtPTRWBRk8U1R3lIDWB6noPUI8NoD4ihfWAzvd1ia25k03Mgg8U1RzsAT4Ur1oNNaNEpYfocjLBgxSpWYwKmxoprliBIC5+xLiFIp4TpTs6ZJy+xiHllD7oxpuZ6QHTBkBasWGFIzbYqydqHeSrWWBnCa3pkVI/pBmCGoZb1F8Ug9eqL6jE9QyAthOlLBFJzPSCGdsFM6qtANs0fdJOXWDWmO8ABOXrcQ91seoByYawHRNeSZDPapQAOqffcjPSAGHqHQlqgYv0mWbS2bKIHRNeGbC+tV3qY7oFn0piMasfBS2haDx63HqZQNSY8t2pLKBmk3hnrXGvRP/FMGjy3SgcYUp68562zZr6vlValmOb7YZnHrYMpdtCNH1uV4+B8Pyytnk7FqoDUQA+IoaFiMy0NTHcU5FeKKdQPS2uTf8ZSQVq8B8TQueTsEUqjsYJVY2KIVWVTuB+WEt+89hSftq/Kbi2cKr8YjYpVDWmFczas1SCKXbH+RXiHtc1S4skL6YelxK1YNwiv0IuwGkzxVoMoZv/vjEB+ZdmUAim7YsVbBulVK5mzXZL2wuv/bZC4qGjORoOUWbHKq7FElWRTWatBFAtTUiatVTQOlvTD0pthZNMB3tfKqAJMZf2wtNbpmKoPupEquLXwUtpqSEVIx1TeMhBVAabfqDwxLjBvUM4QE5V+uWaPzBOjYqUcdONFyx4HUw7IkcgVq7oaE1T2HTBlq0EQ+YxVYyxaNqbY4BfZDC0rqPpaaWm2KqniQErGlAWpl01LjZADKbn/R7aKyXMrc862RzbDUCRMqQfdSKVmUx6k3uOmjIPfM99hqQMM+AoNLlI2Vfe1MtJoVVK1x0oJNb8zrO7/0Q+6kUq8tcCFlIQprWUgqkRMqVVcIkLFSq3GBJU2Dsav0KBS31rgZlJfpd1ayP4QgiLlGUs++IVV2h0wYj8sJWXFqgFpeZhq8KQsBVSDX2TRcjDVgVSJqQ6kpd0B42dSXwpMtSAtaRy8oRr8IurIFuW0DESVMg6m98Mym5Fh+nW9o6VSfsT+fVVvM9KK9T9/L+jp77Ml0zr7R3cv/wzQAE+slbqm/l00rX91t9L87zEa4WP3dfMPLTXu0AvTmupcNPS20nzh3qMR3jjtD5ohrtFGYHQN1zQD/Nh3HjBMR13L6r/Ve3SNw47RADs/63r7WBtbln2CRHjlRWiNV/RWvjMcoSakK9teDO4WEuG14/1XZ1uP07pZTC/1IG2+bnsxWG0EUtsK/usLrRDNYtpZ0IK0+aEfxIBgehVGaPU/aoVoFNPOhc4WGm/CADFMb5zwP1vjNzrfwNqlwRC1IA2yTCDnAYTUtSJpZZu6QUz1IG1ut6MIQEyDTBqqrWP8jQtzpj/UyaTNF3GAlvsIRPgpeYeWlvEbNP3hGj9Cz+qTAJxrAFLHEqRj/PUFY5hqQNpY64sBdPOYCpAGnyL/KTYuTEWoYfeNwOoTAZgeuam/aGsY/5qpD1HjTDqxehmm6QC1jN8Upp0fbEibH9qZ/dujTIDHduYvNIzfFKadQy6kjbf97Pbtz3JIgxDZxm8om7IhTaxewDRr+vkANYzfDKZ8SAWrRzHNQ2rpGL8RTDuHzAhFq0/kpjE9coC/4Ru/EUy5kDY/QgFazo0Y4GAMRsg2fhOYdi55r7DxJv8RhiGKmN524T/iGr+JbMqFdAUJ0OpeCRHeQ4kmeA7brGzTMGD6wzvWQ81avbD3T2pILXa2MYApD9KoqofkJpjeQpl0Ip7xF8eUZ/eA1SeyE0y3MEiDEFnGXxjTzh0nQMDqEwmYSv7KYhp//UfBl/iKA2mmoMi9xCjAEwmkFrPMKIppiwMpbPVChFcUSC2m8RfEdMiANFXVQ4oxlbEcqP+WHmJBTBmZNO4dSkIcUCD1xTD+Yp1hht03UKtP1D0mQWrxjL8QpgxIV1CrT+QeBRFeo3afiGH8hTCld4LzVT30ZsY+pifYmTQdIjnbFOkM02dqUqtPZPuYPqoh9UU2/iLjYHKTTW71iQJMKZD6GlMBKjBnI0O6AlX14K4H0UxNLfJgUT+btn4SX6HK6hPZt/FMTS1yf1F7zkadqSFVPSR3C+lfgOp/pD3i+g/NCIl2T7D6WM51MjUkiGj8upgSZ2rULDPZMznRBCIav2Y2HdIgxat6OEIGpWTj18ymtExKzzK+PEqvSIYfqU8yfj3Tp0HapFl9JC/TjFgREo1fC1OS3fM+wnDcvcULkZRttDAlDX4VVX1W4dH7hnZsm4hU8WthSoGUl2Us9yFotw1uyKYfhEgwfp07YBRIZb1DPEC/QrQ5CZVS8WsUiYRxBcfqPdlHwujJ4pBKuFHEN33CTI2XZRxH7Oo/G3zikEowfnbLjQApuaDw1b3Ojrk/M0glGD97zqa2e47VOzZws+1kTCdVXfHXf/JeotruOVbvOvBV7yM6qerBIvMOmLJwytwIkqr7CbsGfeWQSVUaPxNTZSalW73ThS61TTS6ph5wlP1F3pxNCSnd6t0xdss71FaX+BpVxs8bYKhmaqTeYfDkRROEdUxNOIrBYoOFqaITTLb6jAnCIlujosyoLzAilPcvyFafN0FYVGuUf4ocTBWQNmlZBjRBWCcPJFIVxs84m8ozKdHq3fEtNUCP1HsSqXLjp5u+/EyqHBOGslEThHXlUkiVGj8dUymkNKt3cncRlaJZo9T4ydlUBillTKg2QVgUa5QaP9n0ZZmUYvUEE4R1S7BGmfFTMZUNfilVveMSTBAWxRrbEuMnZlPJPS/KmLD7QDNBWJ/VCUdi/ERM8dKQYPVOl2yCsAjWiGcbGqaSTKqu6t0x/qNfqpRVI278tKuKHRRStdVzTRDWsYpU3PhJnWH0npf6RhDfBGEprRE1fgqm6D0vZUHhPuiYICxVQxX9FAmYYpcRlVZv35sgNNKtvKHaxoyfcLkGs3uF1Tvd4ikmJYU1Ysav7gxj/QtFVU+tBDmSWyNW8SsxRQa/cqsvbIKw5NaIGH9d9etg2O7lVu9ajEqQJak1jlegEFVzNgRSae/QjAnCOnbw14hkGwWm8LhCZvUFztkUjW5wa4SNX1EkgpDKrN6kCcJ6xK0RNH45piCkMqs3a4KwJNYIGb98HAxBKrF6xzVsgrAGn1BSoU9RejaF7B4vKMowQVioNUJlhsz0oRuzqNU7tmTiYlqoNUIVvwRTAFLU6l2nLBOEdY8kHMD4JZjmm2xo7xCfCZYl7HJDPtvg2RTIpIjVO5nfvFaiEXwNJ3+jCP8nQfKQIgVF+SYIC7bGfJmBmn4OUuTyr43/82QlC26o5m4UYS23HKSw1WPXDioRXDVmP0UM0yykcEHRvanKBGFdQaRmjR85fWcHv9CYsFIThDUCrDFr/MicLTNTgwoK15pOiknrPj/ByVwlhltumU4wZPX2UdUmCAuoGjPGD2bTdCYFPsKSK0GORte5hJPONg0A01Ya0pXtLAl6M8GylLfGdLYBMO0cpj7CrNU70zNBWDlrTBs/gGkK0tyY0LWmaIKwBtk2Vaq/CJi+WBrmrN6esgnCusrk1L7wKeZNXxz8Zqv6GTBBWKOHzL+IJvQXc6Yv2n0my7AuxlSsrRSpovHnMBUgzVj9rJggrGNXTDhCfzFr+oLdp3uHjj0zJghrkGqoCsafwTSZqaWrerfQtYNq9Ci2qRLjz/zsMhn8ilmmpImLaaWuxMfGn8Y0gVS0+qlWghyJVWNi/ClM4ys0Yu9wNk0QlnDvLzb+1Dg4yqRCQTGzJgjrJLncEGcboeUW2b1XUESPYpZNEFZijZHxC2fTyO4Tq59tE4QVW2Nk/MI4eDJTi63e6c64CcKKrXFi/ElneDKuiK3+KZggrMfJWTwcLCZ3wMImW1xQ2FtPj9BIJ5PWf2j88QAjhHRi9W41M8HSNKkaA+OP5mwhpM3XwQvu3jzdFxgq/LVYYPzRODj4nVpo9Q74T4s/MYX3/gLjn2Dq231o9bPVbNJXMGv0jT/E1Ic07B0+RROE5c8aHd/4g2zqQ+pb/Qy1Q4tr5J3F269XQtP3Mqlv9dVdO6hGj7b//yHyTb+zUPetnv4rrKcir2r0jP9u6Nv9m/5TN0FQgyO7v+Zl08u1lXGZd++mqSt3/MdCZ6GxLfsp8tPW6OHXRefwV/v3MEFYW7+GbzV/hfVUdPy/38gEYf2eKWauueaaa665Zlz/Bypfv+kaPrfCAAAAAElFTkSuQmCC" className="card-img-top" alt="..." />
                                    <div className="card-body text-center">
                                        <h5 className="card-title mb-0">Gitlab</h5>
                                        <div className="card-text text-black-50">to store our code</div>
                                    </div>
                                </div>
                            </div>
							<div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-0 shadow">
                                    <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8PDRAODxAWEBUQFRgVFRIYFhcaFxgQFhcXGhoYGhkYHCggGBolHxcWITEhJSkrLi4uGiszODMtNygtLisBCgoKDg0OGxAQGy0mICUtNS8wLy0tLTcvLy01LTAvLS0rLS03KzUrKy0tLS0tLS8vLS0tLzUtLS0tLS0tLSstLf/AABEIAO0A1QMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAABgcDBAUCAf/EAEoQAAEDAwICBgYGBgcGBwAAAAEAAgMEBRESIQYxBxMiQVFhFDJxgZGhI0JSYnKxFVOCksHRM3OTosLh8AgXNIOy0hZDVGOUo7P/xAAZAQEAAwEBAAAAAAAAAAAAAAAAAgMEAQX/xAArEQEAAgIABQIFBAMAAAAAAAAAAQIDEQQSIUFRIjEUYaGx8BMygZEjM3H/2gAMAwEAAhEDEQA/ALxREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERARFgqKyKP+kkZH+JwH5lBnRc9l8o3HAqoSfASszn4reY8OGWkEeIOQg9IiICIiAiIgIiICIiAiIgIiIC8ySNa0ucQ0NBJcTgBo5kk8gvSpTpc4gqbjcYuG7ed3OAnOcAvI1aSfsMb2nee31dw3eKOmhom9Es9Oa2UktEhDiwuH2I2dqTv325d4XOjoeN67L3zCja7cNLo48D2Rtc8ftbqSQw27hilbBTxieqkbl7zgPd957vqMznDB/MqKXLjK41BOqodGD9SLsAe9vaPvJV+PBa8b7Kb5606NxvAPFw7X6abnwNTUEfOPC9m3cb0mCyojrAPqZhdn3ysY74FRz9J1Oc+kS58etf/NblLxRcIvUq5fY5xePg/IVk8Jbyrjio8OwOle70BDbxaHNbnBlYHsHu1amPPscFz/8AeDxFe5Hss1MIImnBkAYSM/all7AON8NGfap9wJfa6sjlkqxF1DAQZS3SXOHMc9OkDmcD88cC/cf6Aaa1sZBE3IEgYBnfcsZjDR5kZPkqowWm3LC2c1YrzORH0dcUTkvqLyY89zaioPyaA0e5Z29H3FEHap73rPhJNPj4Oa4KO1V1qZSTLUSvz4yOPyzgLxDcJ2HLJ5GH7sjx+RV3wc+VXxUeEkfwRxXXO0111ELBt2Hkah+CFrA4fiOVrjoatkf/ABl4Jd3nMUe/7bnFZLTx9cICA+QVDfsyDfHk8b59uVI2xWu+A6B6JV4zyGXHxOMCUeezvYq54eaz6vb5Jxni0en3+aMjol4fftHd3Z/rqc/INC8u6GK2nzLartgncevFn9uJxz8Fyr3Z56KYwTt0nmCN2ub9pp7x+SwUVbNA7XBI6I+LHEfHHP3q34SJjdZV/EzE6tDsRce8QWORsV5pjUwk4EvZBx92VnZccAnS4avMK2+FuKKO6U/X0kusDZzDs9jvB7e4+fI9xKgti49bMw0l1jZLHINJkLQQQf1jMYI8wNvDvUY4x4RqOH6hl7ssh9HyDJHkua1jiNnb/SQu23O4ODnOCMt8dqTqWimSt43C+0XD4M4mhutDHWQ7auy9mclkoxqYfiCD3gg967igmIiICIiAiIgIiICIiDBW1LYYZJn+rExz3fhaCT+SpToKh6yW6X6pwXZcNXg52ZZjju+p8SrQ6RJC2yXIj/0so+LCP4quOAGiHguokbsZpJM+ep7IvyC7WNzEOWnUTKO3SvfU1ElRIe1K4uPkO5o8gMD3LVRF7MRro8qZ2LYoKR880cEfrSuDR7SeZ8hz9y11NOimhElwdKRtBGSPxv7I+WtRyW5azKVK81ohv9Ile2kp4LRTHS1rAZMcy3uafNxy4+7xVeLo8RV5qa2onzkPkdp/A3st/ugLnKOKnLV3JbmsIiKxAXqORzXBzSWuachwOCCORBHIryiCzbNcYr5SOoavDaiMao5QBk4+uB49zm9439ld3Cikp5nwSt0vjOHD+I8QRgg+BXygrJIJo54jpfG4OafMdx8QeRHgVO+PqaOtoKe7wjGwbKPuk4382vy33+Soj/HfXafpK6fXXfePsrxWN0Y15qYqm2T/AEkRjJa07/Ru7L2fh7Q28yq5U36I2E3GU9wgd83x/wAl3iIicc7cwzMXhrf7PeuCa7UDzkQSs/fBlY4+/Q34K51UXQvHqu/EMw5GpwD7Zag/yVury3oiIiAiIgIiICIiAiIg4HH0Bks1xY0ZJpZsDxIY4gfJVtwOes4Jla3cxyPz7pmv/Iq5Z4g9jmO3DwWkeRGCqY6F4CaC92d/rxSPB/E9jotvYYQpUnVolG0brMIoiAovYeWKyeieBxpq97PXdpY3P2g15G/tcFWysfo2mc22XIsOlzNTmnwd1RwfiFTxH+tbg/exxcC0NGxrrlWgEj1GkNHuyC9/tAC9Q23huocIYZ3RvccNdqlGXdwBlGknyVezSukcXyOL3O3LnElxPmTzUwm48L7b6E+lY53V9Xrz2QMYDtGNnDyPPfyULY8nmZ+iVb08Q4vFPD8lvqOpedbXDVHJjGpvs7nDvH81xlYvGrjVWK31h3e0tDnfiYWu/vMCrpW4rTavX3V5KxFugu1wrw9JcKnqmnQ1o1SSYzpby2He493+S4qsXowJ9CuXV/0mkY8fUfp+eUzWmtJmDFWLW1LbZbLJBqY2mmrTHs97GSygOHMFzcMz5BdClqqCos1e2ijLImRy6mkEYk6vVkZJ8jsq8tfF1bTUnokMgazfSdPbaHbnS7u3J555qS2L6Hhesk/XOeP3iyJZr45j3nvHdfS8T7R28K9CsPorAhhr61+zYmAZ8mNc9/y0qvFNuIZ/0bwa/ukr+yB49ed//qaVbxVtU15Q4eu77dD/AGfqR4tU9XJ61XUvfnxa0Bv/AFdYrPXD4HtHoNqo6TGDFE3WP/dd2n/3nOXcXmt4iIgIiICIiAiIgIiICqKxD0Djmtpz2WXGIysHjIQJCfbqbOrdVSdMw9CuVnvTdhDN1Mru8x51Bv7vX/FBGOJaL0evqYeQZK7H4HHU35ELmKcdLFDorYqhvq1EfPxfHt/0lnwUHXr4rc1Il5eSvLaYFYfRztbLm48tJ+UTj/FV4rD4S7PDlzcOZMoPs6lg/iVDP+zXzhPD+7+FeBERXKlhetwjv9R+3/yv8yq9Vh3pvo3C9NA7Z07mnHf2num+QwFXipw+1p+crMvb/kCmXRZchDcDC49mpYW/8xnab8tY96hqyU8zo3tkYdLmODmnwc05B+IVl681ZhGluW0S6HE9sNJXT0+MBriWf1Tt2/I49oUupBr4SmaObH5PunY8/IrJxvTtuNtgu0I7UbcStHMMz2gfwOz7iSsfCPa4dubTyaZSP7Fh/MLPa/NSJn3iYXRXV5jtMShdktzqqqhpm/8AmvAJ8Gc3H3NBKmPGTBceJLZZ2f0NAPSpwMY2wWNI9gYPZKvvRvSx0tPVXio7McLHBp+40apHDxOwaPeFtdDNDLM2svlS3EtzlJYD9WnYSABtyzkexjSqOKvzW14XcPTVd+VloiLM0CIiAiIgIiICIiAiIgKJ9KVk9OslXC0ZexnWxjv6yLtYHmQC39pSxfCgqOKp/SnCVNU+tLRgNf45i+jdn2sLXqDqb8CRttl9udglGIKvM9M07AtcDlg8exkf8kqK3m3OpKqWmfzicQD4s5td7wQVv4S/SasXE168zSU96L6yN4qrdKdqlhLR49ktePbpwf2SoEt2zQ1ElTE2lz1uoFhHcR3k9wHfnuWjLXmrMKMduW0S+Xe2yUlRJTyjDozjPc5vc4eRG62OGbV6ZWw0/wBVzsv/AKpu7viBj2lWZxbNaXej0l2qYoqh8epsgPV4xgOcHHIY0nOA84ODzwVm4N4Ugo5ZaiGoFTrZpaez2Wk5PaaSDnDe4clm+Kjk+a/4eeb5IZ0o3US1jaZnqUrdOBy61wBPwGkfFQxSjizhOtpS+qmLZWyPLnyMJ2e857QIyASee4UXWjFy8kRVTk3zTsREViCb9GN3ayaSgmwYqoHAPLrcYI/abt7QPFSWSwmjtctvidqkrZ3MYfuPOMn8MLCT5hVZbqeWWeOOAEyOcNAbsdQ3BB7sYznuwr4pI5xTsknZHJVRxuHZOGl5A2DiOyHFrc7bLFxHptuO/ZrweqNSrnpDb18lv4UoTjrNL6lw5spmb9rbGSQXnluG/aVp0NJHBDHBE3QyJrWMaO5jQAB8AqX4aoeJaGvrK59oZVVFWe1K6eIaWZzoZ9JszZu3g0eCl0k/FszSWQ2+k25PfI9+fa3LfksTWsFFWXRjxvX1Vwq7Vc42iema5+toxs17WuacHB9dpBHcrNQEREBERAREQEREBERAREQVl002WUR017oxie2PD3Y+tT6snONyGncj7LnLDxRBHeLdBeaIajoxLGN3aR6wIH1mOyPMHPgrRlja5pa4BwcCCCMgg7EEd4VIV1LX8JVslRSxuqbZUOy+LJ+id5nfQ4cg47OGAdwMTpeaW3CN6RaNSj0MTnuaxjS9zyA1o3JceQCs6goxZKRuiL0u4VYIjgbjJIAJbn6sTMguecDl4tC4MPStZg7raG2yyVcuQ2NsTGuMh7tTSTufsgk+Cj44tulqv5uF6pnhtRD1WloBbHC4teGxHOCWlvabnc5z3FXZuIm8ahViwck7lmd0PXe41L6y51cULpTqeRmR4+6GjDQANgA7AwpJcLjR2agFrtTsvOetnBBdqPrOLhsZDy29UDuwFtVlz4fuv0pu5i1c43z9WAf6ucYHu2WoWcLUI66W4MqtPJglbLk/ggG/v2UMf6cdbf07f9SekOtwBFUS2qrbU6nwyBwi1kklukh+M/VzjHnlVY07BWzwbxyy8R3HqIephpWNbHn13amSZJA2aOyAGjPt3wKmbyC1cNbmm0s+evLFYfURFqZ036JIQ64SvIzohOPIuewflle7lw1ezXS1MYfqc9xbI2ZoxGScNGXAhuMDGMLY6JvoxX1B5RsZ8uscfyCj3CX/AItuVCysprpE1j3Pbpka0Oy04ztC7bOe9YMuWaZJ02YscWxxtMqC08RPwJa1sDe8kRvd8Gs3/eCw8T8d0VjhfGal9wq3D1C/OHffA7MLd+QGo+fMcObo84lrOzXXoMYdi2N0hBB8WtbGD713+D+h+3W97J5S6smZgh0gAja4fWbGO/8AEXY7lntkm3j+IX1pFWPod4dqWNqbvXgipuLtWkjBbDnUNvq6iQdPcGtVkoigmIiICIiAiIgIiICIiAiIgL45oIwRnP5L6iDTpLVTQuL4aeKJzubmRsaT7S0brYngZI0skaHtPNrgCCPMHYrIiCK3Do4stR/SW+EZ/Vgxf/kWpaOjmzUjxJBQxhwOQ55fIQR3jrXOwfYpUiCmuhv/AIriSPwl/wAVSP4KHM5D2Kb9EbdN84jgO2qcnH3RNOP8YUJDcbHu2+C28H3Y+K7CIi2sqdcPvMHDN3qAdJcyZrT59SGtP7zypR0O0pi4eoGnm5j3/wBpI9w+RChvFD/R+CHDODUvaPaHT6vmxitDhSi9HttFAecVPEw/iaxoPzyvJzTu8vSxRqkOqiIq1giIgIiICIiAiIgIiICIiAiIgIiICIiAiIgpqz1IoeO6yF/ZbXx4Z4F7mRyA/FkjfaVyOL7W6kr54iMBzjIw+MbySMezdvtClPTRwXUVXU3W3gmpo8Za313Rsdra5g73sdk45kHbcAHlWrpFtN2gZTXpvolRHt1uCGl/eWuGTHy3a8Y8yrsGX9O3VVmx88Iktq2W+WqnZTwt1PecDwA73HwaOZKlxouF4R1kl1EjRvpEzHHHsibqPuWrJ0k0kWaLhugdUzSbCTq3Y/EQfpH429bSB4rVfi669LPXhrb6vvSmxlRU2bhqDt4ex8oHNsTRoBPno65x78AeKuUBV30ZcCT0kst0ub+urqkHVkg9U12CRkbFxwBtsAMDbnYq89tEREBERAREQEREBERAREQEREBERAREQEREBERAUev3BFrr3F9VRxyPPOQZY8+18ZDj7ypCiCE0vRNYY3ahQhxH2pJnD91z8H3hSu22unpWdXTQRwN+zGxrBnxw0DJW2iAiIgIiICIiAiIgIiICIiAiIg59XfKOF5jmqoYnjGWPlY1wzuMgnKwt4mtxOBXUxJ7uvj/7lDqG1U1TxTdm1NPFOGwUxaJI2vAJbuQHA4Upl4LtLgWm20uD4U8QPxDchB3WuBAIOQdwRywvqr/owBpqq72tjnOgoZ2GDUSdDJmucYwTvhpHzUiqONLVHIYn3Cma4HBaZmbHwO+xQd5F4hla9oexwe1wyHAggjxBHMLi1vGVrgkMUtfTse04cwysyD4EZ2PtQd1Fpw3WmkgNSyeN0IBJmD2mMNHMl+cDCi/BvF0dTUV8U1ZC7TWOjpW64wXQaGaQwDBkGS7fdBNEXJuvE1vpHiOqrIYHHfQ+Rodjx05zhZqa+UcsccsdVC9krxGx4kYWulPJjTnd/wB3mg6CLQq71SQiV0tTFGIS0SapGjQXjLQ7J7JI5A80pL1STU5qo6iN8IzmYPGgaTg5dyGEG+sdRM2NjpHnS1gLnOPINAySuRRcXWyeQRQ19PI92wY2VhJPkM7+5ZuK6V81uqooxlz4nBo7yccvfyXYjcuTOocKl4luFbl9vo2CIEhs07yA7B3w1u/5r3UXW9U7TJNRwTsbu4QPcHBvecOyT7gvPAPENI6hgpzKyOSJuh0biGkkHmM8889lMAc8lbeYrbXKrrE2jfM51gvUNdTtqIScE4LT6zXjm0+fL4rjXDi8undSW+ndWys2e4ENiYfN52Py9uVzr3Rfoe318sEriap40DAHVueSDpx4NJx+EKRcI2ZlHRRRNADi0OkPe6UgZ+HIeQSYrHq9/Bu0+n+3N9Iv/rdRRkfY1yavZnOMrJaOLtdQKOtgdRzn1WuILH/heNj/AKGSVKFG+PbO2qoJXYxJA0yxuHrBzBkgHzAx8D3LlZradTDsxasbiWbiG/upKiihEYf6XL1ZcXY0jUwZAwc+ty25LvKsbhcjVt4enccudOA4+L2SRNcfeWk+9WcuXryxH53KW5pn87CIirWCIiAiIgq/qK5/FF0FDPFA4QU2syxukBbp2ADXNweakD7ZxA4Fv6SpY8/XbSOLh5gOlxn2r7Z7VUM4iuVW+MiGaGBscmRhzmN7QAznbzCl6Csb/YP0fRU1uhnkc+717GVdU4/Sva8OdKQR6uQzSAO4nnkkz2isdJBAKeKmiZGBjQGNxjz23J7yea53HNhkrqVno7xHUUszKmne7Onr4icB2N9LgXNPtz3LSh4xqWtDZ7PWtmAw5kbI5Iy77soeG6fM4Qcm30TqW53Sz0buqjqKL0qnZ9SCd5fE7R9lpdpdjkO5YuEb5Ba6CGjrLbU0b4WBsrxTOkifIB2pBJCHB2o5O/iuna7FXzOuFxncKOqrIRDTsBD/AEaFgcWanDZzi86jjZZbVxPWQwRw19tqzPG0NfJCxkscjmjGtrmOyNWM4IGMoMfB0Frnqa6pt9RHLHVBgnpA0BrZRka3RuALS7cHLRnC41hbDSw8R1bYIy+jqqiSLsN7JjgY5obt2RnwXYsFvqKi+S3d9M6ii9FFM2OTT1srusD+se1jiGgAaQCcrBaaSopq250lRQyywXGpdI2oYYzGIpY2tcJAXhzcYI5H+Yb3RvYKeK209S5jZZ6yJk89Q4B0j5JWh5y474GrAHJcXpCscMNfZ6qFoiMtyp2SsaAGyOGoskc0ba2gObq54fg8hjesIu1sgbRCkFzgh7MFRHNFHJ1A9VsjJSAS0bZaeQXH4xbXzVlmqKtjaZguMDIqVrxI7J1OdLK8DTkBgAa3IGTuUG1ZLHDVcT3qWoYJWU7qUsjcAWdc+nb2y07FwDcAnlqK8cTT0zr9FR1ML30tJTioFPFTyStfVyPLQ6RkTCC1rWnGoYyVI+G7bPFeLzUSRlsdS6mMT8jDxHBpdgA5GDtvhY+JLXVQ3CG70MYnc2I09RTag10lPq1tdG5x0iRjsnB5g4yEGtebraqyndTVFFVPjcMY/R9Vlvm09VlpHcQsvAt3fFZ2vuD3x+jyPh62dj43via/ET3B4DslpbueZWSfi2qkYWUlqqzMRhomY2KJrj3vkL8ED7ucrMeG55LMaGqqTUTuZl07uRn1axjbZgOG8uQXY1vq5O9dG7cOGbfWjrJKdjy8A9Y3LXEHkdTcE+9cd3ATYu1Q1tRSnuaH6mZ827E+8rVsfF3oMLKO6RSU74QGNl0FzHsbsDlucnGNxkH5LpTdIVtA+jldM7uYyOTUT4DU0D5q7WWOkbmPop3jnrPv9UN4hulVUW+soqvDpqGWNxe0Y1xklmSBtzc05wNnctlatBUNlhjlYctkY1wPk4AqIcK2OSodXVlbF1fp/YEJ9YQ+feDjSO49nKwW6sqrLmmqYn1FICTFUMGosaTnS8dw/wBDPISvEW9NfePydOUma+qfafyE+XO4iqGxUNTI7k2J/wAdJwPecBcc9IVq059J/Z6uXPs9VRbifiQ15hikZLS0L5AHzlhzJjcDwDfj492FCmK0z1hO2WuuktWkgLKbh4O+tUud7nTsI+WCrcUA46fHHJZ5Yml0UMmsdWNQ6phiI0457DZdL/eBSfqqn+xP81LJFrxExHn7o0mKTMTPj7JaiiR6QKT9TU/2J/mpXG/UAfEZ+KptWa+62LRPs9IiKKQiIgIiICIiAiIgL45oIIIyDsR5L6iCF2nhy5W1hpqCop5aYOcYoqhkmuJrjq0CSN3baCTjLc7rZo+GqqathrrlUMldTauop4WOZCx7hgyOLnF0j8bDOAO4KVogIiICIiD4Wg7EZXlkTW+q0D2ABe0QEREGPqGZzobnxwMrxWUkc0bopWB7HjDmkbELOibNMFFSRwxMhiaGMYMNaOQAWdEQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBfF9RAREQEREH/9k=" className="card-img-top" alt="..." />
                                    <div className="card-body text-center">
                                        <h5 className="card-title mb-0">PostgreSQL</h5>
                                        <div className="card-text text-black-50">to create our database</div>
                                    </div>
                                </div>
                            </div>
							<div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-0 shadow">
                                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJcAAACXCAMAAAAvQTlLAAABrVBMVEX///+ZEDn/ujkWADq/v7/f39/qLS7K4vFVotN2tdwSfsIjh8Y0kMpEmc9mrNeHveCYxuSo0Om52e3a6/Xr9fp9fn5beo3n5+eqqqrS0tK3t7cbg8TD3u/S5/MPX5IUf8JrrtmeyubGxsaWlpb96+uenp7rNzj+9fXtS0v83t7j4+Pwa2z3ra2QkJCHh4eCT1/6y8vyeHmqjpZ9RFWnjl++p66SIELTw8iHW2inlHHb1s04KlA2K0ggDkBeWmTziYn0lpf4urrtTk/vXl9kZWW/sZZGPVXnrkJRSl+KNk+XdoCPLEmriEdoYnKLhpF7am7PnDzTpk+If270k5P5wMBQXGRHa4FtbW1jkq9ofYucsr+vpZLPxbKtqrKnkWizk1bHnU94coCvl4uBKS+SUyabcS9JLic+MVN+NiWxcCxsSyU7IiyTIzKEQyXalzNVOSUoETQwIEslME4TU3+1gjAiKE0jQVwhL0GgZicbU3xfMEx9VyjKhzBePyUxGDGKk6BDQ09EdJZyMDpkUTuObzpVNEt7Yll0Vk+CS0tTLT2Pf2A8f6mMhXlRPEV6m7ABWVLvAAAGD0lEQVR4nO2a+VPbRhiG18YWEO7DSHbapMWHMGBzhnAfhhhDMBSCOUMgTdPQFtKStqRt2hqSljQB8jd3JcuypF2tJUNmdzJ6fgHmA+aZfV+t15IBcHBwcHBwcHBwcGCHbtoCJvSEaBvgmZikbYBnsoO2AZ6eNtoGeBJsrleU66GtgEPs4TppO+Do5KZZ3CeiEW6StgOG1WmOxW012sZFGNSainDcav4HkaaJFvFBhIso12Koc5KVdRM7OI6bkr/t7mmbomyjEk1wXEQOMdrBTTATYncbx7VF4TfiBBdhZrFAN2y8vJ/CKzIRpW2jsgpD7JB0phK5r2wQgiHK+1Y3DJOV6xAyAa9EaYMQoR9Dp3sRlks+c3VyXIK2jIaoslwA7mAsnb0kL7ns08q6sQLMUd5Ip/OCjPBAqXsP9JqmLaMhFMn1awp6MXUszB+e4fbK1gFMebMxBTcwboK2jAZxOrdMndKC0ZbREkrIO5coVZ+2i45QQk5SeoWkraJH7JCSFNm6ICVEacFCTG1gOaQ9v7ODmSO0ltAkbQM8LO2pTMMHgzxtBwxCXAjEBNoWRgLxsBAIeDxBD20TLf54MCzIXq0BdsLk41Ar79XqDzMSZtAXC2q9/P5wK20nSGYmbvDyej1h+mG23Mr4jF7tvEA9zJYbVWsZxIvnBcphQq/qmsy60Uv0rgfoe1VsZOJ6r7jgpbuV5bxqKzdnhIJXOMYDRrzq6jM+xUuI++GAGa+GrZkg9PL4ckLseDWWZ2fgC6UyYMmrqXlG3VHZ8vKpA8cLi+NlD8fLHo6XPRwvezhe9vjEvfj5ecJ7O35oiDRNp9Hp9Xj1zrlcc71m0+0Ft3th22y6OFpWNrr4Mbx6+1wSA113cNN7/W6J2f6HWKtkmcRYaue6ve50DbgU5gaRQB72z7oVltEwd1JjZQoj6fbr9Lo7eN+loU8f5vCjZbeGXX2YntRRmYakNsyrebUPzrn0DHTdVaf80J5bjzZMb/pZmR5tmFfymu9zoahhDu26UfaGlNvwj41WuTDzUV/BqxdnpYap1B1l9x5Q646SD7NkL03dEQYGvy7UHWH2UbBQd4SjVOsVvAx1R3jyjakW5Om3plqQZ49L9ULrjrL/Hcns+x9IZslsSV7YuiMcHD4niP3y408EsaPUmm0v07rbDPMVMcwXP9vzItXdZpizxDDHjs+texWr+7WG+etxjUUv70s7i5XjyW+kMJ8S+/9izaLXrd+TtsVc+3+Swvzrb1KYL1used24efyHbbGDQ/NNVgrTfJOVwqy35HX75mnSfpj7xDBfkcPMWPOqqiohzINDUpjPi4SZteZVfV5CmFd5ZRo9seZVXVNCmK/fkMT+OSGKxSx61dQc/2tvuc6824TlWvcuEqx8XmDVq6LiPFn8xTvP/TN4Th3+ysRq7wyeU1vNrsqRdZ1JUa+K2jWr/X+bO9cvYK1mz4LydBRf+nHDoz0LXl/Unljp/9sL5c+xJ9g3+UqncFrjMcTBgldl5UbRMP9bUd+dDaFW79fVaRq1SvnQN+LWvOrq0sT+9y0NF/4cKf7CiuYpJ1L85LoXVbDsVV9nHubcku7pqqH4yyu6qaH4I2n8QcuyV/2X2Uvs6Wdgyfg0Wlv82RXjVFt8pO5EL3EN69XQMP4O1RpEKqst/vswMtUUH6070QuAD6dYr8aty9d6q6445s6SWvz+C8xULT6u7kW8AN9yjvNq/GzxUvPK1LeEq2y++LsX2Okiqe7FvADYasF6lX+uhmmoewG5+Ia6F5CLb1b34l4AZE+xXk2eyzls3QssYOpeYJRQdytewLu5gfNqao69c/WhhS7Qj6l7gVSSNLXgBYCnBevVrHngK8PHYmFBUAvjucLUkhcMcwPnBXz63woGBY+fNwunlGnR+0wwzOJePvl/m8VTytTCfbmtTTa9YJgf2PQC/mwd0SsQh//ba/YRpFKmVu+T72RJXqBVCATMP7JVwtTy/fvm7BbB69qx8bzDn21g0gsAIcumF+DDW0x6wZIGy5n0gmEKbHrBMP1MesEww2x6ARD2fGzofxbZwcHBwcHB4dPhfyEULY0vjqzDAAAAAElFTkSuQmCC" className="card-img-top" alt="..." />
                                    <div className="card-body text-center">
                                        <h5 className="card-title mb-0">PlantUML</h5>
                                        <div className="card-text text-black-50">to visualize the design of our database</div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-0 shadow">
                                    <img src="https://www.postman.com/img/v2/logo-glyph.png?8656ac4b337e324e6431329c4a9e772d" className="card-img-top" alt="..." />
                                    <div className="card-body text-center">
                                        <h5 className="card-title mb-0">Postman</h5>
                                        <div className="card-text text-black-50">to design our API</div>
                                    </div>
                                </div>
                            </div>
							<div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-0 shadow">
                                    <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw0SDQ0NDRAQDQ8QEBAQDQ0PEBAQDg0NFxEWFhYTFRMYHSggGRolHRUVIT0jJSkrLy8uFx8zODMtOCgtLisBCgoKDg0OGxAQGzAiHSIrLS0tLS0tKystKzArLS0tMCsrKy0vLSstLS0tLS0rKzAtLSsrKy0tLy0tKy0rLSstK//AABEIAOEA4QMBEQACEQEDEQH/xAAcAAEBAAMBAQEBAAAAAAAAAAAAAQQFBgcDAgj/xABHEAACAQEDBQgOCQMFAQAAAAAAAQIDBAUREiExQVEGB1JhcZGT0RMUFRciMjNTcnOBkrHhFiM0oaKys8HTJEKCNWJ0wvBD/8QAGwEBAQADAQEBAAAAAAAAAAAAAAEDBAUCBgf/xAAzEQEAAQICBwUHBQEBAAAAAAAAAQIDBBESFCExMlFxBVKRsdEVM0FhocHhEyI0gfByQv/aAAwDAQACEQMRAD8A9xAAAAAAAAAfitVhCLnOUYRisZSk1GMVtbegsRM7ISZiNsuTvXfBsdNuNCMrVJa4+BSx9N537E0bVGDrq4tjVrxlEcO1zFt3wbwniqXYrOtWTDLmvbLFPmNmnB2437WtVi7k7tjTWjdJeM/HtVf/AAm6a5oYGaLNuN1MMU3rk76pYVS3V5eNWqy9KpN/FnrRpj4PGnVzl8JSb0tvleJ6RFJrQ2uR4EV96dvtEfErVoejVnH4Mk0U/GF06ucs2zbp7yp+Jaq3+cuy/nxMc2bc76XuL1yN1TdWHfFt0MFWhStEdeZ05v8Ayjm/CYasHRO7YzU4uuN+11d0bvrBWajVcrLN+dw7G3xVFmS9LA1q8LXTu2tmjFUVb9jqoyTSaaaaxTWdNbTWbKgAAAAAAAAAAAAAAAAADCvm9KVmoTtFZ+DHMorxqk3ojFbX8z3btzXVow8XLkUU6UvH7/3QWm1zyq0smmnjToRb7HDZ6UuN/doOvas0242eLk3b1Vydvg1RlYkIIACoBCAFAIQArebm91Fpsckot1KGPh2eT8HDW4P+1/c9aMN2zTc682a1eqt9OT1+6ryo2mhC0UJZUJeyUZa4yWpo5ddE0TlLp0VxVGcMw8vQAAAAAAAAAAAAAAAA8w3z7fKVrpWbHwKVNTa21Zt53yRS52dPBURFE1c3NxlWdcU8nGG403oO4/cTRnRharanN1EpUqGLjGMHnUp4Z23pw0YPPxc+/ipidGhv2MLExpVuitu467akHHsEaT1To/Vzi9ubM/ama9OJuROebYqw1uY3ZdHlm6G56lktM7PN5SwUqdRLBVKbxweGp5msNqZ07VyLlOlDm3bc26tGWsMjGhACgEIAVAIRXW7218So21WeT+qtPg4ao1kvBl7cMnjxWw1sVb0qM/jDYw1ejXl8JeuHNdIAAAAAAAAAAAAAAAAeR743+p1PV0vynWwnunKxfvHLz0PkNmGtL36xVYTpUp08HCUISg1oyHFNfccGqJiZiXdpmJiJh9iK8z316sHaLJBePGlNz25MpLJ/LI6OCidGZc/GzGlEOFNxpAUAhACoBCKAZV0Satdla0q0UWuVVI4HmvhnpL1RxR1h7+cZ2AAAAAAAAAAAAAAAAB5Hvj/6nU9XS/KdXCe6crF+8cubLWdVuV3aVLLBUKsHXoJvIweFSljnajjma4nhp06jVvYaLk5xsltWcTNuMp2w39t3yqGQ+16FWU9XZsiEE9ryZSb5M3KYKcFVn+6WerG05bIee3hbqtetOvWll1JvGT0JakktSSzG/TTFMZQ0aqpqnOWMVACEAKgEIoBAMq6vtVl9fR/UieK+Gej1RxR1h/QBx3YAAAAAAAAAAAAAAAAHke+R/qdT1VL4M6uE925WL95/TlzZa6AQgBQCEAKgEIoBABBlXV9qsvr6P6kTzXwz0e6OKOsP6AOO64AAAAAAAAAAAAAAAA0N8bkrHaazr11Uy2oxeTPJWC0ZjPbxFdEZQw14eiuc5YXe+u3ZW6V9R71u48apbTve3bsrdK+oa3cNUtne9u3ZW6V9RNbuGqWzve3bsrdK+oa3cNUtne9u3ZW6V9Q1u4apbO95duyt0r6hrdw1W28vvqzRpWu00YY5FOtUhDF4vJjJpYs6FFUzTEy0K6YiqYhhHp5AIAIIFZV0/arL6+j+pE818M9Fo4o6w/oE47sAAAAAAAAAABpr93TWSyZqs8qphiqFPCVRra1oiuVozWrFdzduYbl+i3v38nF27fHtUm+16NKlHbUyqs+XNgl95uU4KmOKc2nVjap4Yya/6eXn5yn0UT3qlp41u4fT28+HT6KI1S0a3cPp7efnKfRRGqW11u4n09vPzlPoojVLZrdw+n15+cp9FEapbNauJ9Pr085T6KJNVtmtXD6fXp5yn0URqttdauH0+vTzlPoojVbZrVxPp9ennKfRRGq2zWrh9P704dPook1W2a1cc5bLTOrVqVqmDnUnKc2lgnKTxeYzxERGUMMzMznL4lRABBAqAZd0/arL6+j+pE818M9HqjijrD3u1tqnUazPJeDWlZj57GTMYeuY2TlPk7dqM646tF2epw5+9I+Q1m936vGfV0tCnlHgdnqcOfvSGs3u/V4z6r+nTyjwFaai0Tl7zZacXfpnOK58Z+6Tbon4Qy7PeslmqLKXCWaS9mhnSw3bNdM5XozjnG/0n6MFeFieHY21OpGSUovFPWfQ2rtF2mK6JziWlVTNM5S/RkeQAByG7jdX2su1rM12xJYynmas8HoeHCepe16sdvDYfT/dVu82picRoftp3+Ty2pOUpOUm5Sk25Sk25Sk9LbelnT3bIc1+QiAAqAQgBQCEAKgEIoBABBAqAAMq6ftVl9fR/Uiea+Gej1RxR1e+W3yVT0X8D53Hfxrn/M+Tt2uOOrnj4p1UCoAAyLDa3Tlti/GX7rjN3A4yrDXM/wDzO+Pv182K9ai5T83QRaaTWdPOntR9jTVFURMbpcuYy2SpUYF+XlGzWWtaZZ8iPgx4dR5ox9raPduia6oph4uVxRTNUvELVaJ1Kk6tSWVOcnKcnrk/2O3EREZQ4szMznL5BEABUAhACgEIAVAIRQCACCBUAAAMm6ftVl9fR/UieK+Gej1RxR1h77bfJVPRl8D57Hfxrn/M+Tt2eOOrnT4p1kAAQKEG4uWvjF03pjnj6L+fxPpexcRpUTanfTu6T6T5w5+Lt5TpR8WyO21Hnm+peGezWRPU69Rc8Yf9/uOhgqN9X9NDG17qf7efm80EABUAhACgEIAVAIRQCACCBUAAAIRWfueoudusUFnxtFHH0VUTb5kzxcnKiej3bjOqOr3e2+Sqei/gfP47+Nc/5l2bPHHVzx8U6yAQKAQisi7quTWg9TeS+R5vjgbvZ139PE0zz2eP5yYb9Olbl0Z9m5LxrdzauyXnanjioONOPEoxSa97KOxhqcrUORias7ktAZmACoBCAFAIQAqAQigEAEECoAAAQioB2m9bdTqW12qS+rs8Xg9TrzTilx4RcnxeDtNbE15U6PNs4ajOrS5PTr1nhRa4TSXPj8EfO9rXNDDTHPKPv5Q6+GpzudGiPknSQKAQioAxwzrStHKImY2wZZuh7egfZa9bcr9Cp4je1TKtVpnwq9aXPUkz6qjZTEfKHzte2qessQ9PKAQgBQCEAKgEIoBABBAqAAAEIqAbTc/cNptlXsdCPgprstaS+rpLjet/7VnfJnWO5cpojOXu3bqrnKHtdx3TRstnhZqK8GOeUn41Sb0zlxvqWo5tdc1znLpUURRGUMS97RlTUFohp9LX/wC5T5PtfE/qXf06d1Pn+PV1MLb0adKfi15yW0AQioAAgUxZdKUyeU1JYyk9rb52frkPhn5KIQAoBCAFQD72Kw160siz0p1pa1CLlk8behLjZ5qqimM5l6ppmqcoh0Nm3v7zmsZQpUeKpVWP4FIwTircM8YW5LJ72t4ecsvSVf4zzrdHKXrVK/l/v6O9reHnLL79X+Ma3Ryk1Sv5He1vDzll6Sr/ABjW6OUmqV/JO9peHnLL0lX+Mmt0cpNUr+R3tLw85ZekrfxjW6OUmq1/L/f0ne0vHztl6St/GNbo5SarX8jvZ3j52y9JW/jGtUfM1Wv5PrQ3sbY39ZXs8Fth2So+ZqJJxdPwh6jC1fGW9uve1scGpWipUtTX9vkqT/xi8r8RiqxVU7tjLThaY37XZWWzU6cI06MI0oRzRhCKjFciRrTMzOctiIiIyhh2+8FFOFN4y0OS0R+Zxe0O06bcTbtTnVz5fny+PJt2cPNW2rc0x8y6IBCCBQCBUAEV5VNYNrY2j9efCy/AQCgEIAVAOs3F7kHa/wCotGVCyp4JLNO0STzpPVFaG/Ytq1b9/Q2RvbNixp7Z3PVbFY6VGnGlQhGlCOiEEkuXjfGc6qqapzl0aaYpjKErWynHM3i9izs0b/aFizsqnOeUbf8Af2zUWa6t0MaV6LVDneBzq+24/wDNHjOXqzRhecvx3VfAXvfI8e3Ku59fw9arHM7qvgL3vkPblXc+v4NUjmd1XwF73yHtyrufX8GqRzO6z4C975E9uVdz6/g1SOad1nwF73yHtyrufX8GqRzO6z4C975D25V3Pr+F1SOZ3XfAXvfIe3Ku59fwapHN+JXvPVGK5W2eKu27s8NMR4z6LGEp+Msavbass0pZtizI0b+PxF7ZVVs5RsZqLNFO6GMabMAQggUAgVABFAPMb0p5NptMODWqx5qjX7H65ROdMT8nw1fFPWWKekAIQAqAZ9w3ZK02ujZo4pTl4cl/ZTSxk+XBP24GO5XoUzUyW6NOqKXulnoQp04U6cVCEIqMIrRGKWCRx6qs9sutEZRlDXWy3OTcYPCO3XL5HzGP7TquTNFqcqefxn8N+1YinbVvYRyGygEABUABUIAVAIBAoBCCBQCBUAEVAGBcpHBbs7P2O87ZHbU7IuPLip/GTP1exOduHxV+MrktKZmJCAFQCEV3+9LY06trtDXiQhSg/SblL8sOc08ZVsiluYOnbNTvb0rYQUVplp9HWfM9r4ibdqLcb6vL4uth6M6s+TUnzDfQCAAqAAqACKgEAgUAhBAoBAoBCKgBJvMtLzLlLETOyN8meTpO0IH2WoW3I1ip53vq2HJtNC0pZqtN05bMuDxz8qn+E7+Dq/bNLkYynKqKnDm20wKgEIoB6jvTL+itL19stexUqfWzn4zjjo38Jwz1+0OhvWX1iWyK+LPjO2as78RyiPu7WGj9jCOS2EABUABUAEVAIBAoBCCBQCBQCEVAAGVdVHKrQ2R8J+zR9+Bv9m2f1cTTHwjbP9fnJhxNejbn57HSn2TjtDu2ul2mwVYQWVVp4VaK1ucccYrjcXJcrRmw9zQriZ3MN+3p0ZRveK4nWcoAhFAIB6nvTfYbR/ypfo0jn4zjjp95b+E4Z6/aG+vTyr5EfE9r/wAmekO3huBiHMZ0CoACoAIqAQCBQCEECgECgEIqAAIFb+5bNk08t6Z51xR1dfMfVdj4X9K1+pVvq8vh6uXi7ulVoxuhsTrtQA8n3wdzToVpWujH+nqyxmlooVm86eyMno43hsOlhr2lGjO+HOxNnRnSjdLjjZawBABB6nvS/YbR/wAqX6NI0MXxx09W/hOGev2hvr08q+RHxPa/8mekO3huBhnMbCAAqACKgEAgUAhBAoBAoBCKgACBWwuu73Nqc19WtC4b6jrdm9nTfmLlyP2ef4589zUxOI0I0ad/k6A+rcsAAfitShOEqdSKnCScZQkk4yi9KaelFiZic4SYiYyl5xui3u6icql3tTi8/a05YTjxQm8zXFLDlZvW8XG6vxaVzCzvo8HFWy7LTSbVahVpNcOnJJ8jwwfsNqK6Z3S1ZoqjfDGyJbHzMuaZJkS2PmYzMnqW9MmrDaMU1/VS0+ppHPxfHHT1b+E4Z6/aG9vTyr5EfFdr/wAmekO3huBhnMZwKgAioBAPurFWaTUHg86zrRzm5HZ+JqjOKNn9erFN+3GzNe0K3AfOusvs7FdyfGPU/Xt807QrcB866x7NxXc+seq/r2+adoV+A+ePWT2biu5PjHqaxb5naFfgPnj1j2biu5PjHqaxb5naFfgPnj1j2biu5PjHqusW+adz6/AfPHrHs3FdyfGPU1i3zO59fgPnj1j2biu5PjHqaxa5p3Pr+bfPHrHs3FdyfGPVdYtc07n1/Nvnj1j2biu5P09TWLXN+6d1V3/ao8cpL9sTJR2Tiqt9OXWY+2bzVirUfHNsLLc8I56jy3s0R+Z1cN2NbonSuzpTy+H58vk1bmMqq2U7GzSO1GxpgAAAAAAAAABpb18q+RHyna/8mekOjhuBhnMbCACCBUAgVvKN4UVGKcs6ik/Blpw5D6u12nhabdMTVtiI+E+jm1Ye5NUzl5P33SocP8Muoye1cJ3/AKT6POrXeXkd0qHD/DLqHtXCd/6T6GrXeXkd0qHD/DLqHtXCd/6T6GrXeXkd06HD/DLqJ7Vwnf8ApV6GrXeXkndOhw/wy6h7Vwnf+lXoatd5eSxvKg2kp528F4MtPMeqe1MLVMRFW2flPoThrsbcvJlm+wAAAAAAAAAAAAAAAADSXr5V8iPlO1/5M9IdHDe7YZzGwEECoAAgVCCBQCBQCEV+6Pjw9KPxMtn3tPWPN5r4Z6OrPvHDAAAAAAAAAAAAAAAAGkvbyr5EfKdr/wAmekOjhvdsM5bYQKgACBUIIFAIFbGzXTOSUpvIx0Rwxl7dh2cN2NcuU6VydH5b5/DUuYummcqYzS1XTOKcoPLS0rDCWHFtJiex7lunStzpRy+P5LeLpqnKqMmuOM3H6o+PD0o/Ey2feU9Y83mvhno6w+8cMAAAAAAAAAAAAAAAAaS9vKv0UfKdr/yZ6Q6WG92wjlthAAECoQQKAAr73dFOtTT0Y48yb/Y3Oz6IrxNFM7s/KJn7MN+Zi3Mw6U+0cgA5i8IJVqiWjK+Of9z4nHURRia6ad2fntdmxMzbiZfGj48PSj8TBZ95T1jzZK+GejrT71wgAAAAAAAAAAAAAAABrL5o5o1Fq8GXJq/9xnB7aw8zEXo+Gyft/vm3MJXvpak+ebwBAqEAKgAKgFp1HGSlHSmmj1buVW64rp3xtSqmKoyl0NmvClNLwlGWuMng8eLafX4btGxep35Tyn/bXKuYeuid2cJarxpQTwanLVGLxz8ewmJ7SsWaZynSq5R9+Rbw9dc7soc7Um5ScnpbbfKz5Cuua6pqq3ztdamIiMofSxU3KrTiuEm+RZ2ZsHam7fopjnHhG2Xm7Vo0TLqj7lxAAAAAAAAAAAAAAAABJRTTTWKeZrajzVTFVM01RnErEzE5w0dusMoYyj4UNuuPL1nymO7Orw8zVTto8uvq6Vm/Feyd7COa2EIAVAAVAIQAqAQKsINtRim29CWlnqiiquqKaYzmUmYpjOXQXZYOxrKlnnLTsitiPrOzuz4w1OlVxz9Pl6uViL/6k5RuZ502sAAAAAAAAAAAAAAAAAADCtF2U5Z14D/26OY5mI7JsXdtP7Z+W7w9MmxRia6dk7Wvq3VVXi4TXE8HzM5F3sfEUcOVX0n6+rapxdE79jFnZqi0wkvY2uc0a8Jfo4qJ8GeLlE7ph8nx5jXnZslkhMSZiYjNUxJnA/UYSfipy5E2ZKbddfDEz0jNJqiN8vvTu+vLRBr0sI/E2rfZ2Jr3UTHXZ57WKrEW6fizKFyP/wCk8OKGd876jo2ew6p23av6j1n0a9eNj/zHi2lnstOCwhFLa9LfKzt4fC2rEZW4y8/Fp3LtVc/ul9jYYwAAAAAAAAAAAAAAAAAAAAAAB86+gxXuF7o3tbWOPd3tuh8omKne9yzbMdHDte4zjotYAAAAAAAAAAAAAB//2Q==" className="card-img-top" alt="..." />
                                    <div className="card-body text-center">
                                        <h5 className="card-title mb-0">Python</h5>
                                        <div className="card-text text-black-50">to pull data from Twitter API and the News API using the Tweepy and the NewsAPI python libraries</div>
                                    </div>
                                </div>
                            </div>
							<div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-0 shadow">
                                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMEAAAEFCAMAAABtknO4AAAAkFBMVEX29vYAAAD8/Pz5+fn9/f2+vr46Ojr19fWVlZXy8vLs7Ozh4eGPj4/n5+fW1tbv7++Dg4OdnZ3S0tLFxcWvr6+4uLh7e3vb29u/v7/Hx8ejo6OKiop0dHRMTEwsLCxpaWlFRUVhYWEXFxdVVVWampofHx81NTUpKSkODg4dHR0VFRVAQEBkZGRRUVELCwtISEg2r4u+AAAbn0lEQVR4nO1dB3eqShBmi9KkSBVEkWAvyf//d29nly54c++LkZyTOeeWKJr9YHb6zErSL/3SL/3SL/3SA8KMyKsX8T8IW55tL135x2IgHkLxIkZr54dCwB46GpRQ/Yz0nwkBb5BHCSE0RjZ99WL+jaZIdZzQ0GJ0+ZkIaIbYRkCnE0IfPxMBcdEMCVriVy/m34gmaC8Q/FRhJEkfAsDhxwIgePbDETBGEhBCyjAQoFcv6K+JaBzBeqFrmqGHZqj9tD1NnEIaHQ+H3REYKv5ZEIi8QWi64hjOBZYfZWFgFYTRfsf+mlFqHDgC8wchoE553xHKglUhWa1XL+vTRIiCeij7MQYGsbI+AMxjID/Ea5M7AM7r8n+5b2CKR68aiN659bnqBtPi/2+57TmhSsctWOWkjWDFvDXVCRqvTK/+qD1oYuxaCN5BDxBqrJov7o0xQ6CtjbAWniYJ4lPz5VE7bzStBKinU7pFE7Za3BVQ+oj3QvEMbp7jx7HnGgmKKHj9bVJMeazClehcIa/iBuMrmrpkRt7G9rxj/ao90ueAbdRHu2TCWAqTY/PFmTq6x0CwPOtZ/mrpWJSpMosu229MnVHtaCb3Q2/TWfz7de7qEiWcYVTd7IKbWXQ0z4H4+bqzvEwxVcp4By9ODkCg19DvQljHjLvGASJtr2yzdV1vmW2mq8DnkVS2ShogP79nsnM+GQEI3OL/m2962Xv14xuTpSnjeHprXHPIE2XO5K0Xx9sAKfqrrSWrCWAVT1GbmITdUiqsvvV163u2RTlhTpQ9mUusvVJDEEOsdB8EaIjSeHpIvYgtHdMp2hkEm2kgbjwWCi9xpNc9CAsEfR6ZwwD2qReCfwBXU/ZzQJ11VoQlacmDq4n0qudAnCSbKbeh5d8UUwKeKa9GENfOtHL/0kPNgdFrHgOh4fIysPpzyiw8lVlJiuIZxeWmEkTSpLzd1Gteb7/Ce8BGMrD8tb3QMHWy8wp03SosFkcwu/0VAKe+/qjrQW59OwR6p6cEZfOQWaCE+GvFIMSMtD4nmdBFa7tE1M+/GwHtCa/sktghTFaCa59pPEzRu3xMtQAdJrLqzIuQPdMc628OUd4ByD1HAjtUD7GTwt0f+CDDFcVMlR8NLLAUamR2QN8KALdYCBSxKWSOlZhcSoZ9955QYrm2UNvLwkBlIMJ4xdzs9/Q7A3zEKB3g48r2Hf7Dkkd5iebrVMmSVsiX7QrGWZYRzbIKdVRfwN5UQ1P7VpsbL0vW4R5MsYWB7SHiO6eFzcaWBqQaoRPb18roSJXLXVT7u2NiGr/rlwgLhVXsRUAwgXXOeaSCWlEcJEGSXRqhmPXmGlJd2784Lk+4KEwlGiaby0zGQjcFGixKDlMIuhPqBLXO5XQJPNPQVHa3VTp/MQLORBuCQ/aPz/YfdoVqFvDw5OLTqGNr3GJDruOnJHxxZoGCKRdhiAd5fP8xzTXP89JgoNRZoQ4tSXOjMqt28noEFgH3IBYLI7Qy4Rj/tO//1PICthNaS9ZOr02x8eCKhV3YmZz5JWO+tTfonWAsT2qXUkj+mLkGVPc9ufEN2k55LQLYuqEwLldct4ZCuGoLu7l993Fs274QrLW4JMx71navreHhzplSmMcHX6aFoVlJzaR4DjN6780TB6nEOAevDRphZlcfJUBwWbM/trLvCE6B5dAnMpl77Eg6Sl7r43OuWbIbfzOyrtSpKelLGEC+xwh0rsFfSRScmy3789a39DR9O+WK3huFYDbhyli56EPuefMbiTh9Ky8oA7lKBsJZ1EZbb6agvfrNS+4S6XGQTx97Lj+FWWT2a122DfzEvaK3VyfLcTe7wei8NZghujitHokZ+oFmbxAF075trf10l4AVdPMkuixtHuEwdJ4EBCFt2Tuil+cFadP0eZuE7pnp4CiMJOeMciEpDR9LZOm3V4rnTBYRJgleXvbSYqNjJGIPeyXm0WyfQ8CJQaV81VmpbEcYPv3ygsLCkKjkD/MPtvx/F2ZXrASC+M3H6t2G5iGMCC1ejUAEDevw++6wEj9AhdGZh+nAfMo7mb/SQXCQ9/KUINux7+dDlS+YNLlKFM0SZrzu2tvAKhx8hmD+cgRsfdNuAq2USIKJMA0vHV+MOBWC2csRSKRv8VPmC0vC0Y8Vh1Bvb7Q+pApVzBCMoES+IU/fS7t6bxW+MF7akZ9eZkZnnaon3AX31cYpEK4Dj2tHj7lrU0TimK/GtoVtqfbBbmsuHHM9wDb5t0d674lJxFoSbXX2RE6ZEDBkHquge08etXyl/SlLh78Z+un3r/iOZM74jU0Ql7FQ/W090UC3Jfe1XSqY1Uwnn19sXgMR2AgGxI4OBi8VnBc85DjaHiWStriiy535o8nFR19tXktFOnnCE5p2CA7DlCMwDJrG+gWtNYz1mdfL7rK1QtoINgK3hZZHtpOnJRsxJUDe5uo7iuOcLX6wWJPpw5cbRoy0aiN7wiZCQYCCUEM5YxKLPsp1MxP19WYFo36dvORG30Sz/LsPFN6CTOhsBGYF93kbxP3Ot4mZowTqcZyZ7gWNRRJgKh3sIqK7Ok5G0XEkclFlsIhXckWUMBfadacJWNGealilk4aXAHkjQUnPwmY7eQxF2eBqMrHZiFuARiAGSqlaxBodzyrsa8J4iuTIAPdmkTJGO4wAgaRe0HKOaBXr5eFc2btcKHA8Z5hMR0VVGvvR2iOFeTZGGBhOjsaAgAZoii9u+RDeIyq0xIVSfWGAa0AO4RU5JcdrzHg6AK4JpvHrPWVJbATHWNVWqk0IlJ5eqIImUcqeBE0yxjBmAcGCKCv7V1Yxu+z1fqbYCCm15brI42LQLVSOfyAdTwieJwvE3JxzYVvIDGoKaMiVibFR9BkRZoE6mmvV4dO9ylDl7A5DPQX1UHheMXwrYQRBuDXDHPk6QckoNsIMbrhTll1nCYijJROUybvFFqpsYylBNmcvuJzbIWy7Ex/pGlqPAQGPADumVrDR6Q3tQkLsmM5OKuhdhqfY5oWLz6AyBxkvPUJ331tIMUTyHmLVnlsy0SlWMVFNI0ayKGU2xTZPheAh1gG5hDk4TshcoDHYdsKw0I1GDmfnUx+9HdBSaIm1FkJOp5RG2ANZREwddvw4tjIwyayqOMs8/4A8nq0t29MS6k01owobEe6agb6LeDnq64ky1+Akq8VDOM1gT2tkiU7cQD0w5aBBxVfXIdPZS+OwK0QE2KOt6t8lFT8rXET1JgoMy6WncTQ98szsirbq2w9MEwCws2F48/7tSnSPKYdRODkSvYKsxK2sFFs1LyQ8O8OOmoo9dB0FG/GwUdZuk5gzi8hhUmrz9uAuE20snacUQo5RK50wVYGLdsg30XYYAntu0Rg2Ajjth2CPWwUtIIMACc4e8DpWxhD+BbLYI8iXjRgk2kSx0GcRM1QHOYWE7GF950IHiSmwxED+tUaQU4yhWg0pDBc8hP79LK9HwkaQUjMU1KzhZ4KeUC1e6uwtthOIqN6UO7XVzCQZhYUN/gqaSa3e0htwB8HMyNvDMyChyINsW9qN4AU6vDoxLogJ1IPcTvI7RVkF4zDIKRQV2KQZryYTCPq5o2AjSbohT2oh2C1D3o5G9BNii8e8FphYTQT0pODVGFI5QMzjn9LGPgjA5s4nEncoP1Ld9MDGDlQ9adxxau/gMyMIYQPJG+Q3Al9r0Wa3mTAZxFTvO0ILeHcjZXZ9y5lhTvT1OJwEnhfb0253IIIGG0p4bPLG+0XmZpkl4R9KMZ2j9TieAczxql3NJuUOrXvq9iRvQpAIBO39kTwEdqPvesIFTb36dXPSXDHmWYdx+DncTLOrHsGgF8sqZZecGntXFnw3Di9BYtoJhWV70Nqp26Q2WRkGsKUY2ae6/UOSIx/SV+/jMI4gxf9Rd+dUD2GaUNmZ56KCRGyIfcU2hKpgAY7EQoWoRVzPTCglqulETKFhY1FLqtozI04E4exxBI64u/muy1UY+7BgdsYsFv2KzGOrmvU/6hQzjmPqnQK0GUF2XBKB7Awb9RijxEY7kf2oJhFMldhstpFqGlETpkhm4+Aj8JVjClohqyvgeZKAOOULV7kreIhPQwgVvGLFd6TuoKg/gZKQUv58pCBoDGVevrC+L1OQsPm2G4d5BGptxWfCvVepNQ/cM7lh9e3Ce47B1u46CgQSyaCxvd2Ek0Bak7qNGhhP7USRCGCfj2Ir8BJOs1XRn6443zR3ONoHs7DJ96lMGPZxbAVQaXtMGrn+qemm4Elqne60himBswRzX3sUjKRewGogzd5rhXcok3A/BIHoS57Pmo5iypRIS9HWTKnz0oDmcK0DoQ5cc39UPowln8CUwl6n2/ZqbQ2LWPB2vkZXsU82zfUaEo4aNt9LibBbfTXuivt9yoPDu4g6pe3UsIag8AWcZm8MEGBP7jR6N6olD2X+WmrQ8MbDY00PWYeOtcNIdDNw0PJuAhajIBTsk5g0hv81qxKInhDAfuxrLv92wuxehy02Oi63KXgIe3Tgnc2Zqzl++3bLR4WCTt+PwbwgxhFl7flwtzx9v8BchFvo8fEIl1hrN0sxaephaBDJX7XsJmHw51sPAQyNdQycNd1GHnd2TnbYwmC9IZWA7z+KYDCeoZ3qNLTwezBb8ug8+Dnr0sC4Og0MTJqyrY23Y5GpObuVsltt5xu0mKqmwkElYHQEc5j5cnNrDNSHuAvIVGUEEGAcxIQSOi8h8JJsgsmCPYkzk0XobaZT4wLjCioMGOwnop7HAQE73FJTK0tiLvOiR0Lx4gB8wiynJMKGq6Bbk5eKHit7BBNR2W2+kNZskcyU+XQIMPsCg+rQTD7N0gs00TYEqwq1eCgZgZVHr5AbIE2hOk3spZ2KfZzGZqMiJq7vOYTOnATlxsu1M2Fe84wSbaDbCLX9haSelcjspzXTe4fXjx4kPAtI1Diowo5FuhMKy1Ggx01wt1oX46Tc/a/mJOCHLSWYVsss4nYH/u/KN5sVMZuGOcEk6pRZsqn1ak6Cwikbk4HJm6jTYT6tGYls0RGGphzcV3MSWKi5iochcDKjOAWfblPfcRoxcO83xmt3EbJvJm5ku5Tejw99cxUmlJbwhoYx8aC6p1ENj+UC9XHy4rG0HIJPcXvsWubnKLOot34DozujuEhgNfsVaOFNoEx/LStx73hC1ShuOPrvCUijJNKFw7mUqScqrNJGMoRQJxEBb+XTO/opB1/Q6B0YiVC1ZxItQ3Vib09jXQ0nc8VzWiN0CdXnPAC49j61HQg2TP0JGHC4BggMy+TBdr6kiu+YYai3UgkES+YW3OuPxZ+3A3WuJ7TPnuCnEi2DKASR2mP4+mnfLUdiBq3DM9IufvgciIgU+o+qyv4dgsTUUwZTv4SAUYwi4BKbd6OaerLLxBQG69SThrU0scRXnXoH0f1/4kXBx8ATZsWpiuidYQJ2M6zRm8wh5TTb43JoKi2pzMT0SQqEhp3Ib31EQlJZ4IHvoWDg82WWdOWpPdxUj84/PS3WQdSqiu0IuYRzBxFHEG/2ZOAWNoaJpr7W6ZYkRmYUpQ9PDJmR6jae0/vFwyE63HFITUK1yr1pFvOEOTqVdblprMu0kpvE2qAiVfrcsCVd1Jbc6g7F20ea8lDSMjhsijtJmH7gcwl5/B67Cc8EnXl6fRNMdDFgjqh1O9mT1TcudDBSPG7KrZMeVhLEBQoJPKp7QZou/RD4BjSWu3Cq8AH6SDzHoHJVOyMmfbR02hdraUJ8tvJsiWwVwNyMwRnBMGKKJjN5WQ7E2Gx1SELAUYRR68rdqi75TuGBEV23qvFhNEotwW7kiw7bwpoNka7l2aNRJyPSpjmVk4NXPqMALMMsEqKUGLOeYixOh1Wer4/vp4vtStArDft/t40MVbV0M3JdR/3/bEaoyZhZCW+HiPqDTMRoX+zN43a3D6kI2+w9PpqKcZTuDQ0Zrijfekp3iDgMN/zfEBgG7x2dJ9Hm4tDJ0KzmchUfyN5d2RagK5S70OQwM6ByEgamUj0WTLj2+SxzzfHj4OPc+YakG2n4kmomDGbqR+Rcc5c6GSrOwxpEQVVZohuFllUne3vCtBqAELEcjTGXLAYzMpJ0sL6qRwTn8gSTyAk1I+QT9L9okB41AuAJR8nnUrhcRz0letUuic2jB32ozp69tBSC6D3xTD0KxDa/ZOnq8pEltuJN3Mjn/dLTpgv+ltjB9aNg2a8aBUhomECW03QZa1se3Jx9i2n3ExGzB1sNXRjedaYt0VIHttu8o3+nrxsoydwXmzHtdaFSgnE425+UyA8qrj2dG4+Bk8nkyu098Y/z5THuSXJ9kr6yGotQI2a3dK2ETLVSa5GdFAMbi2X2YFfsoo/D6qC5vZMA/0zn4IvbrwiW+elRuafBoQhMzk/nDqaS5d/5DeyiSvZmy/t3/0hvuT13rCcEPpgt6gKIzAORSBZs6VPbc50/ivu/oaniWryZ4MuXX4BgC4cFX7auTBlnJfd66N9ovefMloUPh398GYhoBht3nWwnOsFMO82yffNcrN2xf5UDdLzCKG5KLf/mf1cAmWkkYzH74Hdtl9mxa6iWYUaLief5kygkRpyvIRR5Ouwvm9Vl/0CiHjO/mrmCvzUAzuxOsA3svGCjaWrHkW5YPPXD/iJGGOqGKnSv7q3ROrp3lvZw918abAUYqm4utkyNCrF62mSpPWe72zHNMDRNx5nEdprvz+j9o154HsTRZOGEjyfefBsJ003WDJ2Za8sgzS8DOmJ3yRN7Pgl1i4C/Nr7jdmFJnH9kVTOMMPJjZTmbzZR57PmuoxuapRLKl/7qlX6GBJqaRnjDf+mXfumXfumXfgph6dllQ4Q880BRInnT81Mbtwg2MxQ/zZfEEYRSnlmLiS0eAnjOl4sI11MRYMkXIe3PLeihXUhIJ1iPcXUa4pPO/4DAXxkt+8z1mj9xdBXTXmLurR46kTtZlHMQZK/2pZ6DgN2hOpL/mes5u513h/V+ymjFaAP/ma7Xh93u7Vw64UWgmzQD4E/pIm2fxvCZDzyueKpIDAQpDwYV9JxR4x/NX/GZD8ifC/sVzU3Ea0Q5n4KAOM3H/ClpSlsnTb6tBR0Ou7dmGKdkGEy8ylF/zmlKlaj7NAKpecx5rBV7WmXuuG5G8yK11GjAp1XZw7Nm45I66f9ZjVZPjWpm3nm6lAh0zWYnHD0ZQWNBn9bJapdZGl/GIbTG4Jbdms9DUB2u8mkEdDOIAMbhdtRvWe74xAnL9K8RBA8QANO0RH9ZBf9MBIe/RFCdHtCno2CsV6sbhUyej2D6pQgmqF3w9uMQMK6ZtQdG/TgEUqdY6wci6NAvgk/QL4I/0C+CT9CXIejPnZcInlEVXdBXISBmZvfMbSgRPPE4qBLBpz8wgAAMvp4b/YMQ4HfUd7TwJxDwtOUfyjfKa/qCPV+EgA8+6Qkt/gkBW5ke+dul4jnaUCEBoZoziWdLJfajULtL3HcQiBORi6RnN4L1AAFY0ce/3gcYm0pVpnhY9tavE+xeG+Wd69u8M5ezjYCYl9U1mM09bwK0cF13ET1AQIpsL4XzBHY9q3yEAJNORdS6pwuChndVU7fODmwhwH3xlPaJeRWCiMqWETrRxPPiJYQu930+zyACQv27Et67XhbRU9WhY2fga/sZ9LXMtEVM43y0DvUdWjqIAOtFuCqZ+NuqgLYrzUoXL4l9368OO24/qs4+wJY7b9caHtM23w0j6DvpZggBLoMkc9h11C/iTZ3pasQQ1VoWyCFMy4FOk0cI+F4u6/rXy4UB5bSfRNA3A2cAQRXxLM5TrpilfVCL6MM5V/kZrPIAV7vd4F6aEku8lLuE9sjfCsEqB7p9TNfFDezLEfQjINVoqbKKsozKtKsq1Uvn2RICtVG3xwiIyguo9u6AeK5lkSCiavpk/5cIyrGu1eTW8qG0EBShoOaYeCJfYMLNIwRE45XltjqkRHv0AcHG4a8QSIaoBsyqfsxShLQRiJnsa9x9rbWVuztZh7W8Pxif2qvRePTyEYJO/oBv5KNSpziJ24egeFStPiHsePqDnSxaRQ7mAztswKpY/xUCttlMvdk22o+gnIvfat3vHoDXQiACn/uHExf6EUDj4iNpep/DaQuJXgRS1TA3jYarLxsIiibNzePk8sAzWEDPw98g6FzXtw8kXFdlXvgQ6scIiCb0gPPYlB+wrtV9b5bjswjKNEDnxNxm/9Pe6y8Ar3w0Wo7t+cPIziEfzdL7kH8GAaQenHUfgk7q7jQzejCUCJw6OfP4ALNBT7/3U39CwGwFOZwsq+6+uz6B5mh/pp5n93u0RNCgx/Mu/y1W0YuALV/32iXvdwiI117cyes+hh4Ej/s0vwwB9IKsuoXg970aVU9kdYPVfmnKaG0XdfNvj/joqxDUaci3IHYMewgBe1JOe0rerfNFFYKZXM0EeHTI8dcgwFbB4O+zEHzaMmXY2y9DRPtaRe0vqxCYtDimB+jB3NQvQYAjYZy+x0SoqlKjDXTbtxLHHVlTIuC8X1cSDPdcfwUCXOivaTUXpEQwyL+Ed+OVN7i5uhJBcaJfKVPPg3NT/w1Bq7altHfOtQrpR9D+gTql09/S/h3btBr5v3kmgoIjGjnD0qpoISB658vk4oOX5qvdeFHFR0MndVQDcf4ZQfU7Gv5uHwI4DK7jN6titfvm0Puuh1PXKQxYFzT53wjMzyGA0M+2ExsSt2/aOhqlQFA5bvXEnn6BVAmvT0W7y5U1B2JUPub8notEo37xm0BHtO3MIsTR3geFCV7tXNIYp9cHQS7f/dT5r2XA59Sc71NWQlzqjVyeyZxA74qu1gjOLYOxeAYtWYSLb6tPaiL1iWGze7+iLs+xP9PyVZ0rsG1crZWOi837vtlfYWU5HG+7cqcWqBohB+7nd3mtbHJucH1jXE/WCSgT2jgQLTF5J9KD9TcHHNhGdTGuJPsmdkxzMuu2pL/x05jKEGjqyFxtY1yMrmnKMEyquKrD+6IEhMbI5FiTcBnwJloYtxrid5k9j4YPMSBOyzbO7bg4ALR1LpygloEpag1xNbtpGsT+xI8TYQfmjd9gxI1au5UdR2WJXFOF50ky4xFZsmh2TFY0eOof6aksFBCw1/2OiNQh3lUxVhD391M3bFOyuG/b9MRNkjozk/l04YFax8GTbNWeiwu7pw0hMAmpNtjeK2URMXomvewasyCl9kxeQYUeKyOPJfF9zvZbYm+hp41RtPC9eGYHgT1sysxuWWDPtrHnLRYuu1yxvfI9rBfzAlAWF64j9nbofJ00Qm2EGBM7b97mzNNaYgjsg90lTwN7uVQYzeykPPWrGHZ+XF/ya5oUv7hKZpEq80EfbWUYuULLNjjRD1e/h61w4ru6VYd/sGqo3cIS9q6qRX68VZS5zyRHx1AglmapfORD0W9Hm4IrdNi7qsx/+eAi/w/dp7gGTOuyJ/Bv6/N/uwd/6Zd+6Zd+qYf+A8w9o2wW1usLAAAAAElFTkSuQmCC" className="card-img-top" alt="..." />
                                    <div className="card-body text-center">
                                        <h5 className="card-title mb-0">Flask/Flask restless</h5>
                                        <div className="card-text text-black-50">micro web framework that we used to build our backend server and API</div>
                                    </div>
                                </div>
                            </div>
							<div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-0 shadow">
                                    <img src="https://pbs.twimg.com/profile_images/476392134489014273/q5uAkmy7_400x400.png" className="card-img-top" alt="..." />
                                    <div className="card-body text-center">
                                        <h5 className="card-title mb-0">SQL Alchemy</h5>
                                        <div className="card-text text-black-50">used in combination with Flask to interface with our database layer and build our API</div>
                                    </div>
                                </div>
                            </div>
							<div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-0 shadow">
                                    <img src="https://www.selenium.dev/images/selenium_logo_square_green.png" className="card-img-top" alt="..." />
                                    <div className="card-body text-center">
                                        <h5 className="card-title mb-0">Selenium</h5>
                                        <div className="card-text text-black-50">to test the GUI</div>
                                    </div>
                                </div>
                            </div>
							<div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-0 shadow">
                                    <img src="https://seeklogo.com/images/M/mocha-logo-66DA231220-seeklogo.com.png" className="card-img-top" alt="..." />
                                    <div className="card-body text-center">
                                        <h5 className="card-title mb-0">Mocha</h5>
                                        <div className="card-text text-black-50">to test the JavaScript we wrote in our frontend.</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="section">
                        <h1>links</h1>
                        <div>
                            <a href="https://gitlab.com/simc97/the-buzzz" styles="text-decoration:none; font-size:large;">Gitlab</a>
                        </div>
                        <div>
                            <a href="https://documenter.getpostman.com/view/10505643/SzKYNGXS?version=latest" styles="text-decoration:none; font-size:large;"> Postman API Documentation</a>
                        </div>
                        <div>
                            <a href="https://drive.google.com/file/d/1sUfgj05u308DIczSzBzwQ2SKqJiBVouI/view" styles="text-decoration:none; font-size:large;"> Presentation</a>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}