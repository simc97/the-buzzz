import React from 'react';
import ReactDOM from 'react-dom';
import {shallow, configure} from 'enzyme';	
import Adapter from 'enzyme-adapter-react-16';
import Navbar from './components/NavBar.jsx';
import Home from './Pages/Home/Home.jsx'
import About from './Pages/About/About.jsx'
import Countries from './Pages/Countries/Countries.jsx'
import News from './Pages/News/News.jsx';
import Tweets from './Pages/Tweets/Tweets.jsx';
import CountryProfile from './Pages/Countries/CountryProfile.jsx'
import NewsProfile from './Pages/News/NewsProfile.jsx'
import TweetProfile from './Pages/Tweets/TweetProfile.jsx'

 configure({ adapter: new Adapter() });

  
  describe('<Navigation />', () => {
      it('renders a Navigation Bar', () => {
        const nav = shallow(<Navbar/>);
  
        expect(nav).toMatchSnapshot();
        
      });
  });
  
  it('TweetProfile', async () => {
    const component = shallow(<TweetProfile/>);
    const data = component.instance();
    await data.componentDidMount();
    expect(component).toMatchSnapshot();
      
  });
  
  it('Tweets', async () => {
    const component = shallow(<Tweets/>);
    const data = component.instance();
    await data.componentDidMount();
    expect(component).toMatchSnapshot();	
  
  });
  
  it('News', async () => {
    const component = shallow(<NewsProfile/>);
    const data = component.instance();
    await data.componentDidMount();
    expect(component).toMatchSnapshot();
  });
  
  it('Countries', async () => {
    const component = shallow(<Countries/>);
    const data = component.instance();
    await data.componentDidMount();
    expect(component).toMatchSnapshot();
  
  });
  
  it('CountryProfile', async () => {
    const component = shallow(<CountryProfile/>);
    const data = component.instance();
    await data.componentDidMount();
    expect(component).toMatchSnapshot();
  });
  
  it('About', async () => {
    const component = shallow(<About/>);
    expect(component).toMatchSnapshot();
  });
  