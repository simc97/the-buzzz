import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Navbar from './components/NavBar.jsx';
import Home from './Pages/Home/Home.jsx'
import About from './Pages/About/About.jsx'
import Countries from './Pages/Countries/Countries.jsx'
import News from './Pages/News/News.jsx';
import Tweets from './Pages/Tweets/Tweets.jsx';
import CountryProfile from './Pages/Countries/CountryProfile.jsx'
import NewsProfile from './Pages/News/NewsProfile.jsx'
import TweetProfile from './Pages/Tweets/TweetProfile.jsx'
import Search from './Pages/Home/Search.jsx'
import Visualizations from './Pages/Visualizations/Visualizations.jsx';


function App() {
  return (
    <div>
      <Router>
          <Navbar/>
          <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/about" component = {About}/>
              <Route exact path="/countries" component = {Countries}/>
              <Route exact path="/news" component = {News}/>
              <Route exact path="/tweets" component = {Tweets}/>
              <Route path={'/countries/:country'} render={() => <CountryProfile />} /> 
              <Route path={'/news/:news'} render={() => <NewsProfile />} /> 
              <Route path={'/tweets/:tweet'} render={() => <TweetProfile />} /> 
              <Route path={'/visualizations'} render={() => <Visualizations/>} /> 
              <Route path= {'/search/:query'} component = {Search}/>
          </Switch>
      </Router>
    </div>
  );
  }

export default App;
