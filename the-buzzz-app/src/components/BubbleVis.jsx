import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { api } from '../instance.js';
import BubbleChart from '@weknow/react-bubble-chart-d3';



export default class BubbleVis extends React.Component {
	constructor() {
		super();
		this.state = {
			news: [],
			loading: true
		}
	}

	componentDidMount() {
		api
			.get("news")
			.then((res) => {
				const news = res.data;
				this.setState(
					{
						news: news.objects,
						loading: false,
					}
				);
			})
			.catch((error) => console.log("OOPS i DID IT AGAIN " + error.response));
	}

	getData = () => {
		var data = "";
		this.state.news.map((element) => {
			if (element.title) {
				data += element.title.toLowerCase().replace(/[^a-z0-9 ]/g, "");
				data += " "
			}
		});

		let obj = {};
		data.split(" ").forEach(function (el, i, arr) {
			obj[el] = obj[el] ? ++obj[el] : 1;
		});

		var stopWords = ["i", "me", "my", "myself", "we", "our", "ours", "ourselves",
			"you", "your", "yours", "yourself", "yourselves", "he", "him",
			"his", "himself", "she", "her", "hers", "herself", "it", "its",
			"itself", "they", "them", "their", "theirs", "themselves", "what",
			"which", "who", "whom", "this", "that", "these", "those", "am",
			"is", "are", "was", "were", "be", "been", "being", "have", "has",
			"had", "having", "do", "does", "did", "doing", "a", "an", "the",
			"and", "but", "if", "or", "because", "as", "until", "while", "of",
			"at", "by", "for", "with", "about", "against", "between", "into",
			"through", "during", "before", "after", "above", "below", "to",
			"from", "up", "down", "in", "out", "on", "off", "over", "under",
			"again", "further", "then", "once", "here", "there", "when", "where",
			"why", "how", "all", "any", "both", "each", "few", "more", "most",
			"other", "some", "such", "no", "nor", "not", "only", "own", "same",
			"so", "than", "too", "very", "s", "t", "can", "will", "just", "don",
			"should", "now", "us", "1", "2", "3", "4", "5", "6", "7", "8", "9", ""]
		let bigContainer = [];
		Object.keys(obj).forEach(function (key) {
			if (stopWords.indexOf(key) !== -1 || obj[key] < 3) {
				delete obj[key];
			}
			else {
				let dict = { label: key, value: obj[key] };
				bigContainer.push(dict);
			}
		});
		return bigContainer;
	}
	render() {
		if (this.state.loading) {
			return <h2> Loading... </h2>;
		} else {
			return (
				<div className="bubblediv">
					<div className="bar_graph">
						<h2 className="my-4">
							<strong>Common News Topics</strong>
						</h2>
					</div>
					<BubbleChart
						graph={{
							zoom: 0.95,
							offsetX: 0,
							offsetY: 0,
						}}
						width={1000}
						height={1000}
						padding={0} // optional value, number that set the padding between bubbles
						showLegend={true} // optional value, pass false to disable the legend.
						legendPercentage={10} // number that represent the % of with that legend going to use.
						legendFont={{
							family: 'Arial',
							size: 12,
							color: '#000',
							weight: 'bold',
						}}
						valueFont={{
							family: 'Arial',
							size: 12,
							color: '#fff',
							weight: 'bold',
						}}
						labelFont={{
							family: 'Arial',
							size: 16,
							color: '#fff',
							weight: 'bold',
						}}
						//Custom bubble/legend click functions such as searching using the label, redirecting to other page
						bubbleClickFunc={this.bubbleClick}
						legendClickFun={this.legendClick}
						data={this.getData()}
					/>
				</div>
			);
		}
	}
}