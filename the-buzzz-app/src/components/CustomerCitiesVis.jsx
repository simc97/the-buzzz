import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { customerApi } from '../instance.js';
import BubbleChart from '@weknow/react-bubble-chart-d3';

export default class CustomerCitiesVis extends React.Component {
    constructor() {
        super();
        this.state = {
            jobs: null,
            loading: true, 
        }
    }

    componentDidMount() {
		customerApi.get("roles")
        .then((res) => {
            const jobs = res.data;
            this.setState(
                {
                    jobs: jobs,
                    loading: false
                }
            );
        })
        .catch((error) => console.log("OOPS i DID IT AGAIN " + error.response));
    }

    getData = () => {
		var data = "";
		this.state.jobs.map((element) => {
			if (element.skills) {
				data += element.skills
				data += " "
			}
		});

		let obj = {};
		data.split(". ").forEach(function (el, i, arr) {
			obj[el] = obj[el] ? ++obj[el] : 1;
		});
		let bigContainer = [];
		Object.keys(obj).forEach(function (key) {
			if (obj[key] < 3) {

				delete obj[key];
			}
			else {
				let dict = { label: key, value: obj[key] };
				bigContainer.push(dict);
			}
		});
		return bigContainer;
	}
    

    render() {
        if (this.state.loading) {
			return <h2> Loading... </h2>;
		} else {
            return (
                <div className="container">
                    <div className="bar_graph">
                        <h2 className="my-4">
                            <strong>Customer Visualizations</strong>
                        </h2>
                        <h3 className="my-4">
                            <strong>Desirable Skills</strong>
                        </h3>
                    </div> 
                    <BubbleChart
						graph={{
							zoom: 0.8,
							offsetX: 0,
							offsetY: 0,
						}}
						width={1000}
						height={850}
						padding={0} // optional value, number that set the padding between bubbles
						showLegend={true} // optional value, pass false to disable the legend.
						legendPercentage={30} // number that represent the % of with that legend going to use.
						legendFont={{
							family: 'Arial',
							size: 12,
							color: '#000',
							weight: 'bold',
						}}
						valueFont={{
							family: 'Arial',
							size: 12,
							color: '#fff',
							weight: 'bold',
						}}
						labelFont={{
							family: 'Arial',
							size: 16,
							color: '#fff',
							weight: 'bold',
						}}
						bubbleClickFunc={this.bubbleClick}
						legendClickFun={this.legendClick}
						data={this.getData()}
					/>
                </div>
            );
		}
		
    }  
}

























































































































































































































































































































































































































































































































































































