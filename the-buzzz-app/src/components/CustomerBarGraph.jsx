import React, { PureComponent } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { customerApi } from '../instance.js';
import {BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip} from 'recharts';

export default class CustomerBarGraph extends PureComponent {
    constructor() {
        super();
        this.state = {
            companies: null,
            loading: true, 
        }
    }
    componentDidMount() {
		customerApi.get("companies") 
        .then((res) => {
            const companies = res.data;
            this.setState(
                {
                    companies: companies,
                    loading: false
                }
            );
        })
        .catch((error) => console.log("OOPS i DID IT AGAIN " + error.response));
    }

    getData = () => {
        var ourCompanies = []
        this.state.companies.map((company) => {
            if(company.company_marketCapitalization !== null){
                ourCompanies.push([company.name, company.company_marketCapitalization]);
            }
            
        });

        ourCompanies.sort(function(x,y){return y[1] - x[1];});
        var dataforChart = [];


        for (let i = 0; i < 10; i++) {
            let company = ourCompanies[i][0];
            let cap = ourCompanies[i][1];
            let dict = {name : company, market_cap : cap};
            dataforChart.push(dict);
        }      

       return dataforChart;
    }


    render() {
        if (this.state.loading) {
			return <h2>Loading...</h2>;
		} else {
            let data = this.getData();
            return (
                <div>
					<div className="bar_graph">
						<h2 className="my-4">
							<strong>Top 10 Company's Market Cap.</strong>
						</h2>
					</div>
					<BarChart
						width={1300}
						height={500}
						data={data}
						margin={{
							top: 20, right: 30, left: 20, bottom: 5,
						}}>
						<CartesianGrid strokeDasharray="3 3" />
						<XAxis dataKey="name" />
						<YAxis />
                        <Tooltip />
                        <Bar dataKey="market_cap" stackId="a" fill="#61d4b3" /> 
					</BarChart>
				</div>
            );
        }
    }
}