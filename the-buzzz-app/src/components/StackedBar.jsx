import React, { PureComponent } from 'react';
import {
	BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip,
} from 'recharts';
import { api } from '../instance.js';
import {subregions} from "../Pages/Tweets/subregions.js";
import {colors} from "./colors.js";

//Array of dicts. name: country name, topic: tweetVolume...
//iterate through every subregion to get its top 5 or top max tweets

// name: 'Australia and New Zealand', uv: 4000, pv: 2400, amt: 2400,

export default class BarGraph extends PureComponent {
	constructor() {
		super();
		this.state = {
			tweets: [],
			loading: true
		}
	}

	componentDidMount() {
		api
			.get("tweets")
			.then((res) => {
				const tweets = res.data;
				this.setState(
					{
						tweets: tweets.objects,
						loading: false,
					}
				);
			})
			.catch((error) => console.log("OOPS i DID IT AGAIN " + error.response));
	}

	
	getData = () => {
		var obj = {};
		this.state.tweets.map((tweet) => {
			if (tweet.subregion && subregions.includes(tweet.subregion)) {
				if(obj[tweet.subregion]){
					obj[tweet.subregion].push([tweet.topic, tweet.tweetvolume]);
				}
				else{
					obj[tweet.subregion] = [[tweet.topic, tweet.tweetvolume]];
				}
				
			}
		});

		var bar = [];
		let bigContainer = [];
		for (let subregion in obj){
			let dict = {name: subregion};
			
			//iterate through list of tuples
			var colorIndex = 0
			for (let tweetData of obj[subregion]){

				if(tweetData[1] !== null){
					let topic = tweetData[0];
					let volume = tweetData[1];
					let color = this.randomColor(colorIndex);
					bar.push(<Bar dataKey={topic} stackId="a" fill = {color} />);
					dict[topic] = volume;
					colorIndex++;
					if(colorIndex > colors.length){
						colorIndex = 0
					}
				}
			}
			if(Object.keys(dict).length > 1){
				bigContainer.push(dict);
			}
			
			
		}
		return [bigContainer, bar];
	}

 	randomColor(colorIndex) {
		return colors[colorIndex];
	}
	
	render() {
		let res = this.getData();
		let data = res[0];
		let bar = res[1];
		
		if (this.state.loading) {
			return <h2> Loading... </h2>;
		} else {
			return (
				<div>
					<div className="bar_graph">
						<h2 className="my-4">
							<strong>Tweet Volume by Subregion</strong>
						</h2>
					</div>
					<BarChart
						width={1300}
						height={500}
						data={data}
						margin={{
							top: 20, right: 30, left: 20, bottom: 5,
						}}>
						<CartesianGrid strokeDasharray="3 3" />
						<XAxis dataKey="name"/>
						<YAxis />
						<Tooltip />
						{bar}
					</BarChart>
				</div>
			);
		}
	}
}