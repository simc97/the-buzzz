import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom';

class NavBar extends Component {
    static propTypes = {
        match: PropTypes.object.isRequired,
        location: PropTypes.object.isRequired,
        history: PropTypes.object.isRequired
    }

  render() {
    const {location} = this.props
    const headerColor = location.pathname === '/' ? { backgroundColor: '#61d4b3'} : { backgroundColor: '#61d4b3' }
    return (
        <nav className="navbar navbar-expand-lg navbar-light" style={headerColor}>
            <NavLink className="navbar-brand" to="/">The Buzzz</NavLink>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler"
                aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarToggler">
                <ul className="navbar-nav ml-auto" id="navbarToggler">
                    <li className="nav-item">
                        <a className="nav-link" href="/tweets">Tweets</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="/news">News</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="/countries">Countries</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="/visualizations">Visualizations</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="/about">About</a>
                    </li>
                </ul>
            </div>
        </nav>
    );
  }
}

export default withRouter(NavBar);
