import React, { PureComponent } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { customerApi } from '../instance.js';
import {PieChart, Pie, Cell, Legend} from 'recharts';

const COLORS = ['#0088FE', '#00C49F', '#FFBB28'];

const RADIAN = Math.PI / 180;
const renderCustomizedLabel = ({cx, cy, midAngle, innerRadius, outerRadius, percent, index,}) => {
    const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
    const x = cx + radius * Math.cos(-midAngle * RADIAN);
    const y = cy + radius * Math.sin(-midAngle * RADIAN);

  return (
    <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
      {`${(percent * 100).toFixed(0)}%`}
    </text>
  );
};

export default class CustomerScatter extends PureComponent {
    constructor() {
        super();
        this.state = {
            cities: null,
            loading: true, 
        }
    }
    componentDidMount() {
		customerApi.get("cities") 
        .then((res) => {
            const cities = res.data;
            this.setState(
                {
                    cities: cities,
                    loading: false
                }
            );
        })
        .catch((error) => console.log("OOPS i DID IT AGAIN " + error.response));
    }
    
    getData = () => {
        var ourData = [ {name: "LOW", value: 0}, {name:"MEDIUM", value: 0}, {name: "HIGH", value: 0}];
        this.state.cities.map((city) => {
            var thisCost = city.pollution_readable;   
            for(var i = 0; i < 3; i++){
                if (thisCost === ourData[i]["name"]){
                    ourData[i]["value"]++;
                    break;
                }
            }
        });
  
        return ourData;
    }

    render() {
        if (this.state.loading) {
			return <h2>Loading...</h2>;
		} else {
            let data = this.getData();
            return (
                <div className = "piediv">
					<div className="bar_graph">
						<h2 className="my-4">
							<strong> Pie Chart of Cities Pollution</strong>
						</h2>
					</div>
					<PieChart width={800} height={800}>
                        <Pie
                            data={data}
                            cx={500}
                            cy={200}
                            labelLine={false}
                            label={renderCustomizedLabel}
                            outerRadius={200}
                            fill="#8884d8"
                            dataKey="value">
                        {data.map((entry, index) => <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />)}
                        </Pie>
                        <Legend verticalAlign="top" height={40} align='right' layout = "vertical"/>
                    </PieChart>
				</div>
            );
        }
    }
}