import axios from 'axios';

export const gitlab = axios.create({
  baseURL: 'https://gitlab.com/api/v4/projects/16944761/',
});

export const api = axios.create({
  baseURL: 'https://api.thebuzzz.me/api/',
});

export const customerApi = axios.create({
  baseURL: 'https://api.thesweetcities.com/',
});

export const flags = axios.create({
  baseURL: 'https://www.worldometers.info/img/flags/',
});

export const hiFlags = axios.create({
  baseURL: 'https://flagpedia.net/data/flags/w1160/',
});


