from selenium import webdriver
from selenium.webdriver.common.keys import Keys

from unittest import TestCase, main
import time

driver = webdriver.Chrome("/Users/sim/Downloads/chromedriver")
driver.get("http://localhost:3000/")
time.sleep(5)

class guiTests(TestCase):
    def test_load(self):
        driver.get("http://localhost:3000/")
        assert driver.current_url == "http://localhost:3000/"
        time.sleep(5)

    def test_nav_about(self):
        link = driver.find_element_by_link_text('About')
        self.assertEqual(link.text, 'About')
        link.click()
        time.sleep(5)
        self.assertEqual(driver.current_url,"http://localhost:3000/about")

    def test_nav_countries(self):
        link = driver.find_element_by_link_text('Countries')
        self.assertEqual(link.text, 'Countries')
        link.click()
        time.sleep(5)
        expected_text = "Countries"
        self.assertTrue(expected_text in driver.page_source)
    
    def test_nav_tweets(self):
        link = driver.find_element_by_link_text('Tweets')
        self.assertEqual(link.text, 'Tweets')
        link.click()
        time.sleep(5)
        expected_text = "Trending Tweet Topics"
        self.assertTrue(expected_text in driver.page_source)

    def test_nav_news(self):
        link = driver.find_element_by_link_text('News')
        self.assertEqual(link.text, 'News')
        link.click()
        time.sleep(5)
        expected_text = "What's going on in the news..."
        self.assertTrue(expected_text in driver.page_source)

    def test_nav_home(self):
        link = driver.find_element_by_link_text('The Buzzz')
        self.assertEqual(link.text, 'The Buzzz')
        link.click()
        time.sleep(5)

    def test_home(self):
        link = driver.find_element_by_link_text('The Buzzz')
        self.assertEqual(link.text, 'The Buzzz')
        link.click()
        time.sleep(10)

        expected_message = "What's happening around you?"
        actual_message = driver.find_element_by_class_name("slogan").text[0 : len(expected_message)]
        self.assertEqual(expected_message, actual_message)

    def test_back_to_home(self):
        link = driver.find_element_by_link_text('News')
        self.assertEqual(link.text, 'News')
        link.click()
        time.sleep(10)

        self.assertEqual(driver.current_url,"http://localhost:3000/news")
        home_link = driver.find_element_by_link_text('The Buzzz')
        self.assertEqual(home_link.text, 'The Buzzz')
        home_link.click()
        time.sleep(10)
        self.assertEqual(driver.current_url,"http://localhost:3000/")
    
    def test_tweet_instance(self):
        time.sleep(10)
        link = driver.find_element_by_link_text('Tweets')
        self.assertEqual(link.text, 'Tweets')
        link.click()
        time.sleep(10)

        tweet_instance = driver.find_element_by_class_name('card-body')
        tweet_instance.click()
        time.sleep(10)

        expected_text = "Countries"
        self.assertTrue(expected_text in driver.page_source)
    
    def test_news_instance(self):
        time.sleep(10)
        link = driver.find_element_by_link_text('News')
        self.assertEqual(link.text, 'News')
        link.click()
        time.sleep(10)

        news_instance = driver.find_element_by_class_name('card-body')
        news_instance.click()
        time.sleep(10)

        expected_text = "Author"
        self.assertTrue(expected_text in driver.page_source)
    
    def test_country_instance(self):
        time.sleep(10)
        link = driver.find_element_by_link_text('Countries')
        self.assertEqual(link.text, 'Countries')
        link.click()
        time.sleep(10)
  

        country_instance = driver.find_element_by_class_name('country-instance')
        country_instance.click()
        time.sleep(5)

        expected_text = "currencies"
        self.assertTrue(expected_text in driver.page_source)
    
    #filter tweets on Subregion
    def test_tweet_filter(self):
        link = driver.find_element_by_link_text('Tweets')
        self.assertEqual(link.text, 'Tweets')
        link.click()
        time.sleep(10)
        tweet_filter = driver.find_element_by_xpath("//button[text()='Subregion']")
        tweet_filter.click()
        time.sleep(10)

        tweet_filter = driver.find_element_by_xpath("//a[text()='Northern America']")
        tweet_filter.click()
        time.sleep(10)
        
        tweet_filter = driver.find_element_by_xpath("//button[text()='Go']")
        tweet_filter.click()
        time.sleep(10)
        
        expected_text = 'Canada'
        self.assertTrue(expected_text in driver.page_source)

    #filter news on Source... could fail if we update data & no news comes from CNN
    def test_news_filter(self):
        link = driver.find_element_by_link_text('News')
        self.assertEqual(link.text, 'News')
        link.click()
        time.sleep(10)
        news_filter = driver.find_element_by_xpath("//button[text()='All']")
        news_filter.click()
        time.sleep(10)

        news_filter = driver.find_element_by_xpath("//a[text()='CNN']")
        news_filter.click()
        time.sleep(10)
            
        news_filter = driver.find_element_by_xpath("//button[text()='Go']")
        news_filter.click()
        time.sleep(10)
            
        expected_text = 'CNN'
        self.assertTrue(expected_text in driver.page_source)

    #filter countries on Subregion
    def test_country_filter(self):
        link = driver.find_element_by_link_text('Countries')
        self.assertEqual(link.text, 'Countries')
        link.click()
        time.sleep(10)
        country_filter = driver.find_element_by_xpath("//button[text()='Subregion']")
        country_filter.click()
        time.sleep(10)

        country_filter = driver.find_element_by_xpath("//a[text()='South America']")
        country_filter.click()
        time.sleep(10)
            
        country_filter = driver.find_element_by_xpath("//button[text()='Go']")
        country_filter.click()
        time.sleep(10)
            
        expected_text = 'Argentina'
        self.assertTrue(expected_text in driver.page_source)
    
    #sort on country because only static data
    def test_tweet_sort(self):
        link = driver.find_element_by_link_text('Tweets')
        self.assertEqual(link.text, 'Tweets')
        link.click()
        time.sleep(10)
        tweet_sort = driver.find_element_by_xpath("//button[text()='-']")
        tweet_sort.click()
        time.sleep(10)

        tweet_sort = driver.find_element_by_xpath("//a[text()='Country']")
        tweet_sort.click()
        time.sleep(10)
        
        tweet_sort = driver.find_element_by_xpath("//button[text()='Go']")
        tweet_sort.click()
        time.sleep(10)
        
        expected_text = 'Albania'
        self.assertTrue(expected_text in driver.page_source)
        
    #sort on country because only static data however could fail if data changes because using source to verify because country is not on instance card
    def test_news_sort(self):
        link = driver.find_element_by_link_text('News')
        self.assertEqual(link.text, 'News')
        link.click()
        time.sleep(10)
        news_sort = driver.find_element_by_xpath("//button[text()='-']")
        news_sort.click()
        time.sleep(10)

        news_sort = driver.find_element_by_xpath("//a[text()='Country']")
        news_sort.click()
        time.sleep(10)
            
        news_sort = driver.find_element_by_xpath("//button[text()='Go']")
        news_sort.click()
        time.sleep(10)
            
        expected_text = 'Cnet.com'
        self.assertTrue(expected_text in driver.page_source)

    #sort on capital because static data
    def test_country_sort(self):
        link = driver.find_element_by_link_text('Countries')
        self.assertEqual(link.text, 'Countries')
        link.click()
        time.sleep(10)
        country_sort = driver.find_element_by_xpath("//button[text()='-']")
        country_sort.click()
        time.sleep(10)

        country_sort= driver.find_element_by_xpath("//a[text()='Capital']")
        country_sort.click()
        time.sleep(10)
            
        country_sort = driver.find_element_by_xpath("//button[text()='Go']")
        country_sort.click()
        time.sleep(10)
            
        expected_text = 'Abu Dhabi'
        self.assertTrue(expected_text in driver.page_source)
    
    


    
if __name__ == "__main__":
    main()

