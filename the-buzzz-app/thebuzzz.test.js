import React from 'react';
import ReactDOM from 'react-dom';
import {shallow, configure} from 'enzyme';	
import Adapter from 'enzyme-adapter-react-16';
import About from "./Pages/About"
import TweetProfile from "./Pages/Tweets/TweetProfile"
import Tweets from "./Pages/Tweets/Tweets.js"
import News from "./Pages/News/News.js"
import NewsProfile from "./Pages/News/NewsProfile.js"
import Countries from "./Pages/Countries/Countries"
import CountriesProfile from "./Pages/Countries/CountriesProfile"
import gitlab from "./components/gitlab"
import NavBar from "./components/NavBar"

configure({ adapter: new Adapter() });

  
  describe('<Navigation />', () => {
      it('renders a Navigation Bar', () => {
        const nav = shallow(<NavBar/>);
  
        expect(nav).toMatchSnapshot();
        
      });
  });
  
  it('TweetProfile', async () => {
    const component = shallow(<TweetProfiles/>);
    const data = component.instance();
    await data.componentDidMount();
    expect(component).toMatchSnapshot();
      
  });
  
  it('Tweets', async () => {
    const component = shallow(<Tweets/>);
    const data = component.instance();
    await data.componentDidMount();
    expect(component).toMatchSnapshot();	
  
  });
  
  it('News', async () => {
    const component = shallow(<NewsProfile/>);
    const data = component.instance();
    await data.componentDidMount();
    expect(component).toMatchSnapshot();
  });
  
  it('Countries', async () => {
    const component = shallow(<Countries/>);
    const data = component.instance();
    await data.componentDidMount();
    expect(component).toMatchSnapshot();
  
  });
  
  it('CountriesProfile', async () => {
    const component = shallow(<CountriesProfile/>);
    const data = component.instance();
    await data.componentDidMount();
    expect(component).toMatchSnapshot();
  });
  
  it('About', async () => {
    const component = shallow(<About/>);
    expect(component).toMatchSnapshot();
  });
  
  it('gitlab', async () => {
    const component = shallow(<gitlab/>);
    expect(component).toMatchSnapshot();
    }
  );

  