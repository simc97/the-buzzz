# used the the model of tests from https://gitlab.com/CameronEgger/inneedofsoup/blob/master/backend/tests.py

import unittest
from flaskAPI import app
import requests, json
import psycopg2
import apis

conn = psycopg2.connect(
    host="buzzz.cyg5ivkoczx2.us-east-2.rds-preview.amazonaws.com",
    database="buzzz",
    user="dbAdmin",
    password="thebuzzz",
)


class ThebuzzzTests(unittest.TestCase):
    def test_tweets_model(self):
        s = app.Tweets
        assert hasattr(s, "__dict__")
        db_info = s.__dict__
        assert "__tablename__" in db_info
        assert "__table__" in db_info
        db_table = db_info["__table__"]
        db_table = eval(str(db_table._columns))
        assert db_table == [
            "tweets.countryid",
            "tweets.topic",
            "tweets.rank",
            "tweets.promotedcontent",
            "tweets.tweetvolume",
            "tweets.asof",
            "tweets.createdat",
            "tweets.hashtag",
            "tweets.tweetsearchlink",
        ]

    def test_news_model(self):
        c = app.News
        assert hasattr(c, "__dict__")
        db_info = c.__dict__
        assert "__tablename__" in db_info
        assert "__table__" in db_info
        db_table = db_info["__table__"]
        db_table = eval(str(db_table._columns))
        assert db_table == [
            "news.countryid",
            "news.tweettopic",
            "news.title",
            "news.description",
            "news.link",
            "news.author",
            "news.source",
            "news.publishedat",
            "news.publishtime",
            "news.imageurl",
        ]

    def test_countries_model(self):
        o = app.Countries
        assert hasattr(o, "__dict__")
        db_info = o.__dict__
        assert "__tablename__" in db_info
        assert "__table__" in db_info
        db_table = db_info["__table__"]
        db_table = eval(str(db_table._columns))
        assert db_table == [
            "countries.countryid",
            "countries.name",
            "countries.capital",
            "countries.population",
            "countries.subregion",
            "countries.latlng",
            "countries.currencies",
            "countries.languages",
            "countries.timezones",
            "countries.borders",
            "countries.relevance",
            "countries.callingcodes",
            "countries.tweettopic",
            "countries.newstitle",
        ]

    def test_api(self):
        r = requests.get("https://api.thebuzzz.me/api/countries")
        assert r.status_code == 200
        r = requests.get("https://api.thebuzzz.me/api/tweets")
        assert r.status_code == 200
        r = requests.get("https://api.thebuzzz.me/api/news")
        assert r.status_code == 200

    def test_api_fail(self):
        self.assertEqual(
            requests.get("https://api.thebuzzz.me/country").status_code, 404
        )
        self.assertEqual(
            requests.get("https://api.pathogerm.com/tweet").status_code, 404
        )
        self.assertEqual(
            requests.get("https://api.pathogerm.com/NEWS").status_code, 404
        )

    def test_updatesql(self):
        cur = conn.cursor()
        tweet1, article1 = apis.get_tweet_and_article_for_country("United States", cur)
        tweet2, article2 = apis.get_tweet_and_article_for_country("Saudi Arabia", cur)
        assert tweet1 != None  # Test 2
        assert tweet2 != None  # Test 4
        news_test = apis.get_article_for_tweet(tweet1["name"])
        assert news_test != None  # tweet from US will always have a news article
        assert news_test == article1
        news_test2 = apis.get_article_for_tweet(tweet2["name"])
        assert news_test2 != None
        assert news_test2 == article2

    def test_search(self):

        response = requests.get("https://api.thebuzzz.me/api/countries/3")
        output = {
            "borders": ["BOL", "BRA", "CHL", "PRY", "URY"],
            "callingcodes": ["54"],
            "capital": "Buenos Aires",
            "countryid": 3,
            "currencies": ["ARS"],
            "languages": ["es", "gn"],
            "latlng": [-34.0, -64.0],
            "name": "Argentina",
            "newstitle": "When Ester Gets Hoppy",
            "population": 43131966,
            "relevance": 0.0,
            "subregion": "South America",
            "timezones": ["UTC-03:00"],
            "tweettopic": "Ester",
        }
        assert response.status_code == 200
        self.assertEqual(response.json(), output)

        response1 = requests.get("https://api.thebuzzz.me/api/countries/33")
        output1 = {
            "borders": ["GTM", "SLV", "NIC"],
            "callingcodes": ["504"],
            "capital": "Tegucigalpa",
            "countryid": 33,
            "currencies": ["HNL"],
            "languages": ["es"],
            "latlng": [15.0, -86.5],
            "name": "Honduras",
            "newstitle": "Join Elizabeth Bruenig to chat about whatever\u2019s on your mind",
            "population": 8725111,
            "relevance": 0.0,
            "subregion": "Central America",
            "timezones": ["UTC-06:00"],
            "tweettopic": "#covid__19",
        }

        assert response1.status_code == 200
        self.assertEqual(response1.json(), output1)

        response2 = requests.get("https://api.thebuzzz.me/api/tweets/1")
        output2 = {
            "asof": "04/11/2020",
            "countryid": 1,
            "createdat": "04/11/2020",
            "hashtag": "yes",
            "promotedcontent": None,
            "rank": 1,
            "subregion": "Southern Europe",
            "topic": "#EurovisionAgain",
            "tweet1": "1249045809053786114",
            "tweet2": "1248987590553997315",
            "tweet3": "1249049949272236033",
            "tweetsearchlink": "http://twitter.com/search?q=%23EurovisionAgain",
            "tweetvolume": 23314,
        }
        assert response2.status_code == 200
        self.assertEqual(response2.json(), output2)

        response3 = requests.get("https://api.thebuzzz.me/api/news/1")
        output3 = {
            "author": "Richard Trenholm",
            "countryid": 1,
            "description": "COVID-19 earns null points, but you can still enjoy this year's entrants to the infamous song contest.",
            "imageurl": "https://cnet3.cbsistatic.com/img/zeZjkYC4wAE7M0F0wTOOlXoIJfY=/2020/03/18/5043372a-e1ea-4e06-b8fb-0d3a46d896da/eurovision.jpg",
            "link": "https://www.cnet.com/news/eurovision-song-contest-2020-canceled-due-to-coronavirus/",
            "publishedat": "2020-03-18",
            "publishtime": "18:13:00",
            "source": "Cnet.com",
            "subregion": "Southern Europe",
            "title": "Eurovision 2020 canceled by coronavirus, so enjoy the songs here - CNET",
            "tweettopic": "#EurovisionAgain",
        }
        assert response3.status_code == 200
        self.assertEqual(response3.json(), output3)

        response4 = requests.get("https://api.thebuzzz.me/api/tweets/11")
        output4 = {
            "asof": "04/11/2020",
            "countryid": 11,
            "createdat": "04/11/2020",
            "hashtag": "no",
            "promotedcontent": None,
            "rank": 22,
            "subregion": "Eastern Europe",
            "topic": "Eurogroup",
            "tweet1": "1248565417322385409",
            "tweet2": "1248602732232994816",
            "tweet3": "1248577331503017984",
            "tweetsearchlink": "http://twitter.com/search?q=Eurogroup",
            "tweetvolume": None,
        }

        assert response4.status_code == 200
        self.assertEqual(response4.json(), output4)

        response5 = requests.get("https://api.thebuzzz.me/api/news/77")
        output5 = {
            "author": "Mike",
            "countryid": 77,
            "description": "A full featured, compact embedded computer with optional 2.8\u2033 touch LCD and DIN-mountable enclosure that measures... The post TS-7100 \u2013 Feature Dense Embedded Solution with Optional 2.8\u2033 Touch Screen appeared first on Electronics-Lab.",
            "imageurl": "https://www.electronics-lab.com/wp-content/uploads/2020/04/ts-7100-angled-600x400-large@2x.jpg",
            "link": "https://www.electronics-lab.com/ts-7100-feature-dense-embedded-solution-optional-2-8-touch-screen/",
            "publishedat": "2020-04-05",
            "publishtime": "10:46:38",
            "source": "Electronics-lab.com",
            "subregion": "South-Eastern Asia",
            "title": "TS-7100 \u2013 Feature Dense Embedded Solution with Optional 2.8\u2033 Touch Screen",
            "tweettopic": "#circuitbreakersg",
        }

        assert response5.status_code == 200
        self.assertEqual(response5.json(), output5)


if __name__ == "__main__":
    unittest.main()
