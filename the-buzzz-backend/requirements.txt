Flask==1.1.1
Flask-Restless==0.17.0
uWSGI==2.0.18
requests==2.22.0
coverage==4.5.3
SQLAlchemy==1.2.15
Flask-SQLAlchemy==2.3
Flask-CORS==3.0.7
alembic==1.0.11
black==19.10b0
pylint==2.2.2
pyOpenSSL==19.0.0
psycopg2-binary==2.8.3
Flask-Migrate==2.5.2
plotly==4.0.0
pandas==0.25.0
tweepy==3.8.0
wordsegment==1.3.1
country-list==0.1.5
newsapi-python==0.2.6




