from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_restless import APIManager
import psycopg2


app = Flask(__name__)

POSTGRES_URL = "buzzz.cyg5ivkoczx2.us-east-2.rds-preview.amazonaws.com:5432"
POSTGRES_USER = "dbAdmin"
POSTGRES_PW = "thebuzzz"
POSTGRES_DB = "buzzz"

DB_URL = "postgresql+psycopg2://{user}:{pw}@{url}/{db}".format(
    user=POSTGRES_USER, pw=POSTGRES_PW, url=POSTGRES_URL, db=POSTGRES_DB
)

app.config["SQLALCHEMY_DATABASE_URI"] = DB_URL

db = SQLAlchemy(app)



class Tweets(db.Model):
    # features of a Tweet
    countryid = db.Column(db.Integer, primary_key=True)
    topic = db.Column(db.String)
    rank = db.Column(db.Integer)
    promotedcontent = db.Column(db.String)
    tweetvolume = db.Column(db.Integer)
    asof = db.Column(db.String)
    createdat = db.Column(db.String)
    hashtag = db.Column(db.String)
    tweetsearchlink = db.Column(db.String)


class News(db.Model):
    # features of News
    countryid = db.Column(db.Integer, primary_key=True)
    tweettopic = db.Column(db.String)  
    title = db.Column(db.String)
    description = db.Column(db.String)
    link = db.Column(db.String)
    author = db.Column(db.String)
    source = db.Column(db.String)
    publishedat = db.Column(db.String)
    publishtime = db.Column(db.String)
    imageurl = db.Column(db.String)


class Countries(db.Model):
    countryid = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    capital = db.Column(db.String)
    population = db.Column(db.Integer)
    subregion = db.Column(db.String)
    latlng = db.Column(db.JSON)  # added a JSON type
    currencies = db.Column(db.JSON)
    languages = db.Column(db.JSON)
    timezones = db.Column(db.JSON)
    borders = db.Column(db.JSON)
    relevance = db.Column(db.Float)
    callingcodes = db.Column(db.JSON)
    tweettopic = db.Column(db.String)
    newstitle = db.Column(db.String)


# creates an API Manager using Data in the db
manger = APIManager(app, flask_sqlalchemy_db=db)

# creates the apis and methods that are needed
# access using 127.0.0.1:5000/api/person
manger.create_api(
    Tweets, methods=["GET", "DELETE", "PUT"], results_per_page=0
)  # PUT updates the restful api
manger.create_api(News, methods=["GET", "DELETE", "PUT"], results_per_page=0)
manger.create_api(Countries, methods=["GET", "DELETE", "PUT"], results_per_page=0)

if __name__ == "__main__":
    app.run(debug=True)
