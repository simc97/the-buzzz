import tweepy
import private 
import datetime, time
from wordsegment import load, segment
from country_list import countries_for_language
import psycopg2 as p
import staticData

from newsapi  import NewsApiClient 
import requests
import math 

woeids = staticData.where_on_earth_IDs
english = staticData.English
numbers = staticData.Numbers


'''
NOTE: 
    This function assumes that if a char, either starting the tweet
    or following a series of numbers, is in english, then the entire
    tweet is in english
'''
def get_tweet_and_article_for_country(country, cur):

    auth = tweepy.OAuthHandler(private.TWITTER_APP_KEY, private.TWITTER_APP_SECRET)
    auth.set_access_token(private.TWITTER_KEY, private.TWITTER_SECRET)
    api = tweepy.API(auth)
    
    #get the Where On Earth ID and find the trends there
    woeid = woeids.get(country)
    trends = api.trends_place(woeid)
    
    finding = True
    j = 0
    rank = 1
    temp_tweet = []
    temp_art = []
    #while true, go through every tweet until found a valid english one
    while finding: 
        #get one tweet

        tweetInfo = trends[0].get("trends")[j]
        tweet = tweetInfo.get("name")
        index = 0

        cur.execute("SELECT * FROM tweets;")
        rows = cur.fetchall()
        duplicate = False
        for row in range(len(rows)):
            if rows[row][1] == tweet:
                duplicate = True
                break


        if(duplicate == False):
            #if it is a hashtag, look after the tag
            if tweet.startswith('#'):
                index = index + 1
            
            allNumbers = False
            #while there are no more numbers starting the tweet, increase index
            while(tweet[index] in numbers):
                index = index + 1
                #if the index is greater that the len of the tweet, tweet is full of numbers
                if(index >= len(tweet)):
                    allNumbers = True
                    break
            #if there are no more numbers
            if(allNumbers == False):
                #check if in english, and return if so
                if tweet[index] in english:
                    
                    article = get_article_for_tweet(tweetInfo.get("name"))

                    if(article != None):
                        tweetInfo['rank'] = rank
                        return tweetInfo, article



        #move to next tweet
        j+=1
        rank+=1
        #if we have looked at all trends and still found nothing, return none
        if(j >= len(trends[0].get("trends"))):
            info = trends[0].get("trends")[0]
            news = get_article_for_tweet("None") #TODO CHANGE THIS
            info['rank'] = 51
            return info, news

#split the hashtag up into differnet words
def get_words(hashtag):
    load()
    words = segment(hashtag)
    search = " "
    return (search.join(words)) 



def get_article_for_tweet(topic):
    newsapi = NewsApiClient(api_key=private.NEWS_KEY)
    #if hashtag
    if(topic.startswith('#')):
        #split up words
        topic = topic[1:]
        search = get_words(topic)
    else:
        search = topic
    #search
    news = newsapi.get_everything(q=search , language = "en")
    #get the top article
    top_articles = news.get("articles")
    try:
        article = top_articles[0]
    except:
        return None
    date, time = getDateAndTime(article)
    article['time'] = time
    article['publishedAt'] = date


    return article


def getDateAndTime(article):
    #get the string
    published = article.get('publishedAt')
    date = ""
    time = ""
    index = 0
    #while not T, place every char of published into date
    while(published[index] != 'T'):
        date = date + published[index]
        index += 1
    
    #move pass T
    index += 1
    while(published[index] != 'Z'):
        time = time + published[index]
        index += 1
    
    return (date, time)



def search_for_tweet(topic):

    auth = tweepy.OAuthHandler(private.TWITTER_APP_KEY, private.TWITTER_APP_SECRET)
    auth.set_access_token(private.TWITTER_KEY, private.TWITTER_SECRET)
    api = tweepy.API(auth)
    
    tweetIDs = []
    for result in api.search(q = topic, result_type = "popular", count = 3):
        tweetIDs.append(result.id)

    return tweetIDs
